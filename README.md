# Introduction

This sub-project is part of the ["New IACS Vision in Action” --- NIVA](https://www.niva4cap.eu/) project that delivers a suite of digital solutions, e-tools and good practices for e-governance and initiates an innovation ecosystem to support further development of IACS that will facilitate data and information flows.
This project has received funding from the European Union’s Horizon 2020 research and innovation programme under grant agreement No 842009.
Please visit the [website](https://www.niva4cap.eu) for further information.
A complete list of the sub-projects made available under the NIVA project can be found on [gitlab](https://gitlab.com/nivaeu/).

## NIVA UC1C Back-End

The back-end for uc1c-ui.

## Development

### Requirements

- Node v14.6.1+
- Npm
- PostgreSQL with [PostGIS extension](https://postgis.net/install/)

### 1. Create database, extension and schemas

Create database in postgres shell:
```sql
CREATE DATABASE farmerperformance with TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';
```

Create postgis extension in postgres shell:
```sql
\connect farmerperformance
CREATE EXTENSION postgis;
```

Create schemas called 'sc_niva' and 'sc_niva_extra' in postgres shell:
```sql
\connect farmerperformance
CREATE SCHEMA sc_niva;
CREATE SCHEMA sc_niva_extra;
```

### 2. Setup environment variables

- Create .env file in the root directory

Add only the variable values that need to be changed.
By default the following values are used:

```sh
DB_HOST=localhost

DB_USERNAME=postgres

DB_PASSWORD=postgres

DB_TYPE=postgres

DB_PORT=5432

DB_DATABASE=farmerperformance

DB_SYNC=true

DB_SSL=false

SERVER_PORT=4000
```

Note that `DB_SSL=false` is necessary if the postgres database does no have SSL incoming connections enabled, as is the default case then simply running the database in localhost.

### 3. Install dependencies in root directory

`npm install`

### 4. Start the application in root directory

`npm run build && npm run start`

### 5. Add sample data to the database tables

After the application successfully starts use the file named 'sample-data.sql' located in the root directory to add sample data to the database tables.
If pgAdmin GUI is used to manage Postgres database it can be done via Tools -> Query Tool -> Open File -> Select -> Execute. It can also be done by manually using 'sample-data.sql' file content in query tool and running it. Afterwards, certain database tables should be filled with data and testing can begin by starting the uc1c-ui.

## API Endpoints

Open [http://localhost:4000](http://localhost:4000) to view API endpoints in the browser.

## Tests

` npm run test`
