/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FarmerModule } from './endpoints/farmer/farmer.module';
import { ParcelModule } from './endpoints/parcel/parcel.module';
import { CrpModule } from './endpoints/crp/crp.module';
import { CrvModule } from './endpoints/crv/crv.module';
import { FerModule } from './endpoints/fer/fer.module';
import { LabModule } from './endpoints/lab/lab.module';
import { PhyModule } from './endpoints/phy/phy.module';
import { UniModule } from './endpoints/uni/uni.module';
import { ParcelActivitiesModule } from './endpoints/parcelActivities/parcelActivities.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { join } from 'path';

@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        type: configService.get<any>('DB_TYPE') || 'postgres',
        host: configService.get<string>('DB_HOST') || 'localhost',
        port: configService.get<number>('DB_PORT') || 5432,
        username: configService.get<string>('DB_USERNAME') || 'postgres',
        password: configService.get<string>('DB_PASSWORD') || 'postgres',
        database:
          configService.get<string>('DB_DATABASE') || 'farmerperformance',
        entities: [join(__dirname, '**', '*.entity.{ts,js}')],
        synchronize: true,
		extra: {
          ssl:
			(configService.get<string>('DB_SSL') ? configService.get<string>('DB_SSL') : 'true').toLowerCase() === 'true'
				? { rejectUnauthorized: false }
				: false
        },
      }),
      inject: [ConfigService],
    }),
    FarmerModule,
    ParcelModule,
    CrpModule,
    CrvModule,
    FerModule,
    LabModule,
    PhyModule,
    UniModule,
    ParcelActivitiesModule,
  ],
})
export class AppModule {}
