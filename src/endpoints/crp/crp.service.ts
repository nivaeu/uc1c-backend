/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Loc_copcropproductRepository } from './repositories/loc_co_pcropproduct.repository';

@Injectable()
export class CrpService {
  constructor(
    @InjectRepository(Loc_copcropproductRepository)
    private loc_copcropproductRepository: Loc_copcropproductRepository,
  ) {}

  async queryCrpData() {
    const results = await this.loc_copcropproductRepository.queryCrpData();
    return results;
  }
}
