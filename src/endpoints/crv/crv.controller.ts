/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Controller, Get } from '@nestjs/common';
import { CrvService } from './crv.service';
import { ApiNotFoundResponse, ApiOperation, ApiTags } from '@nestjs/swagger';

@ApiTags('List of crop varieties')
@Controller('crv')
export class CrvController {
  constructor(private crvService: CrvService) {}

  @Get()
  @ApiOperation({
    summary: 'Get list of crop varieties',
  })
  @ApiNotFoundResponse({
    description: 'Unable to find list of crop varieties data',
  })
  queryCrpData() {
    return this.crvService.queryCrvData();
  }
}
