/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Loc_copcropvarietyRepository } from './repositories/loc_co_pcropvariety.repository';

@Injectable()
export class CrvService {
  constructor(
    @InjectRepository(Loc_copcropvarietyRepository)
    private loc_copcropvarietyRepository: Loc_copcropvarietyRepository,
  ) {}

  async queryCrvData() {
    const results = await this.loc_copcropvarietyRepository.queryCrvData();
    return results;
  }
}
