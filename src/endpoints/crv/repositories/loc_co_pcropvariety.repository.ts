/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { EntityRepository, Repository } from 'typeorm';
import { LocCoPcropvariety as loc_co_pcropvariety } from '../../../entities/schemas/sc_niva_extra/loc/loc_co_pcropvariety.entity';

@EntityRepository(loc_co_pcropvariety)
export class Loc_copcropvarietyRepository extends Repository<loc_co_pcropvariety> {
  async queryCrvData() {
    const crvData = await loc_co_pcropvariety.find();
    return crvData;
  }
}
