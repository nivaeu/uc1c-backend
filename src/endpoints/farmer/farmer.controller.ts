/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Controller, Get, Query, ValidationPipe } from '@nestjs/common';
import { FarmerService } from './farmer.service';
import { QueryFarmerDto } from './dto/query-farmer-dto';
import { ApiNotFoundResponse, ApiExcludeEndpoint } from '@nestjs/swagger';

@Controller('farmer')
export class FarmerController {
  constructor(private farmerService: FarmerService) {}
  @ApiExcludeEndpoint()
  @Get()
  @ApiNotFoundResponse({ description: 'Unable to find farmer data' })
  queryFarmer(@Query(ValidationPipe) queryDto: QueryFarmerDto) {
    return this.farmerService.queryFarmer(queryDto);
  }
}
