/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FarmerService } from './farmer.service';
import { FarmerController } from './farmer.controller';
import { PR_dfarmerdataRepository } from './repositories/pr_dfarmerdata.repository';

@Module({
  imports: [TypeOrmModule.forFeature([PR_dfarmerdataRepository])],
  providers: [FarmerService],
  controllers: [FarmerController],
})
export class FarmerModule {}
