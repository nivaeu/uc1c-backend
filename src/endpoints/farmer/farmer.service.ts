/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PR_dfarmerdataRepository } from './repositories/pr_dfarmerdata.repository';
import { QueryFarmerDto } from './dto/query-farmer-dto';

@Injectable()
export class FarmerService {
  constructor(
    @InjectRepository(PR_dfarmerdataRepository)
    private pr_dfarmerdataRepository: PR_dfarmerdataRepository,
  ) {}
  async queryFarmer(queryDto: QueryFarmerDto) {
    const results = await this.pr_dfarmerdataRepository.queryFarmer(queryDto);

    if (results.length !== 0) {
      return results;
    } else {
      throw new NotFoundException('Farmer(s) not found.');
    }
  }
  async onApplicationBootstrap() {
    await this.pr_dfarmerdataRepository.setEconomicGroupsData();
  }
}
