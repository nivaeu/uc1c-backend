/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { EntityRepository, ILike, Repository } from 'typeorm';
import BigNumber from 'bignumber.js';
import { PrDfarmerdata as pr_dfarmerdata } from '../../../entities/schemas/sc_niva/pr_dfarmerdata.entity';
import { PrDproductionunit as pr_dproductionunit } from '../../../entities/schemas/sc_niva/pr_dproductionunit.entity';
import { QueryFarmerDto } from '../dto/query-farmer-dto';
import { sortEconomicInfo, setClientsData } from './utils/economicClass';
import { sortSubsidiesData } from './utils/subsidies';
import { sortAnimalsData } from './utils/animals';
import { sortLivestockData } from './utils/livestockLoad';
import {
  sortParcelsData,
  setParcelsLandTypeTableData,
  setAverageParcelSizesByGroup,
  setAverageParcelLandTypeDataByGroup,
  addEconomicGroupDataForParcelsPieChartTableData,
} from './utils/parcels';
import { sortPlantFertilizersData } from './utils/plantFertilizers';
import { sortPlantChemicalsData } from './utils/plantChemicals';
import { sortAgrotechnicsData } from './utils/agrotechnics';

BigNumber.config({ ROUNDING_MODE: BigNumber.ROUND_DOWN });

export const queryPunId = async (pr_dproductionunit, farId: string) => {
  const punIdQuery = await pr_dproductionunit.find({
    select: ['punId'],
    where: { farId },
    take: 1,
  });

  if (punIdQuery.length !== 0) {
    return punIdQuery[0].punId;
  }
};

@EntityRepository(pr_dfarmerdata)
export class PR_dfarmerdataRepository extends Repository<pr_dfarmerdata> {
  async queryFarmer(queryDto: QueryFarmerDto) {
    const { clientId, name, limit, farmerId } = queryDto;
    let farmerDataQuery;

    if (limit) {
      if (clientId && name) {
        farmerDataQuery = await pr_dfarmerdata.find({
          select: ['farId', 'fadPersonalId', 'fadName'],
          where: [{ fadPersonalId: clientId, fadName: name }],
          take: limit,
        });
      } else if (clientId) {
        farmerDataQuery = await pr_dfarmerdata.find({
          select: ['farId', 'fadPersonalId', 'fadName'],
          where: { fadPersonalId: clientId },
          take: limit,
        });
      } else if (name) {
        farmerDataQuery = await pr_dfarmerdata.find({
          select: ['farId', 'fadPersonalId', 'fadName'],
          where: { fadName: name },
          take: limit,
        });
      } else if (farmerId) {
        farmerDataQuery = await pr_dfarmerdata.find({
          select: ['farId', 'fadPersonalId', 'fadName'],
          where: { farId: farmerId },
          take: limit,
        });
      }

      if (farmerDataQuery.length !== 0) {
        const queryManager = this.manager;
        const { farId, fadName } = farmerDataQuery[0];
        const punId = await queryPunId(pr_dproductionunit, farId);
        const parcels = await sortParcelsData(punId, queryManager);
        const animals = await sortAnimalsData(punId, queryManager);
        const subsidies = await sortSubsidiesData(punId, queryManager);
        const livestockLoadData = sortLivestockData(parcels, animals);
        const plantFertilizers = sortPlantFertilizersData(parcels);
        const plantChemicals = sortPlantChemicalsData(parcels);
        const economicInfo = sortEconomicInfo(parcels, animals);
        const agrotechnics = sortAgrotechnicsData(parcels);

        addEconomicGroupDataForParcelsPieChartTableData(parcels, economicInfo);
        const parcelsLandTypeTableData = setParcelsLandTypeTableData(
          parcels,
          economicInfo,
        );

        const response = {
          id: farId,
          name: fadName,
          parcels: {
            ...parcels,
            ...parcelsLandTypeTableData,
          },
          animals,
          subsidies,
          economicInfo,
          plantChemicals,
          plantFertilizers,
          livestockLoadData,
          agrotechnics,
        };
        return response;
      } else {
        return [];
      }
    } else {
      if (clientId && name) {
        farmerDataQuery = await pr_dfarmerdata.find({
          select: ['fadPersonalId', 'fadName'],
          where: [
            { fadPersonalId: ILike(`%${clientId}%`) },
            { fadName: ILike(`%${name}%`) },
          ],
          take: 10,
        });
      } else if (clientId) {
        farmerDataQuery = await pr_dfarmerdata.find({
          select: ['fadPersonalId', 'fadName'],
          where: { fadPersonalId: ILike(`%${clientId}%`) },
          take: 10,
        });
      } else if (name) {
        farmerDataQuery = await pr_dfarmerdata.find({
          select: ['fadPersonalId', 'fadName'],
          where: { fadName: ILike(`%${name}%`) },
          take: 10,
        });
      }

      return farmerDataQuery;
    }
  }

  async setEconomicGroupsData() {
    const queryManager = this.manager;
    const allClients = await pr_dfarmerdata.find({
      select: ['farId'],
      take: 500,
    });

    await Promise.all(
      allClients.map(async (client) => {
        const { farId } = client;
        const punId = await queryPunId(pr_dproductionunit, farId);
        await setClientsData(punId, queryManager);
      }),
    );

    await setAverageParcelSizesByGroup(queryManager);
    await setAverageParcelLandTypeDataByGroup(queryManager);
  }
}
