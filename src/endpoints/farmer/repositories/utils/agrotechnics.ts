/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import * as moment from 'moment';
import { EntityManager } from 'typeorm';
import { Parcel, Parcels } from './parcels';

interface Agrotechnic {
  cadStartDate: Date;
  cadEndDate: Date;
  labNameEt: string;
  labNameEn: string;
  rcpKey: string;
}

interface AgrotechnicsTableDataEt {
  period: string;
  headers: Array<string>;
  parcels: any;
}

interface AgrotechnicsTableDataEn {
  period: string;
  headers: Array<string>;
  parcels: any;
}

interface Agrotech {
  lab_id: number;
  cad_startdate: Date;
  cad_enddate: Date;
}

export const queryAgrotechnics = async (
  rcpId: string,
  rcpKey: string,
  queryManager: EntityManager,
) => {
  const agrotechnicsQuery = await queryManager.query(
    `SELECT lab_id, cad_startdate, cad_enddate FROM sc_niva.rp_dcropactivitydet WHERE rcp_id = '${rcpId}' ORDER BY TO_DATE(CAST(cad_startdate AS char(10)), 'YYYY-MM-DD')`,
  );
  const agrotechnics: Agrotechnic[] = [];

  if (agrotechnicsQuery.length !== 0) {
    await Promise.all(
      agrotechnicsQuery.map(async (agrotech: Agrotech) => {
        const {
          lab_id: labId,
          cad_startdate: cadStartDate,
          cad_enddate: cadEndDate,
        } = agrotech;

        const agrotechTypeQuery = await queryManager.query(
          `SELECT lab_name_et, lab_name_en FROM sc_niva_extra.loc_co_plabortype WHERE lab_id = '${labId}'`,
        );

        const {
          lab_name_et: labNameEt,
          lab_name_en: labNameEn,
        } = agrotechTypeQuery[0];

        agrotechnics.push({
          cadStartDate,
          cadEndDate,
          labNameEt,
          labNameEn,
          rcpKey,
        });
      }),
    );
  }

  return agrotechnics;
};

const setAgrotechnicsTableData = (parcelsData: Parcel[]) => {
  const agrotechnicsTableDataEt: AgrotechnicsTableDataEt[] = [];
  const agrotechnicsTableDataEn: AgrotechnicsTableDataEn[] = [];

  parcelsData.forEach((parcel: Parcel) => {
    const { agrotechnics } = parcel;

    agrotechnics.forEach((agrotechnic: Agrotechnic) => {
      const {
        labNameEt,
        labNameEn,
        rcpKey,
        cadStartDate,
        cadEndDate,
      } = agrotechnic;
      const period = cadStartDate.getFullYear().toString();
      const momentStartDate = moment
        .utc(cadStartDate.getFullYear().toString())
        .format('DD.MM.YY');
      const momentEndDate = moment
        .utc(cadEndDate.getFullYear().toString())
        .format('DD.MM.YY');

      const indexEt = agrotechnicsTableDataEt.findIndex(
        (agrotechnicsTableData) => {
          return agrotechnicsTableData.period === period;
        },
      );

      if (indexEt !== -1) {
        const parcelIndex = agrotechnicsTableDataEt[indexEt].parcels.findIndex(
          (parcel: Parcel) => {
            return parcel.rcpKey === rcpKey;
          },
        );

        if (parcelIndex !== -1) {
          agrotechnicsTableDataEt[indexEt].parcels[parcelIndex].unitsData.push([
            momentStartDate,
            momentEndDate,
            labNameEt,
          ]);
        } else {
          agrotechnicsTableDataEt[indexEt].parcels.push({
            rcpKey,
            unitsData: [[momentStartDate, momentEndDate, labNameEt]],
          });
        }
      } else {
        const headers = ['Algus', 'Lõpp', 'Tegevus'];

        agrotechnicsTableDataEt.push({
          period,
          headers,
          parcels: [
            {
              rcpKey,
              unitsData: [[momentStartDate, momentEndDate, labNameEt]],
            },
          ],
        });
      }

      const indexEn = agrotechnicsTableDataEn.findIndex(
        (agrotechnicsTableData) => {
          return agrotechnicsTableData.period === period;
        },
      );

      if (indexEn !== -1) {
        const parcelIndex = agrotechnicsTableDataEn[indexEn].parcels.findIndex(
          (parcel: Parcel) => {
            return parcel.rcpKey === rcpKey;
          },
        );

        if (parcelIndex !== -1) {
          agrotechnicsTableDataEn[indexEn].parcels[parcelIndex].unitsData.push([
            momentStartDate,
            momentEndDate,
            labNameEn,
          ]);
        } else {
          agrotechnicsTableDataEn[indexEn].parcels.push({
            rcpKey,
            unitsData: [[momentStartDate, momentEndDate, labNameEn]],
          });
        }
      } else {
        const headers = ['Start', 'End', 'Activity'];

        agrotechnicsTableDataEn.push({
          period,
          headers,
          parcels: [
            {
              rcpKey,
              unitsData: [[momentStartDate, momentEndDate, labNameEn]],
            },
          ],
        });
      }
    });
  });

  return { agrotechnicsTableDataEt, agrotechnicsTableDataEn };
};

export const sortAgrotechnicsData = (parcels: Parcels) => {
  const { parcelsData } = parcels;
  const agrotechnicsTableData = setAgrotechnicsTableData(parcelsData);
  const {
    agrotechnicsTableDataEt,
    agrotechnicsTableDataEn,
  } = agrotechnicsTableData;

  return {
    agrotechnicsTableDataEt,
    agrotechnicsTableDataEn,
  };
};
