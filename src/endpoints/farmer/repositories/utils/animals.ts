/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import BigNumber from 'bignumber.js';
import { EntityManager } from 'typeorm';
import { CoAnimalSpecies as co_animalspecies } from '../../../../entities/schemas/sc_niva_extra/co_animalspecies.entity';
import { calculateEconomicAmount } from './economicClass';

interface Animal {
  period: string;
  fanNumber: BigNumber;
  andNameEt: string;
  andNameEn: string;
  ansNameShortEt: string;
  ansNameShortEn: string;
  ansNameEt: string;
  ansNameEn: string;
  livestock: BigNumber;
  economicAmount: BigNumber;
  soc: string | number;
}

export interface SortedAnimalsData {
  animalsEn: AnimalsByYearEn;
  animalsEt: AnimalsByYearEt;
  animalsTableDataEt: AnimalsTableDataEt;
  animalsTableDataEn: AnimalsTableDataEn;
  animalsEconomicInfoByYear: AnimalsEconomicInfoByYear;
  unitEn: string;
  unitEt: string;
}

interface AnimalQueryData {
  and_id: string;
  fan_number: number | BigNumber;
  fan_datefrom: Date;
}

interface AnimalsTableDataEt {
  period: string;
  headers: Array<string>;
  unitsData: any;
  totals: any;
}

interface AnimalsTableDataEn {
  period: string;
  headers: Array<string>;
  unitsData: any;
  totals: any;
}

interface AnimalsSortedByGroup {
  period: string;
  andNameEt: string;
  andNameEn: string;
  fanNumber: BigNumber;
  livestock: BigNumber;
}

interface AnimalsByYearEn {
  period: string;
  name: string;
  shortName: string;
  value: any;
  livestock: BigNumber;
  unit: string;
}

interface AnimalsByYearEt {
  period: string;
  name: string;
  shortName: string;
  value: any;
  livestock: BigNumber;
  unit: string;
}

interface AnimalsSortedByGroup {
  period: string;
  andNameEt: string;
  andNameEn: string;
  fanNumber: BigNumber;
  livestock: BigNumber;
}

export interface AnimalsEconomicInfoByYear {
  period: string;
  economicAmount: BigNumber;
  [socCode: string]: any;
}

const querySoAndSocId = async (andId: string, queryManager: EntityManager) => {
  const soAndSocIdQuery = await queryManager.query(
    `SELECT so_id, soc_id FROM sc_niva_extra.co_animalspecies_detail_soc WHERE and_id = '${andId}'`,
  );
  const { so_id: soId, soc_id: socId } = soAndSocIdQuery[0];

  return {
    soId,
    socId,
  };
};

const querySoValue = async (soId: string, queryManager: EntityManager) => {
  const coStandardOutputQuery = await queryManager.query(
    `SELECT so_value FROM sc_niva_extra.co_standardoutput WHERE so_id = '${soId}'`,
  );
  let { so_value: soValue } = coStandardOutputQuery[0];
  soValue = new BigNumber(soValue);

  return soValue;
};

const queryAnimalSocCode = async (
  socId: string,
  queryManager: EntityManager,
) => {
  const animalSocCodeQuery = await queryManager.query(
    `SELECT soc FROM sc_niva_extra.COU_ANIMAL_SOC WHERE soc_id = '${socId}'`,
  );

  const { soc } = animalSocCodeQuery[0];
  return soc;
};

export const queryAnimalsData = async (
  punId: string,
  queryManager: EntityManager,
) => {
  const animalsDataQuery = await queryManager.query(
    `SELECT and_id, fan_number, fan_datefrom from sc_niva_extra.far_danimal WHERE pun_id = '${punId}'`,
  );

  const animalsData: Animal[] = [];
  await Promise.all(
    animalsDataQuery.map(async (animal: AnimalQueryData) => {
      const { and_id: andId, fan_datefrom: fanDateFrom } = animal;
      const period = fanDateFrom.getFullYear().toString();
      let { fan_number: fanNumber } = animal;
      fanNumber = new BigNumber(fanNumber);

      const animalDetailsQuery = await queryManager.query(
        `SELECT and_name_et, and_name_en, and_lu, ans_id FROM sc_niva_extra.co_animalspecies_detail WHERE and_id = '${andId}'`,
      );

      const {
        and_name_et: andNameEt,
        and_name_en: andNameEn,
        ans_id: ansId,
      } = animalDetailsQuery[0];

      let { and_lu: andLu } = animalDetailsQuery[0];
      andLu = new BigNumber(andLu);
      const livestock = fanNumber.times(andLu);
      const soAndSocId = await querySoAndSocId(andId, queryManager);
      const { soId, socId } = soAndSocId;

      let soc = 0;
      if (socId !== null) {
        soc = await queryAnimalSocCode(socId, queryManager);
      } else {
        soc = 0;
      }

      const soValue = await querySoValue(soId, queryManager);
      const economicAmount = calculateEconomicAmount(fanNumber, soValue);
      const animalNamesQuery = await co_animalspecies.find({
        select: ['ansNameEt', 'ansNameEn', 'ansNameShortEt', 'ansNameShortEn'],
        where: { ansId },
      });
      const {
        ansNameEt,
        ansNameEn,
        ansNameShortEt,
        ansNameShortEn,
      } = animalNamesQuery[0];

      const animalData = {
        period,
        fanNumber,
        andNameEt,
        andNameEn,
        ansNameShortEt,
        ansNameShortEn,
        ansNameEt,
        ansNameEn,
        livestock,
        economicAmount,
        soc,
      };

      animalsData.push(animalData);
    }),
  );

  return animalsData;
};

const sortAnimalsByGroup = (animalsData: Animal[]) => {
  const animalsSortedByGroup: AnimalsSortedByGroup[] = [];

  animalsData.forEach((animal: Animal) => {
    const animalsSortedByGroupIndex = animalsSortedByGroup.findIndex(
      (sortedAnimal) => {
        return (
          sortedAnimal.period === animal.period &&
          sortedAnimal.andNameEt === animal.andNameEt &&
          sortedAnimal.andNameEn === animal.andNameEn
        );
      },
    );

    if (animalsSortedByGroupIndex !== -1) {
      animalsSortedByGroup[
        animalsSortedByGroupIndex
      ].fanNumber = animalsSortedByGroup[
        animalsSortedByGroupIndex
      ].fanNumber.plus(animal.fanNumber);

      animalsSortedByGroup[
        animalsSortedByGroupIndex
      ].livestock = animalsSortedByGroup[
        animalsSortedByGroupIndex
      ].livestock.plus(animal.livestock);
    } else {
      animalsSortedByGroup.push({
        period: animal.period,
        andNameEt: animal.andNameEt,
        andNameEn: animal.andNameEn,
        fanNumber: animal.fanNumber,
        livestock: animal.livestock,
      });
    }
  });

  return animalsSortedByGroup;
};

const sortAnimalsByYear = (animalsData: Animal[]) => {
  const animalsByYearEn: AnimalsByYearEn[] = [];
  const animalsByYearEt: AnimalsByYearEt[] = [];

  animalsData.forEach((animal: Animal) => {
    const indexEn = animalsByYearEn.findIndex((animalByYear) => {
      return (
        animalByYear.period === animal.period &&
        animalByYear.name === animal.ansNameEn
      );
    });

    if (indexEn !== -1) {
      animalsByYearEn[indexEn].value = animalsByYearEn[indexEn].value.plus(
        animal.fanNumber,
      );

      animalsByYearEn[indexEn].livestock = animalsByYearEn[
        indexEn
      ].livestock.plus(animal.livestock);
    } else {
      animalsByYearEn.push({
        period: animal.period,
        name: animal.ansNameEn,
        shortName: animal.ansNameShortEn,
        value: animal.fanNumber,
        livestock: animal.livestock,
        unit: 'animal',
      });
    }

    const indexEt = animalsByYearEt.findIndex((animalByYear) => {
      return (
        animalByYear.period === animal.period &&
        animalByYear.name === animal.ansNameEt
      );
    });

    if (indexEt !== -1) {
      animalsByYearEt[indexEt].value = animalsByYearEt[indexEt].value.plus(
        animal.fanNumber,
      );

      animalsByYearEt[indexEt].livestock = animalsByYearEt[
        indexEt
      ].livestock.plus(animal.livestock);
    } else {
      animalsByYearEt.push({
        period: animal.period,
        name: animal.ansNameEt,
        shortName: animal.ansNameShortEt,
        value: animal.fanNumber,
        livestock: animal.livestock,
        unit: 'loom',
      });
    }
  });

  animalsByYearEt.forEach((animal) => {
    return (animal.value = animal.value.toNumber());
  });

  animalsByYearEn.forEach((animal) => {
    return (animal.value = animal.value.toNumber());
  });

  return {
    animalsEn: animalsByYearEn,
    animalsEt: animalsByYearEt,
  };
};

const setAnimalsTableData = (animalsSortedByGroup: AnimalsSortedByGroup[]) => {
  const animalsTableDataEn: AnimalsTableDataEn[] = [];
  const animalsTableDataEt: AnimalsTableDataEt[] = [];

  animalsSortedByGroup.forEach((animal: AnimalsSortedByGroup) => {
    const animalsTableDataIndexEt = animalsTableDataEt.findIndex(
      (animalTableData) => {
        return animalTableData.period === animal.period;
      },
    );

    if (animalsTableDataIndexEt !== -1) {
      animalsTableDataEt[animalsTableDataIndexEt].unitsData.push([
        animal.andNameEt,
        animal.fanNumber,
        animal.livestock,
      ]);

      animalsTableDataEt[
        animalsTableDataIndexEt
      ].totals[1] = animalsTableDataEt[animalsTableDataIndexEt].totals[1].plus(
        animal.fanNumber,
      );

      animalsTableDataEt[
        animalsTableDataIndexEt
      ].totals[2] = animalsTableDataEt[animalsTableDataIndexEt].totals[2].plus(
        animal.livestock,
      );
    } else {
      animalsTableDataEt.push({
        period: animal.period,
        headers: ['Loom', 'Loomade arv', 'Loomühikute arv'],
        unitsData: [[animal.andNameEt, animal.fanNumber, animal.livestock]],
        totals: ['', animal.fanNumber, animal.livestock],
      });
    }

    const animalsTableDataIndexEn = animalsTableDataEn.findIndex(
      (animalTableData) => {
        return animalTableData.period === animal.period;
      },
    );

    if (animalsTableDataIndexEn !== -1) {
      animalsTableDataEn[animalsTableDataIndexEn].unitsData.push([
        animal.andNameEn,
        animal.fanNumber,
        animal.livestock,
      ]);

      animalsTableDataEn[
        animalsTableDataIndexEn
      ].totals[1] = animalsTableDataEn[animalsTableDataIndexEn].totals[1].plus(
        animal.fanNumber,
      );

      animalsTableDataEn[
        animalsTableDataIndexEn
      ].totals[2] = animalsTableDataEn[animalsTableDataIndexEn].totals[2].plus(
        animal.livestock,
      );
    } else {
      animalsTableDataEn.push({
        period: animal.period,
        headers: ['Animal', 'Animal count', 'Livestock units'],
        unitsData: [[animal.andNameEn, animal.fanNumber, animal.livestock]],
        totals: ['', animal.fanNumber, animal.livestock],
      });
    }
  });

  return {
    animalsTableDataEt,
    animalsTableDataEn,
  };
};

const sortAnimalsEconomicInfoByYear = (animalsData: Animal[]) => {
  const animalsEconomicInfoByYear: AnimalsEconomicInfoByYear[] = [];

  animalsData.forEach((animal: Animal) => {
    const animalsEconomicInfoByYearIndex = animalsEconomicInfoByYear.findIndex(
      (animalInfo) => {
        return animal.period === animalInfo.period;
      },
    );

    if (animalsEconomicInfoByYearIndex !== -1) {
      animalsEconomicInfoByYear[
        animalsEconomicInfoByYearIndex
      ].economicAmount = animalsEconomicInfoByYear[
        animalsEconomicInfoByYearIndex
      ].economicAmount.plus(animal.economicAmount);

      if (
        animalsEconomicInfoByYear[animalsEconomicInfoByYearIndex][animal.soc]
      ) {
        animalsEconomicInfoByYear[animalsEconomicInfoByYearIndex][
          animal.soc
        ] = animalsEconomicInfoByYear[animalsEconomicInfoByYearIndex][
          animal.soc
        ].plus(animal.economicAmount);
      } else {
        animalsEconomicInfoByYear[animalsEconomicInfoByYearIndex][animal.soc] =
          animal.economicAmount;
      }
    } else {
      animalsEconomicInfoByYear.push({
        period: animal.period,
        economicAmount: animal.economicAmount,
        [animal.soc]: animal.economicAmount,
      });
    }
  });
  return animalsEconomicInfoByYear;
};

export const sortAnimalsDataForEconomicGroupCalculations = async (
  punId: string,
  queryManager: EntityManager,
) => {
  const animalsData = await queryAnimalsData(punId, queryManager);
  const animalsEconomicInfoByYear = sortAnimalsEconomicInfoByYear(animalsData);

  return {
    animalsEconomicInfoByYear,
  };
};

export const sortAnimalsData = async (
  punId: string,
  queryManager: EntityManager,
) => {
  const animalsData = await queryAnimalsData(punId, queryManager);
  const animalsByYear = sortAnimalsByYear(animalsData);
  const animalsSortedByGroup = sortAnimalsByGroup(animalsData);
  const animalsTableData = setAnimalsTableData(animalsSortedByGroup);
  const animalsEconomicInfoByYear = sortAnimalsEconomicInfoByYear(animalsData);
  const { animalsEn, animalsEt } = animalsByYear;
  const { animalsTableDataEt, animalsTableDataEn } = animalsTableData;

  return {
    animalsEn,
    animalsEt,
    animalsTableDataEt,
    animalsTableDataEn,
    animalsEconomicInfoByYear,
    unitEn: 'animal count',
    unitEt: 'loomade arv',
  };
};
