/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import BigNumber from 'bignumber.js';
import {
  sortAverageParcelSizesByYearAndGroup,
  sortParcelsDataForEconomicGroupCalculations,
  sortParcelsLandTypeDataByYearAndGroup,
} from '../utils/parcels';
import { sortAnimalsDataForEconomicGroupCalculations } from '../utils/animals';
import { ParcelsEconomicInfoByYear } from './parcels';
import { AnimalsEconomicInfoByYear, SortedAnimalsData } from './animals';
import { EntityManager } from 'typeorm';

interface EconomicSumsByGroup {
  specialistHorticultureSum: BigNumber;
  specialistPermanentCropsSum: BigNumber;
  specialistGranivoresSum: BigNumber;
  specialistGrazingLivestockSum: BigNumber;
  specialistFieldCropsSum: BigNumber;
}

interface SocCodes {
  [socCode: string]: BigNumber;
}

interface EconomicInfoByYearL {
  period: string;
  value: number;
  economicClass: string;
  name: string;
  productionType: string;
  productionTypeEn: string;
  productionTypeEt: string;
}

interface EconomicInfoByYear {
  period: string;
  value: number;
  economicClass: string;
  nameEt: string;
  nameEn: string;
  productionTypeEn: string;
  productionTypeEt: string;
}

export const calculateEconomicAmount = (
  unit: BigNumber,
  soValue: BigNumber,
) => {
  return soValue.times(unit);
};

export const calculateEconomicClass = (amount: BigNumber) => {
  if (amount.isLessThan(new BigNumber(2000))) {
    return '1: 0 - 1 999€';
  } else if (
    amount.isGreaterThanOrEqualTo(new BigNumber(2000)) &&
    amount.isLessThan(4000)
  ) {
    return '2: 2 000 - 3 999€';
  } else if (
    amount.isGreaterThanOrEqualTo(new BigNumber(4000)) &&
    amount.isLessThan(8000)
  ) {
    return '3: 4 000 - 7 999€';
  } else if (
    amount.isGreaterThanOrEqualTo(new BigNumber(8000)) &&
    amount.isLessThan(15000)
  ) {
    return '4: 8 000 - 14 999€';
  } else if (
    amount.isGreaterThanOrEqualTo(new BigNumber(15000)) &&
    amount.isLessThan(25000)
  ) {
    return '5: 15 000 - 24 999€';
  } else if (
    amount.isGreaterThanOrEqualTo(new BigNumber(25000)) &&
    amount.isLessThan(new BigNumber(50000))
  ) {
    return '6: 25 000 - 49 999€';
  } else if (
    amount.isGreaterThanOrEqualTo(new BigNumber(50000)) &&
    amount.isLessThan(100000)
  ) {
    return '7: 50 000 - 99 999€';
  } else if (
    amount.isGreaterThanOrEqualTo(new BigNumber(100000)) &&
    amount.isLessThan(250000)
  ) {
    return '8: 100 000 - 249 999€';
  } else if (
    amount.isGreaterThanOrEqualTo(new BigNumber(250000)) &&
    amount.isLessThan(500000)
  ) {
    return '9: 250 000 - 499 999€';
  } else if (
    amount.isGreaterThanOrEqualTo(new BigNumber(500000)) &&
    amount.isLessThan(750000)
  ) {
    return '10: 500 000 - 749 999€';
  } else if (
    amount.isGreaterThanOrEqualTo(new BigNumber(750000)) &&
    amount.isLessThan(1000000)
  ) {
    return '11: 750 000 - 999 999€';
  } else if (
    amount.isGreaterThanOrEqualTo(new BigNumber(1000000)) &&
    amount.isLessThan(1500000)
  ) {
    return '12: 1 000 000 - 1 499 999€';
  } else if (
    amount.isGreaterThanOrEqualTo(new BigNumber(1500000)) &&
    amount.isLessThan(2999999)
  ) {
    return '13: 1 500 000 - 2 999 999€';
  } else if (amount.isGreaterThanOrEqualTo(new BigNumber(3000000))) {
    return '14: 3 000 000€ +';
  } else {
    return '';
  }
};
const removeProperty = (propKey: any, { [propKey]: propValue, ...rest }) => {
  const removedProps = [];
  removedProps.push(propValue);
  return rest;
};
const removeProperties = (object: any, ...keys: any[]) => {
  return keys.length
    ? removeProperties(removeProperty(keys.pop(), object), ...keys)
    : object;
};

const getEconomicYears = (
  parcelsEconomicInfoByYear: ParcelsEconomicInfoByYear,
  animalsEconomicInfoByYear: AnimalsEconomicInfoByYear,
) => {
  const economicYears: string[] = [];

  parcelsEconomicInfoByYear.forEach(
    (parcelEconomicInfo: ParcelsEconomicInfoByYear) => {
      if (!economicYears.includes(parcelEconomicInfo.period)) {
        return economicYears.push(parcelEconomicInfo.period);
      } else {
        return economicYears;
      }
    },
  );

  animalsEconomicInfoByYear.forEach(
    (animalEconomicInfo: AnimalsEconomicInfoByYear) => {
      if (!economicYears.includes(animalEconomicInfo.period)) {
        return economicYears.push(animalEconomicInfo.period);
      } else {
        return economicYears;
      }
    },
  );

  return economicYears;
};

export const getPossibleSocCodes = () => {
  return [
    'SOC_CLVS015',
    'SOC_CLVS008',
    'SOC_CLVS012',
    'SOC_CLND004',
    'SOC_CLND005',
    'SOC_CLND006',
    'SOC_CLND007',
    'SOC_CLND008',
    'SOC_CLND009',
    'SOC_CLND010_011_012',
    'SOC_CLND013',
    'SOC_CLND014',
    'SOC_CLND015',
    'SOC_CLND017',
    'SOC_CLND018',
    'SOC_CLND032',
    'SOC_CLND033',
    'SOC_CLND030',
    'SOC_CLND022',
    'SOC_CLND023',
    'SOC_CLND024',
    'SOC_CLND025',
    'SOC_CLND026',
    'SOC_CLND028',
    'SOC_CLND029',
    'SOC_CLND031',
    'SOC_CLND034',
    'SOC_CLND035_036',
    'SOC_CLND045',
    'SOC_CLND047',
    'SOC_CLND048_083',
    'SOC_CLND049',
    'SOC_CLND019',
    'SOC_CLND038',
    'SOC_CLND039',
    'SOC_CLND040',
    'SOC_CLND041_042',
    'SOC_CLND051',
    'SOC_CLND052',
    'SOC_CLND044',
    'SOC_CLND081',
    'SOC_CLND046',
    'SOC_CLND082',
    'SOC_CLND079',
    'SOC_CLND070',
    'SOC_CLND056',
    'SOC_CLND057',
    'SOC_CLND059',
    'SOC_CLND060',
    'SOC_CLND061',
    'SOC_CLND067',
    'SOC_CLND069',
    'SOC_CLND062',
    'SOC_CLND071',
    'SOC_CLVS001',
    'SOC_CLVS004',
    'SOC_CLVS007',
    'SOC_CLVS009',
    'SOC_CLVS011',
    'SOC_CLVS003',
    'SOC_CLVS005',
    'SOC_CLVS010',
    'SOC_CLVS013',
    'SOC_CLVS014',
    'SOC_CLVS016',
    'SOC_CLVS017',
    'SOC_CLVS018',
    'SOC_CLVS019',
    'SOC_CLVS020',
    'SOC_CLVS021',
    'SOC_CLVS022',
    'SOC_CLVS023',
    'SOC_CLVS029',
    'SOC_CLND055',
    'SOC_CLND061',
    'SOC_CLND084',
    'SOC_CLND037',
    'SOC_CLVS030',
  ];
};

const calculateSpecialistHorticultureSum = (socCodesMap: Map<any, any>) => {
  return socCodesMap
    .get('SOC_CLND044')
    .plus(socCodesMap.get('SOC_CLND081'))
    .plus(socCodesMap.get('SOC_CLND046'))
    .plus(socCodesMap.get('SOC_CLND082'))
    .plus(socCodesMap.get('SOC_CLND079'))
    .plus(socCodesMap.get('SOC_CLND070'));
};

const calculateSpecialistPermanentCropsSum = (socCodesMap: Map<any, any>) => {
  return socCodesMap
    .get('SOC_CLND055')
    .plus(socCodesMap.get('SOC_CLND056'))
    .plus(socCodesMap.get('SOC_CLND057'))
    .plus(socCodesMap.get('SOC_CLND059'))
    .plus(socCodesMap.get('SOC_CLND060'))
    .plus(socCodesMap.get('SOC_CLND061'))
    .plus(socCodesMap.get('SOC_CLND067'))
    .plus(socCodesMap.get('SOC_CLND069'))
    .plus(socCodesMap.get('SOC_CLND062'))
    .plus(socCodesMap.get('SOC_CLND071'))
    .plus(socCodesMap.get('SOC_CLND084'));
};

const calculateSpecialistGranivoresSum = (socCodesMap: Map<any, any>) => {
  return socCodesMap
    .get('SOC_CLVS018')
    .plus(socCodesMap.get('SOC_CLVS019'))
    .plus(socCodesMap.get('SOC_CLVS020'))
    .plus(socCodesMap.get('SOC_CLVS021'))
    .plus(socCodesMap.get('SOC_CLVS022'))
    .plus(socCodesMap.get('SOC_CLVS023'))
    .plus(socCodesMap.get('SOC_CLVS029'));
};

const calculateLiveStockAnimalsSum = (socCodesMap: Map<any, any>) => {
  return socCodesMap
    .get('SOC_CLVS001')
    .plus(socCodesMap.get('SOC_CLVS004'))
    .plus(socCodesMap.get('SOC_CLVS007'))
    .plus(socCodesMap.get('SOC_CLVS009'))
    .plus(socCodesMap.get('SOC_CLVS011'))
    .plus(socCodesMap.get('SOC_CLVS003'))
    .plus(socCodesMap.get('SOC_CLVS005'))
    .plus(socCodesMap.get('SOC_CLVS010'))
    .plus(socCodesMap.get('SOC_CLVS008'))
    .plus(socCodesMap.get('SOC_CLVS012'))
    .plus(socCodesMap.get('SOC_CLVS013'))
    .plus(socCodesMap.get('SOC_CLVS014'))
    .plus(socCodesMap.get('SOC_CLVS015'))
    .plus(socCodesMap.get('SOC_CLVS016'))
    .plus(socCodesMap.get('SOC_CLVS017'));
};

const calculateMeadowAndPastureSocCodesSum = (socCodesMap: Map<any, any>) => {
  return socCodesMap
    .get('SOC_CLND019')
    .plus(socCodesMap.get('SOC_CLND038'))
    .plus(socCodesMap.get('SOC_CLND039'))
    .plus(socCodesMap.get('SOC_CLND040'))
    .plus(socCodesMap.get('SOC_CLND041_042'))
    .plus(socCodesMap.get('SOC_CLND051'))
    .plus(socCodesMap.get('SOC_CLND052'));
};

const calculateSpecialistGrazingLivestockSum = (
  liveStockAnimalsSum: BigNumber,
  socCodesMap: Map<any, any>,
) => {
  if (!liveStockAnimalsSum.isZero()) {
    return liveStockAnimalsSum.plus(
      calculateMeadowAndPastureSocCodesSum(socCodesMap),
    );
  } else {
    return liveStockAnimalsSum;
  }
};

const calculateSpecialistFieldCropsSumExcludingLiveStockAnimals = (
  socCodesMap: Map<any, any>,
) => {
  return socCodesMap
    .get('SOC_CLND004')
    .plus(socCodesMap.get('SOC_CLND005'))
    .plus(socCodesMap.get('SOC_CLND006'))
    .plus(socCodesMap.get('SOC_CLND007'))
    .plus(socCodesMap.get('SOC_CLND008'))
    .plus(socCodesMap.get('SOC_CLND009'))
    .plus(socCodesMap.get('SOC_CLND010_011_012'))
    .plus(socCodesMap.get('SOC_CLND013'))
    .plus(socCodesMap.get('SOC_CLND014'))
    .plus(socCodesMap.get('SOC_CLND015'))
    .plus(socCodesMap.get('SOC_CLND017'))
    .plus(socCodesMap.get('SOC_CLND018'))
    .plus(socCodesMap.get('SOC_CLND032'))
    .plus(socCodesMap.get('SOC_CLND033'))
    .plus(socCodesMap.get('SOC_CLND030'))
    .plus(socCodesMap.get('SOC_CLND022'))
    .plus(socCodesMap.get('SOC_CLND023'))
    .plus(socCodesMap.get('SOC_CLND024'))
    .plus(socCodesMap.get('SOC_CLND025'))
    .plus(socCodesMap.get('SOC_CLND026'))
    .plus(socCodesMap.get('SOC_CLND028'))
    .plus(socCodesMap.get('SOC_CLND029'))
    .plus(socCodesMap.get('SOC_CLND031'))
    .plus(socCodesMap.get('SOC_CLND034'))
    .plus(socCodesMap.get('SOC_CLND035_036'))
    .plus(socCodesMap.get('SOC_CLND045'))
    .plus(socCodesMap.get('SOC_CLND047'))
    .plus(socCodesMap.get('SOC_CLND048_083'))
    .plus(socCodesMap.get('SOC_CLND049'));
};

const calculateSpecialistFieldCropsSum = (
  liveStockAnimalsSum: BigNumber,
  fieldCropsSumExcludingLiveStockAnimals: BigNumber,
  socCodesMap: Map<any, any>,
) => {
  if (liveStockAnimalsSum.isZero()) {
    return fieldCropsSumExcludingLiveStockAnimals.plus(
      calculateMeadowAndPastureSocCodesSum(socCodesMap),
    );
  } else {
    return fieldCropsSumExcludingLiveStockAnimals;
  }
};

const calculateProductionType = (
  economicSumsByGroup: EconomicSumsByGroup,
  parcelsAndAnimalsEconomicTotal: BigNumber,
) => {
  const {
    specialistHorticultureSum,
    specialistPermanentCropsSum,
    specialistGranivoresSum,
    specialistGrazingLivestockSum,
    specialistFieldCropsSum,
  } = economicSumsByGroup;

  const twoThirdsOfEconomicTotal = parcelsAndAnimalsEconomicTotal
    .dividedBy(new BigNumber(3))
    .plus(parcelsAndAnimalsEconomicTotal.dividedBy(new BigNumber(3)));

  if (specialistFieldCropsSum.isGreaterThan(twoThirdsOfEconomicTotal)) {
    return {
      productionTypeEn: '1: specialist field crops',
      productionTypeEt: '1: taimekasvatus',
    };
  } else if (
    specialistHorticultureSum.isGreaterThan(twoThirdsOfEconomicTotal)
  ) {
    return {
      productionTypeEn: '2: specialist horticulture',
      productionTypeEt: '2: aiandus',
    };
  } else if (
    specialistPermanentCropsSum.isGreaterThan(twoThirdsOfEconomicTotal)
  ) {
    return {
      productionTypeEn: '3: specialist permanent crops',
      productionTypeEt: '3: püsikultuurid',
    };
  } else if (
    specialistGrazingLivestockSum.isGreaterThan(twoThirdsOfEconomicTotal)
  ) {
    return {
      productionTypeEn: '4: specialist grazing livestock',
      productionTypeEt: '4: karjatatavad loomad',
    };
  } else if (specialistGranivoresSum.isGreaterThan(twoThirdsOfEconomicTotal)) {
    return {
      productionTypeEn: '5: specialist granivores',
      productionTypeEt: '5: teratoiduliste kasvatamine',
    };
  } else if (
    specialistFieldCropsSum
      .plus(specialistHorticultureSum)
      .plus(specialistPermanentCropsSum)
      .isGreaterThan(twoThirdsOfEconomicTotal) &&
    specialistFieldCropsSum.isLessThanOrEqualTo(twoThirdsOfEconomicTotal) &&
    specialistHorticultureSum.isLessThanOrEqualTo(twoThirdsOfEconomicTotal) &&
    specialistPermanentCropsSum.isLessThanOrEqualTo(twoThirdsOfEconomicTotal)
  ) {
    return {
      productionTypeEn: '6: mixed cropping',
      productionTypeEt: '6: segataimekasvatus',
    };
  } else if (
    specialistGrazingLivestockSum
      .plus(specialistGranivoresSum)
      .isGreaterThan(twoThirdsOfEconomicTotal) &&
    specialistGrazingLivestockSum.isLessThanOrEqualTo(
      twoThirdsOfEconomicTotal,
    ) &&
    specialistGranivoresSum.isLessThanOrEqualTo(twoThirdsOfEconomicTotal)
  ) {
    return {
      productionTypeEn: '7: mixed livestock',
      productionTypeEt: '7: segaloomakasvatus',
    };
  } else if (parcelsAndAnimalsEconomicTotal.isZero()) {
    return {
      productionTypeEn: '9: non-classified holdings',
      productionTypeEt: '9: määratlemata',
    };
  } else {
    return {
      productionTypeEn: '8: mixed crops – livestock',
      productionTypeEt: '8: segatootmine',
    };
  }
};

const getProductionType = (
  socCodes: SocCodes,
  parcelsAndAnimalsEconomicTotal: BigNumber,
) => {
  const possibleSocCodes = getPossibleSocCodes();
  const socCodesMap: Map<any, any> = new Map();

  possibleSocCodes.forEach((socCode) => {
    return socCodesMap.set(socCode, new BigNumber(0));
  });

  const socKeys = Object.keys(socCodes);
  socKeys.forEach((socKey) => {
    return socCodesMap.set(socKey, socCodes[socKey]);
  });

  const specialistHorticultureSum = calculateSpecialistHorticultureSum(
    socCodesMap,
  );

  const specialistPermanentCropsSum = calculateSpecialistPermanentCropsSum(
    socCodesMap,
  );

  const specialistGranivoresSum = calculateSpecialistGranivoresSum(socCodesMap);

  const liveStockAnimalsSum = calculateLiveStockAnimalsSum(socCodesMap);

  const fieldCropsSumExcludingLiveStockAnimals = calculateSpecialistFieldCropsSumExcludingLiveStockAnimals(
    socCodesMap,
  );
  const specialistGrazingLivestockSum = calculateSpecialistGrazingLivestockSum(
    liveStockAnimalsSum,
    socCodesMap,
  );

  const specialistFieldCropsSum = calculateSpecialistFieldCropsSum(
    liveStockAnimalsSum,
    fieldCropsSumExcludingLiveStockAnimals,
    socCodesMap,
  );

  const economicSumsByGroup = {
    specialistHorticultureSum,
    specialistPermanentCropsSum,
    specialistGranivoresSum,
    specialistGrazingLivestockSum,
    specialistFieldCropsSum,
  };

  return calculateProductionType(
    economicSumsByGroup,
    parcelsAndAnimalsEconomicTotal,
  );
};

const sortEconomicInfoByYear = (parcels: any, animals: SortedAnimalsData) => {
  const { parcelsEconomicInfoByYear } = parcels;
  const { animalsEconomicInfoByYear } = animals;

  const economicYears = getEconomicYears(
    parcelsEconomicInfoByYear,
    animalsEconomicInfoByYear,
  );

  const economicInfoByYear: EconomicInfoByYear[] = [];
  const economicInfoByYearEt: EconomicInfoByYearL[] = [];
  const economicInfoByYearEn: EconomicInfoByYearL[] = [];

  economicYears.forEach((economicYear) => {
    const parcelEconomicInfo = parcelsEconomicInfoByYear.find(
      (economicInfo: ParcelsEconomicInfoByYear) =>
        economicInfo.period === economicYear,
    );

    const animalEconomicInfo = animalsEconomicInfoByYear.find(
      (economicInfo: AnimalsEconomicInfoByYear) => {
        return economicInfo.period === economicYear;
      },
    );

    let parcelsAndAnimalsEconomicTotal = new BigNumber(0);
    let socCodes = {};
    if (parcelEconomicInfo && animalEconomicInfo) {
      parcelsAndAnimalsEconomicTotal = parcelEconomicInfo.economicAmount.plus(
        animalEconomicInfo.economicAmount,
      );
      socCodes = {
        ...removeProperties(parcelEconomicInfo, 'period', 'economicAmount'),
        ...removeProperties(animalEconomicInfo, 'period', 'economicAmount'),
      };
    } else if (parcelEconomicInfo) {
      parcelsAndAnimalsEconomicTotal = parcelEconomicInfo.economicAmount;

      socCodes = {
        ...removeProperties(parcelEconomicInfo, 'period', 'economicAmount'),
      };
    } else {
      parcelsAndAnimalsEconomicTotal = animalEconomicInfo.economicAmount;
      socCodes = {
        ...removeProperties(animalEconomicInfo, 'period', 'economicAmount'),
      };
    }

    const productionType = getProductionType(
      socCodes,
      parcelsAndAnimalsEconomicTotal,
    );
    const { productionTypeEn, productionTypeEt } = productionType;

    const economicClass = calculateEconomicClass(
      parcelsAndAnimalsEconomicTotal,
    );

    const economicInfo = {
      period: economicYear,
      value: parcelsAndAnimalsEconomicTotal.toNumber(),
      economicClass,
    };

    economicInfoByYearEt.push({
      ...economicInfo,
      name: 'Põllumajandusliku standardkogutoodangu väärtus',
      productionType: productionTypeEt,
      productionTypeEn,
      productionTypeEt,
    });

    economicInfoByYearEn.push({
      ...economicInfo,
      name: 'Total standard output of agricultural holding',
      productionType: productionTypeEn,
      productionTypeEn,
      productionTypeEt,
    });

    economicInfoByYear.push({
      ...economicInfo,
      period: economicInfo.period,
      nameEt: 'Total standard output of agricultural holding',
      nameEn: 'Total standard output of agricultural holding',
      productionTypeEn,
      productionTypeEt,
    });
  });

  return { economicInfoByYearEt, economicInfoByYearEn, economicInfoByYear };
};

export const sortEconomicInfo = (parcels: any, animals: any) => {
  const economicInfo = sortEconomicInfoByYear(parcels, animals);
  const {
    economicInfoByYearEt,
    economicInfoByYearEn,
    economicInfoByYear,
  } = economicInfo;
  return {
    economicInfoByYearEt,
    economicInfoByYearEn,
    economicInfoByYear,
    unit: 'eur',
  };
};

export const setClientsData = async (
  punId: string,
  queryManager: EntityManager,
) => {
  const parcels = await sortParcelsDataForEconomicGroupCalculations(
    punId,
    queryManager,
  );
  const animals = await sortAnimalsDataForEconomicGroupCalculations(
    punId,
    queryManager,
  );

  const economicInfo = sortEconomicInfo(parcels, animals);
  const { parcelsByYear, parcelsData } = parcels;
  const { economicInfoByYearEn: economicInfoByYear } = economicInfo;

  await sortAverageParcelSizesByYearAndGroup(parcelsByYear, economicInfoByYear);
  await sortParcelsLandTypeDataByYearAndGroup(parcelsData, economicInfoByYear);
};
