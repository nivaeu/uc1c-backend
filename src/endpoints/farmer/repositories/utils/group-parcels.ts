/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import BigNumber from 'bignumber.js';
import {
  queryRfpId,
  queryAblIdsAndKeys,
  queryParcelHectares,
  queryParcelCultureData,
  queryGeneralParcelData,
  queryParcelSocCode,
  queryCultureValue,
  queryParcelType,
  getLandType,
} from './parcels';
import { calculateEconomicAmount } from './economicClass';
import { AbDagrblock as ab_dagrblock } from '../../../../entities/schemas/sc_niva/ab_dagrblock.entity';
import { RpDrefplot as rp_drefplot } from '../../../../entities/schemas/sc_niva/rp_drefplot.entity';
import { EntityManager } from 'typeorm';
import { ParcelsEconomicInfoByYear, AbDagrblockQueryData } from './parcels';

interface LandTypes {
  landTypeEn: string;
  landTypeEt: string;
}

export interface ParcelsDataForEconomicGroupCalculations {
  period: string;
  hectares: BigNumber;
  clanNameEt: string;
  clanNameEn: string;
  economicAmount: BigNumber;
  soc: string;
  landType: LandTypes;
}

export interface ParcelsByYearForEconomicGroupCalculations {
  period: string;
  nameEt: string;
  nameEn: string;
  value: any;
  total: BigNumber;
  unit: string;
}

export interface SortedParcelsDataForEconomicGroupCalculations {
  parcelsEconomicInfoByYear: ParcelsEconomicInfoByYear[];
  parcelsByYear: ParcelsByYearForEconomicGroupCalculations[];
  parcelsData: ParcelsDataForEconomicGroupCalculations[];
}

export const queryParcelsDataForEconomicGroupCalculations = async (
  punId: string,
  queryManager: EntityManager,
) => {
  const parcelsData: ParcelsDataForEconomicGroupCalculations[] = [];
  const ablIdsAndKeys = await queryAblIdsAndKeys(ab_dagrblock, punId);

  await Promise.all(
    ablIdsAndKeys.map(async (abl: AbDagrblockQueryData) => {
      const { ablId } = abl;
      const rfpId = await queryRfpId(rp_drefplot, ablId);
      const generalParcelData = await queryGeneralParcelData(
        rfpId,
        queryManager,
      );

      if (generalParcelData.length !== 0) {
        await Promise.all(
          generalParcelData.map(async (parcelData) => {
            const { crpId, period, rcpId } = parcelData;

            const hectares = await queryParcelHectares(rcpId, queryManager);

            const parcelCultureData = await queryParcelCultureData(
              crpId,
              queryManager,
            );

            const { clanId, soId, socId } = parcelCultureData;

            const soc = await queryParcelSocCode(socId, queryManager);
            const cultureSoValue = await queryCultureValue(soId, queryManager);
            const { soValue } = cultureSoValue;
            const economicAmount = calculateEconomicAmount(hectares, soValue);
            const parcelType = await queryParcelType(clanId, queryManager);
            const { clanNameEt, clanNameEn } = parcelType;
            const landType = getLandType(clanNameEn);

            const parcel = {
              period,
              hectares,
              clanNameEt,
              clanNameEn,
              economicAmount,
              soc,
              landType,
            };

            parcelsData.push(parcel);
          }),
        );
      }
    }),
  );
  return parcelsData;
};
