/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import BigNumber from 'bignumber.js';

export const sortAndCalculateLivestockLoadByYear = (parcels, animals) => {
  const { animalsEn, animalsEt } = animals;
  const { parcelsByYearEn } = parcels;
  const hectaresByYear = [];

  parcelsByYearEn.forEach((parcel) => {
    const yearIndex = hectaresByYear.findIndex((data) => {
      return parcel.period === data.period;
    });

    if (yearIndex !== -1) {
      if (hectaresByYear[yearIndex].hectares) {
        hectaresByYear[yearIndex].hectares = hectaresByYear[
          yearIndex
        ].hectares.plus(new BigNumber(parcel.value));
      } else {
        hectaresByYear[yearIndex].hectares = new BigNumber(parcel.value);
      }
    } else {
      hectaresByYear.push({
        period: parcel.period,
        hectares: new BigNumber(parcel.value),
      });
    }
  });

  const livestockLoadData = {
    livestockLoadDataEn: [],
    livestockLoadDataEt: [],
  };

  animalsEn.forEach((animal) => {
    const index = livestockLoadData.livestockLoadDataEn.findIndex((data) => {
      return animal.period === data.period && animal.name === data.name;
    });

    const yearIndex = hectaresByYear.findIndex(
      (data) => data.period === animal.period,
    );

    if (index !== -1) {
      if (livestockLoadData.livestockLoadDataEn[index].livestock) {
        livestockLoadData.livestockLoadDataEn[
          index
        ].livestock = livestockLoadData.livestockLoadDataEn[
          index
        ].livestock.plus(animal.livestock);
      } else {
        livestockLoadData.livestockLoadDataEn[
          index
        ].livestock = livestockLoadData.livestockLoadDataEn[index].livestock =
          animal.livestock;
      }
    } else {
      if (typeof hectaresByYear[yearIndex] !== 'undefined') {
        livestockLoadData.livestockLoadDataEn.push({
          period: animal.period,
          livestock: animal.livestock,
          name: animal.name,
          hectares: hectaresByYear[yearIndex].hectares,
        });
      }
    }
  });

  animalsEt.forEach((animal) => {
    const index = livestockLoadData.livestockLoadDataEt.findIndex((data) => {
      return animal.period === data.period && animal.name === data.name;
    });

    const yearIndex = hectaresByYear.findIndex(
      (data) => data.period === animal.period,
    );

    if (index !== -1) {
      if (livestockLoadData.livestockLoadDataEt[index].livestock) {
        livestockLoadData.livestockLoadDataEt[
          index
        ].livestock = livestockLoadData.livestockLoadDataEt[
          index
        ].livestock.plus(animal.livestock);
      } else {
        livestockLoadData.livestockLoadDataEt[
          index
        ].livestock = livestockLoadData.livestockLoadDataEt[index].livestock =
          animal.livestock;
      }
    } else {
      if (typeof hectaresByYear[yearIndex] !== 'undefined') {
        livestockLoadData.livestockLoadDataEt.push({
          period: animal.period,
          livestock: animal.livestock,
          name: animal.name,
          hectares: hectaresByYear[yearIndex].hectares,
        });
      }
    }
  });

  livestockLoadData.livestockLoadDataEn.forEach((data) => {
    data.value = data.hectares.dividedBy(data.livestock);
    data.value = data.value.decimalPlaces(2).toNumber();
  });

  livestockLoadData.livestockLoadDataEt.forEach((data) => {
    data.value = data.hectares.dividedBy(data.livestock);
    data.value = data.value.decimalPlaces(2).toNumber();
  });

  return livestockLoadData;
};

const setLivestockLoadTableData = (
  livestockLoadDataEn,
  livestockLoadDataEt,
) => {
  const livestockTableDataEt = [];

  livestockLoadDataEt.forEach((livestockData) => {
    const livestockTableDataIndexEt = livestockTableDataEt.findIndex(
      (livestockTableData) => {
        return livestockData.period === livestockTableData.period;
      },
    );

    const livestockLoadValue = new BigNumber(livestockData.value);

    if (livestockTableDataIndexEt !== -1) {
      livestockTableDataEt[
        livestockTableDataIndexEt
      ].totals[1] = livestockTableDataEt[
        livestockTableDataIndexEt
      ].totals[1].plus(livestockLoadValue);
    } else {
      livestockTableDataEt.push({
        period: livestockData.period,
        headers: ['Loomaliik', 'Loomkoormus'],
        unitsData: [[livestockData.name, livestockLoadValue]],
        totals: ['', livestockLoadValue],
      });
    }
  });
  const livestockTableDataEn = [];

  livestockLoadDataEn.forEach((livestockData) => {
    const livestockTableDataIndexEn = livestockTableDataEn.findIndex(
      (livestockTableData) => {
        return livestockData.period === livestockTableData.period;
      },
    );

    const livestockLoadValue = new BigNumber(livestockData.value);

    if (livestockTableDataIndexEn !== -1) {
      livestockTableDataEn[
        livestockTableDataIndexEn
      ].totals[1] = livestockTableDataEn[
        livestockTableDataIndexEn
      ].totals[1].plus(livestockLoadValue);
    } else {
      livestockTableDataEn.push({
        period: livestockData.period,
        headers: ['Animal Species', 'Livestock Load'],
        unitsData: [[livestockData.name, livestockLoadValue]],
        totals: ['', livestockLoadValue],
      });
    }
  });

  return {
    livestockTableDataEt,
    livestockTableDataEn,
  };
};

export const sortLivestockData = (parcels, animals) => {
  const livestockLoadByYear = sortAndCalculateLivestockLoadByYear(
    parcels,
    animals,
  );

  const { livestockLoadDataEn, livestockLoadDataEt } = livestockLoadByYear;
  const livestockLoadTableData = setLivestockLoadTableData(
    livestockLoadDataEn,
    livestockLoadDataEt,
  );
  const { livestockTableDataEt, livestockTableDataEn } = livestockLoadTableData;

  return {
    livestockLoadDataEn,
    livestockLoadDataEt,
    livestockTableDataEt,
    livestockTableDataEn,
    unitEn: 'livestock load',
    unitEt: 'loomkoormus',
  };
};
