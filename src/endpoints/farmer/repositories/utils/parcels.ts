/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import BigNumber from 'bignumber.js';
import { AbDagrblock as ab_dagrblock } from '../../../../entities/schemas/sc_niva/ab_dagrblock.entity';
import { RpDrefplot as rp_drefplot } from '../../../../entities/schemas/sc_niva/rp_drefplot.entity';
import { calculateEconomicAmount } from './economicClass';
import { queryPlantFertilizers } from './plantFertilizers';
import { queryPlantProtectionChemicals } from './plantChemicals';
import { queryAgrotechnics } from './agrotechnics';
import { store } from '../../../../store/store';
import { calculateAverageOfNumber } from './utils';
import {
  queryParcelsDataForEconomicGroupCalculations,
  ParcelsDataForEconomicGroupCalculations,
} from './group-parcels';
import { EntityManager, getConnection } from 'typeorm';
import { AverageParcelSizes } from '../../../../entities/schemas/sc_niva_extra/economic-group-calculations/avg_parcel_sizes.entity';
import { LandTypeData } from '../../../../entities/schemas/sc_niva_extra/economic-group-calculations/land_type_data.entity';

export interface Parcel {
  period: string;
  rcpKey: string;
  hectares: BigNumber;
  cpgCoordinates: any;
  clanNameEt: string;
  clanNameEn: string;
  clanNameShortEn: string;
  clanNameShortEt: string;
  crpNameEt: string;
  crpNameEn: string;
  crpBotanicalNameEt: string;
  crpBotanicalNameEn: string;
  isArableLand: boolean;
  landType: any;
  ablKey: string;
  economicAmount: BigNumber;
  plantProtectionChemicals: any;
  plantFertilizers: any;
  agrotechnics: any;
  soc: string;
  parcelYield: number;
}

export interface Parcels {
  parcelsData: Parcel[];
}

export interface ParcelsEconomicInfoByYear {
  period: string;
  economicAmount: BigNumber;
  [socCode: string]: any;
}

interface ParcelsByYear {
  period: string;
  name: string;
  shortName: string;
  value: BigNumber;
  total: BigNumber;
  unit: string;
  cultures: any;
}

interface ParcelTableData {
  period: string;
  headers: string[];
  unitsData: any;
  totals: any;
  totalHectares: BigNumber;
  arableLandCultures: string[];
  arableLandCulturesHectares: BigNumber;
  clientDataTableHeading: string;
  benchmarkingTableHeading: string;
}

interface ParcelsLandTypeData {
  period: string;
  landTypeEn: string;
  landTypeEt: string;
  hectares: BigNumber;
  totalLandSize: BigNumber;
}

export interface SortedParcelsData {
  parcelsData: Parcels;
  parcelsLandTypeData: ParcelsLandTypeData;
  unit: string;
  parcelsEconomicInfoByYear: ParcelsEconomicInfoByYear;
  parcelsByYearEt: ParcelsByYear;
  parcelsByYearEn: ParcelsByYear;
  parcelTableDataEt: ParcelTableData;
  parcelTableDataEn: ParcelTableData;
}

export interface AbDagrblockQueryData {
  ablId: string;
  ablKey: string;
}

interface ParcelsByYearForEconomicGroupCalculations {
  period: string;
  nameEt: string;
  nameEn: string;
  value: BigNumber;
  total: BigNumber;
  unit: string;
}

export const checkIfArableLand = (clanNameEn: string) => {
  if (clanNameEn === 'Crops' || clanNameEn === 'Fallow land') {
    return true;
  } else {
    return false;
  }
};

export const getLandType = (clanNameEn: string) => {
  if (clanNameEn === 'Crops' || clanNameEn === 'Fallow land') {
    return {
      landTypeEn: 'Arable Land',
      landTypeEt: 'Põllumaa',
    };
  } else if (
    clanNameEn === 'Permanent grassland' ||
    clanNameEn === 'Restored grassland' ||
    clanNameEn === 'Environmentally sensitive permanent grassland' ||
    clanNameEn === 'Rough grazings'
  ) {
    return {
      landTypeEn: 'Permanent grassland',
      landTypeEt: 'Püsirohumaa',
    };
  } else {
    return {
      landTypeEn: 'Permanent crops',
      landTypeEt: 'Püsikultuurid',
    };
  }
};

export interface AblIdsAndKeysQueryData {
  ablId: string;
  ablKey: string;
}

export const queryAblIdsAndKeys = async (ab_dagrblock: any, punId: string) => {
  return await ab_dagrblock.find({
    select: ['ablId', 'ablKey'],
    where: { punId },
  });
};

export const queryRfpId = async (rp_drefplot: any, ablId: string) => {
  const rfpIdQuery = await rp_drefplot.find({
    select: ['rfpId'],
    where: { abl: ablId },
  });

  return rfpIdQuery[0].rfpId;
};

export const queryParcelHectares = async (
  rcpId: string,
  queryManager: EntityManager,
) => {
  const cpgSurfaceQuery = await queryManager.query(
    `SELECT cpg_surface FROM sc_niva.rp_dcropparcelgeom WHERE rcp_id = '${rcpId}'`,
  );

  let { cpg_surface: hectares } = cpgSurfaceQuery[0];
  hectares = new BigNumber(hectares);
  return hectares;
};

const queryCpgCoordinates = async (
  rcpId: string,
  queryManager: EntityManager,
) => {
  return await queryManager.query(
    `SELECT
    ST_X((point).geom) as lng,
    ST_Y((point).geom) as lat
  FROM
    (SELECT
       ST_DumpPoints(ST_Transform(ST_SetSRID(cpg_geometry, 3301), 4326)) as point
    FROM 
       (SELECT
         cpg_geometry,
         cpg_surface
       FROM
         sc_niva.rp_dcropparcelgeom WHERE rcp_id = '${rcpId}') g) a; `,
  );
};

export const queryParcelCultureData = async (
  crpId: string,
  queryManager: EntityManager,
) => {
  const pCropProductQuery = await queryManager.query(
    `SELECT clan_id, crp_name_et, crp_name_en, crp_botanicalname_et, crp_botanicalname_en, so_id, soc_id FROM sc_niva_extra.loc_co_pcropproduct WHERE crp_id = '${crpId}'`,
  );

  const {
    clan_id: clanId,
    crp_name_et: crpNameEt,
    crp_name_en: crpNameEn,
    crp_botanicalname_et: crpBotanicalNameEt,
    crp_botanicalname_en: crpBotanicalNameEn,
    so_id: soId,
    soc_id: socId,
  } = pCropProductQuery[0];

  return {
    clanId,
    crpNameEt,
    crpNameEn,
    crpBotanicalNameEt,
    crpBotanicalNameEn,
    soId,
    socId,
  };
};

export const queryCultureValue = async (
  soId: string,
  queryManager: EntityManager,
) => {
  const coStandardOutputQuery = await queryManager.query(
    `SELECT so_value FROM sc_niva_extra.co_standardoutput WHERE so_id = '${soId}'`,
  );
  let { so_value: soValue } = coStandardOutputQuery[0];
  soValue = new BigNumber(soValue);

  return {
    soValue,
  };
};

export const queryParcelSocCode = async (
  socId: string,
  queryManager: EntityManager,
) => {
  const parcelSocCodeQuery = await queryManager.query(
    `SELECT soc FROM sc_niva_extra.COU_CROPPRODUCT_SOC WHERE soc_id = '${socId}'`,
  );

  const { soc } = parcelSocCodeQuery[0];
  return soc;
};

export const queryParcelType = async (
  clanId: string,
  queryManager: EntityManager,
) => {
  const clanNameQuery = await queryManager.query(
    `SELECT clan_name_et, clan_name_en, clan_name_short_en, clan_name_short_et FROM sc_niva_extra.co_pcroplandcover WHERE clan_id = '${clanId}'`,
  );

  const {
    clan_name_et: clanNameEt,
    clan_name_en: clanNameEn,
    clan_name_short_en: clanNameShortEn,
    clan_name_short_et: clanNameShortEt,
  } = clanNameQuery[0];

  return {
    clanNameEt,
    clanNameEn,
    clanNameShortEn,
    clanNameShortEt,
  };
};

export const queryGeneralParcelData = async (
  rfpId: string,
  queryManager: EntityManager,
) => {
  const dCropParcelQuery = await queryManager.query(
    `SELECT crp_id, crv_id, rcp_datefrom, rcp_id, rcp_key FROM sc_niva.rp_dcropparcel WHERE rfp_id = '${rfpId}'`,
  );

  const generalParcelsData = [];

  if (dCropParcelQuery.length !== 0) {
    dCropParcelQuery.forEach((parcel) => {
      const {
        crp_id: crpId,
        crv_id: crvId,
        rcp_id: rcpId,
        rcp_key: rcpKey,
      } = parcel;

      let { rcp_datefrom: period } = parcel;
      period = period.getFullYear().toString();
      generalParcelsData.push({
        crpId,
        crvId,
        rcpId,
        rcpKey,
        period,
      });
    });
  }

  return generalParcelsData;
};

const queryParcelYieldUnit = async (uniId, queryManager) => {
  const parcelYieldUnitQuery = await queryManager.query(
    `select unit_name_et, unit_name_en FROM sc_niva_extra.loc_co_punittype WHERE uni_id = '${uniId}'`,
  );

  const {
    unit_name_et: unitNameEt,
    unit_name_en: unitNameEn,
  } = parcelYieldUnitQuery[0];

  return { unitNameEt, unitNameEn };
};

const queryParcelYield = async (rcpId, queryManager) => {
  const parcelYieldQuery = await queryManager.query(
    `SELECT cry_planned_amount, cry_actual_amount, uni_id FROM sc_niva_extra.RP_DCROPYIELD WHERE rcp_id = '${rcpId}'`,
  );

  if (parcelYieldQuery.length !== 0) {
    const {
      cry_planned_amount: cryPlannedAmount,
      cry_actual_amount: cryActualAmount,
      uni_id: uniId,
    } = parcelYieldQuery[0];

    const parcelYieldUnit = await queryParcelYieldUnit(uniId, queryManager);
    const { unitNameEt, unitNameEn } = parcelYieldUnit;

    return {
      cryPlannedAmount,
      cryActualAmount,
      unitNameEt,
      unitNameEn,
    };
  }
};

export const queryParcelsData = async (punId, queryManager) => {
  const parcelsData = [];
  const ablIdsAndKeys = await queryAblIdsAndKeys(ab_dagrblock, punId);

  await Promise.all(
    ablIdsAndKeys.map(async (abl: AbDagrblockQueryData) => {
      const { ablId, ablKey } = abl;
      const rfpId = await queryRfpId(rp_drefplot, ablId);
      const generalParcelData = await queryGeneralParcelData(
        rfpId,
        queryManager,
      );

      if (generalParcelData.length !== 0) {
        await Promise.all(
          generalParcelData.map(async (parcelData) => {
            const { crpId, period, rcpId, rcpKey } = parcelData;

            const parcelYield = await queryParcelYield(rcpId, queryManager);

            const agrotechnics = await queryAgrotechnics(
              rcpId,
              rcpKey,
              queryManager,
            );

            const plantFertilizers = await queryPlantFertilizers(
              rcpId,
              queryManager,
            );

            const plantProtectionChemicals = await queryPlantProtectionChemicals(
              rcpId,
              queryManager,
            );
            const cpgCoordinates = await queryCpgCoordinates(
              rcpId,
              queryManager,
            );
            const hectares = await queryParcelHectares(rcpId, queryManager);

            const parcelCultureData = await queryParcelCultureData(
              crpId,
              queryManager,
            );

            const {
              clanId,
              crpNameEt,
              crpNameEn,
              soId,
              socId,
              crpBotanicalNameEt,
              crpBotanicalNameEn,
            } = parcelCultureData;

            const soc = await queryParcelSocCode(socId, queryManager);
            const cultureSoValue = await queryCultureValue(soId, queryManager);
            const { soValue } = cultureSoValue;
            const economicAmount = calculateEconomicAmount(hectares, soValue);
            const parcelType = await queryParcelType(clanId, queryManager);

            const {
              clanNameEt,
              clanNameEn,
              clanNameShortEn,
              clanNameShortEt,
            } = parcelType;

            const isArableLand = checkIfArableLand(clanNameEn);
            const landType = getLandType(clanNameEn);

            const parcel = {
              period,
              rcpKey,
              hectares,
              cpgCoordinates,
              clanNameEt,
              clanNameEn,
              clanNameShortEn,
              clanNameShortEt,
              crpNameEt,
              crpNameEn,
              crpBotanicalNameEt,
              crpBotanicalNameEn,
              isArableLand,
              landType,
              ablKey,
              economicAmount,
              plantProtectionChemicals,
              plantFertilizers,
              agrotechnics,
              soc,
              parcelYield,
            };

            parcelsData.push(parcel);
          }),
        );
      }
    }),
  );

  return parcelsData;
};

const sortParcelsByYear = (parcelsData) => {
  const parcelsByYearEt: ParcelsByYear[] = [];
  const parcelsByYearEn: ParcelsByYear[] = [];

  parcelsData.forEach((parcel) => {
    const indexEt = parcelsByYearEt.findIndex((parcelItem) => {
      return (
        parcelItem.period === parcel.period &&
        parcelItem.name === parcel.clanNameEt
      );
    });

    if (indexEt !== -1) {
      parcelsByYearEt[indexEt].value = parcelsByYearEt[indexEt].value.plus(
        parcel.hectares,
      );

      parcelsByYearEt[indexEt].total = parcelsByYearEt[indexEt].total.plus(
        new BigNumber(1),
      );

      if (parcelsByYearEt[indexEt].cultures[parcel.crpBotanicalNameEt]) {
        parcelsByYearEt[indexEt].cultures[
          parcel.crpBotanicalNameEt
        ] = parcelsByYearEt[indexEt].cultures[parcel.crpBotanicalNameEt].plus(
          parcel.hectares,
        );
      } else {
        parcelsByYearEt[indexEt].cultures[parcel.crpBotanicalNameEt] =
          parcel.hectares;
      }
    } else {
      parcelsByYearEt.push({
        period: parcel.period,
        name: parcel.clanNameEt,
        shortName: parcel.clanNameShortEt,
        value: parcel.hectares,
        total: new BigNumber(1),
        unit: 'ha',
        cultures: {
          [parcel.crpBotanicalNameEt]: parcel.hectares,
        },
      });
    }

    const indexEn = parcelsByYearEn.findIndex((parcelItem) => {
      return (
        parcelItem.period === parcel.period &&
        parcelItem.name === parcel.clanNameEn
      );
    });

    if (indexEn !== -1) {
      parcelsByYearEn[indexEn].value = parcelsByYearEn[indexEn].value.plus(
        parcel.hectares,
      );

      parcelsByYearEn[indexEn].total = parcelsByYearEn[indexEn].total.plus(
        new BigNumber(1),
      );

      if (parcelsByYearEn[indexEn].cultures[parcel.crpBotanicalNameEn]) {
        parcelsByYearEn[indexEn].cultures[
          parcel.crpBotanicalNameEn
        ] = parcelsByYearEn[indexEn].cultures[parcel.crpBotanicalNameEn].plus(
          parcel.hectares,
        );
      } else {
        parcelsByYearEn[indexEn].cultures[parcel.crpBotanicalNameEn] =
          parcel.hectares;
      }
    } else {
      parcelsByYearEn.push({
        period: parcel.period,
        name: parcel.clanNameEn,
        shortName: parcel.clanNameShortEn,
        value: parcel.hectares,
        total: new BigNumber(1),
        unit: 'ha',
        cultures: {
          [parcel.crpBotanicalNameEn]: parcel.hectares,
        },
      });
    }
  });

  return { parcelsByYearEt, parcelsByYearEn };
};

const sortParcelsPieChartTableData = (parcelsData) => {
  const parcelTableDataEt: ParcelTableData[] = [];
  const parcelTableDataEn: ParcelTableData[] = [];

  parcelsData.forEach((cropItem) => {
    const parcelTableDataIndexEt = parcelTableDataEt.findIndex((crop) => {
      return crop.period === cropItem.period;
    });

    if (parcelTableDataIndexEt !== -1) {
      parcelTableDataEt[parcelTableDataIndexEt].totals[1] = parcelTableDataEt[
        parcelTableDataIndexEt
      ].totals[1].plus(cropItem.hectares);

      parcelTableDataEt[parcelTableDataIndexEt].totals[2] = parcelTableDataEt[
        parcelTableDataIndexEt
      ].totals[2].plus(new BigNumber(1));

      parcelTableDataEt[
        parcelTableDataIndexEt
      ].totalHectares = parcelTableDataEt[
        parcelTableDataIndexEt
      ].totalHectares.plus(cropItem.hectares);

      if (cropItem.isArableLand) {
        parcelTableDataEt[
          parcelTableDataIndexEt
        ].arableLandCulturesHectares = parcelTableDataEt[
          parcelTableDataIndexEt
        ].arableLandCulturesHectares.plus(cropItem.hectares);

        if (
          !parcelTableDataEt[
            parcelTableDataIndexEt
          ].arableLandCultures.includes(cropItem.crpNameEt)
        ) {
          parcelTableDataEt[parcelTableDataIndexEt].arableLandCultures.push(
            cropItem.crpNameEt,
          );
        }
      }
    } else {
      const headers = [
        'Maakasutustüüp',
        'Pindala kokku (ha)',
        'Põlde kokku',
        'Põllu keskmine suurus (ha)',
        'Põllu keskmine suurus minu grupis (ha)',
      ];
      const tableHeadings = {
        clientDataTableHeading: 'Kasutatav põllumajandusmaa',
        benchmarkingTableHeading: 'Võrdlusandmed',
      };

      if (cropItem.isArableLand) {
        parcelTableDataEt.push({
          period: cropItem.period,
          headers,
          unitsData: [],
          totals: ['', cropItem.hectares, new BigNumber(1), '-', '-'],
          totalHectares: cropItem.hectares,
          arableLandCultures: [cropItem.crpNameEt],
          arableLandCulturesHectares: cropItem.hectares,
          ...tableHeadings,
        });
      } else {
        parcelTableDataEt.push({
          period: cropItem.period,
          headers,
          unitsData: [],
          totals: ['', cropItem.hectares, new BigNumber(1), '-', '-'],
          totalHectares: cropItem.hectares,
          arableLandCultures: [],
          arableLandCulturesHectares: new BigNumber(0),
          ...tableHeadings,
        });
      }
    }

    const parcelTableDataIndexEn = parcelTableDataEn.findIndex((crop) => {
      return crop.period === cropItem.period;
    });

    if (parcelTableDataIndexEn !== -1) {
      parcelTableDataEn[parcelTableDataIndexEn].totals[1] = parcelTableDataEn[
        parcelTableDataIndexEn
      ].totals[1].plus(cropItem.hectares);

      parcelTableDataEn[parcelTableDataIndexEn].totals[2] = parcelTableDataEn[
        parcelTableDataIndexEn
      ].totals[2].plus(new BigNumber(1));

      parcelTableDataEn[
        parcelTableDataIndexEn
      ].totalHectares = parcelTableDataEn[
        parcelTableDataIndexEn
      ].totalHectares.plus(cropItem.hectares);

      if (cropItem.isArableLand) {
        parcelTableDataEn[
          parcelTableDataIndexEn
        ].arableLandCulturesHectares = parcelTableDataEn[
          parcelTableDataIndexEn
        ].arableLandCulturesHectares.plus(cropItem.hectares);

        if (
          !parcelTableDataEn[
            parcelTableDataIndexEn
          ].arableLandCultures.includes(cropItem.crpNameEn)
        ) {
          parcelTableDataEn[parcelTableDataIndexEn].arableLandCultures.push(
            cropItem.crpNameEn,
          );
        }
      }
    } else {
      const headers = [
        'Land cover type',
        'Total area (ha)',
        'Parcels total (total no)',
        'Average parcel size (ha)',
        'Average parcel size in my group (ha)',
      ];
      const tableHeadings = {
        clientDataTableHeading: 'Utilized agricultural area (UAA)',
        benchmarkingTableHeading: 'Benchmarking',
      };

      if (cropItem.isArableLand) {
        parcelTableDataEn.push({
          period: cropItem.period,
          headers,
          unitsData: [],
          totals: ['', cropItem.hectares, new BigNumber(1), '-', '-'],
          totalHectares: cropItem.hectares,
          arableLandCultures: [cropItem.crpNameEn],
          arableLandCulturesHectares: cropItem.hectares,
          ...tableHeadings,
        });
      } else {
        parcelTableDataEn.push({
          period: cropItem.period,
          headers,
          unitsData: [],
          totals: ['', cropItem.hectares, new BigNumber(1), '-', '-'],
          totalHectares: cropItem.hectares,
          arableLandCultures: [],
          arableLandCulturesHectares: new BigNumber(0),
          ...tableHeadings,
        });
      }
    }
  });

  return { parcelTableDataEt, parcelTableDataEn };
};

const sortParcelsEconomicInfoByYear = (
  parcelsData: ParcelsDataForEconomicGroupCalculations[],
) => {
  const parcelsEconomicInfoByYear: ParcelsEconomicInfoByYear[] = [];

  parcelsData.forEach((parcel) => {
    const parcelseconomicInfoByYearIndex = parcelsEconomicInfoByYear.findIndex(
      (parcelEconomicInfo) => {
        return parcelEconomicInfo.period === parcel.period;
      },
    );

    if (parcelseconomicInfoByYearIndex !== -1) {
      parcelsEconomicInfoByYear[
        parcelseconomicInfoByYearIndex
      ].economicAmount = parcelsEconomicInfoByYear[
        parcelseconomicInfoByYearIndex
      ].economicAmount.plus(parcel.economicAmount);

      if (
        parcelsEconomicInfoByYear[parcelseconomicInfoByYearIndex][parcel.soc]
      ) {
        parcelsEconomicInfoByYear[parcelseconomicInfoByYearIndex][
          parcel.soc
        ] = parcelsEconomicInfoByYear[parcelseconomicInfoByYearIndex][
          parcel.soc
        ].plus(parcel.economicAmount);
      } else {
        parcelsEconomicInfoByYear[parcelseconomicInfoByYearIndex][parcel.soc] =
          parcel.economicAmount;
      }
    } else {
      parcelsEconomicInfoByYear.push({
        period: parcel.period,
        economicAmount: parcel.economicAmount,
        [parcel.soc]: parcel.economicAmount,
      });
    }
  });

  return parcelsEconomicInfoByYear;
};

export const sortAverageParcelSizesByYearAndGroup = async (
  parcelsByYear,
  economicInfoByYear,
) => {
  await Promise.all(
    parcelsByYear.map(async (parcel) => {
      const averageParcelSize = calculateAverageOfNumber(
        parcel.value,
        parcel.total,
      );
      const economicInfo = economicInfoByYear.find((economicInfo) => {
        return economicInfo.period === parcel.period;
      });
      const { nameEt, nameEn } = parcel;
      const { period, economicClass, productionType } = economicInfo;

      await getConnection()
        .createQueryBuilder()
        .delete()
        .from(AverageParcelSizes)
        .execute();

      await getConnection()
        .createQueryBuilder()
        .insert()
        .into(AverageParcelSizes)
        .values({
          period,
          nameEt,
          nameEn,
          averageParcelSize: averageParcelSize.toNumber(),
          economicClass,
          productionType,
        })
        .execute();
    }),
  );
};

const addParcelsUnitsDataForPieChartTableAndCovertValueToNumber = (
  parcelsPieChartTableData,
  parcelsByYear,
) => {
  const { parcelsByYearEt, parcelsByYearEn } = parcelsByYear;
  const { parcelTableDataEt, parcelTableDataEn } = parcelsPieChartTableData;

  parcelsByYearEt.forEach((parcel: any, parcelIndex: number) => {
    const index = parcelTableDataEt.findIndex(
      (parcelData) => parcelData.period === parcel.period,
    );

    const averageParcelSize = calculateAverageOfNumber(
      parcel.value,
      parcel.total,
    );

    parcelTableDataEt[index].unitsData.push([
      parcel.name,
      parcel.value,
      parcel.total,
      averageParcelSize,
      '-',
    ]);

    parcelsByYearEt[parcelIndex].value = parcelsByYearEt[
      parcelIndex
    ].value.toNumber();
  });

  parcelTableDataEt.map((tableData) => {
    let allParcelsTotalHectares = new BigNumber(0);

    tableData.unitsData.map((unitData) => {
      return (allParcelsTotalHectares = allParcelsTotalHectares.plus(
        unitData[3],
      ));
    });

    tableData.totals[3] = allParcelsTotalHectares
      .dividedBy(new BigNumber(tableData.unitsData.length))
      .decimalPlaces(2);
  });

  parcelsByYearEn.forEach((parcel: any, parcelIndex: number) => {
    const index = parcelTableDataEn.findIndex(
      (parcelData) => parcelData.period === parcel.period,
    );

    const averageParcelSize = calculateAverageOfNumber(
      parcel.value,
      parcel.total,
    );

    parcelTableDataEn[index].unitsData.push([
      parcel.name,
      parcel.value,
      parcel.total,
      averageParcelSize,
      '-',
    ]);

    parcelsByYearEn[parcelIndex].value = parcelsByYearEn[
      parcelIndex
    ].value.toNumber();
  });

  parcelTableDataEn.map((tableData) => {
    let allParcelsTotalHectares = new BigNumber(0);

    tableData.unitsData.map((unitData) => {
      return (allParcelsTotalHectares = allParcelsTotalHectares.plus(
        unitData[3],
      ));
    });

    tableData.totals[3] = allParcelsTotalHectares
      .dividedBy(new BigNumber(tableData.unitsData.length))
      .decimalPlaces(2);
  });

  return {
    parcelsByYearEt,
    parcelsByYearEn,
    parcelTableDataEt,
    parcelTableDataEn,
  };
};

export const addEconomicGroupDataForParcelsPieChartTableData = (
  parcels,
  economicInfo,
) => {
  const { parcelTableDataEt, parcelTableDataEn } = parcels;
  const { economicInfoByYearEn } = economicInfo;

  parcelTableDataEt.forEach((parcelTableData) => {
    const currentYearEconomicInfo = economicInfoByYearEn.find(
      (economicInfo) => {
        return economicInfo.period === parcelTableData.period;
      },
    );
    const { economicClass, productionType, period } = currentYearEconomicInfo;

    let allParcelsTotalHectares = new BigNumber(0);
    parcelTableData.unitsData.map((unitData) => {
      const nameEt = unitData[0];

      const averageParcelSizeData = store.averageParcelSizesByGroup.find(
        (averageParcelSizeData) => {
          return (
            averageParcelSizeData.nameEt === nameEt &&
            averageParcelSizeData.period === period &&
            averageParcelSizeData.economicClass === economicClass &&
            averageParcelSizeData.productionType === productionType
          );
        },
      );

      if (averageParcelSizeData) {
        const { averageParcelSize } = averageParcelSizeData;
        unitData[4] = averageParcelSize.decimalPlaces(2);
        allParcelsTotalHectares = allParcelsTotalHectares.plus(unitData[4]);
      }
    });

    parcelTableData.totals[4] = allParcelsTotalHectares
      .dividedBy(new BigNumber(parcelTableData.unitsData.length))
      .decimalPlaces(2);
  });

  parcelTableDataEn.forEach((parcelTableData) => {
    const currentYearEconomicInfo = economicInfoByYearEn.find(
      (economicInfo) => {
        return economicInfo.period === parcelTableData.period;
      },
    );
    const { economicClass, productionType, period } = currentYearEconomicInfo;

    let allParcelsTotalHectares = new BigNumber(0);
    parcelTableData.unitsData.map((unitData) => {
      const nameEn = unitData[0];

      const averageParcelSizeData = store.averageParcelSizesByGroup.find(
        (averageParcelSizeData) => {
          return (
            averageParcelSizeData.nameEn === nameEn &&
            averageParcelSizeData.period === period &&
            averageParcelSizeData.economicClass === economicClass &&
            averageParcelSizeData.productionType === productionType
          );
        },
      );

      if (averageParcelSizeData) {
        const { averageParcelSize } = averageParcelSizeData;
        unitData[4] = averageParcelSize.decimalPlaces(2);
        allParcelsTotalHectares = allParcelsTotalHectares.plus(unitData[4]);
      }
    });

    parcelTableData.totals[4] = allParcelsTotalHectares
      .dividedBy(new BigNumber(parcelTableData.unitsData.length))
      .decimalPlaces(2);
  });
};

export const sortParcelsByYearForEconomicGroupCalculations = (parcelsData) => {
  const parcelsByYear: ParcelsByYearForEconomicGroupCalculations[] = [];

  parcelsData.forEach((parcel) => {
    const index = parcelsByYear.findIndex((parcelItem) => {
      return (
        parcelItem.period === parcel.period &&
        parcelItem.nameEt === parcel.clanNameEt &&
        parcelItem.nameEn === parcel.clanNameEn
      );
    });
    if (index !== -1) {
      parcelsByYear[index].value = parcelsByYear[index].value.plus(
        parcel.hectares,
      );
      parcelsByYear[index].total = parcelsByYear[index].total.plus(
        new BigNumber(1),
      );
    } else {
      parcelsByYear.push({
        period: parcel.period,
        nameEt: parcel.clanNameEt,
        nameEn: parcel.clanNameEn,
        value: parcel.hectares,
        total: new BigNumber(1),
        unit: 'ha',
      });
    }
  });
  return parcelsByYear;
};

export const sortParcelsDataForEconomicGroupCalculations = async (
  punId: string,
  queryManager: EntityManager,
) => {
  const parcelsData = await queryParcelsDataForEconomicGroupCalculations(
    punId,
    queryManager,
  );
  const parcelsEconomicInfoByYear = sortParcelsEconomicInfoByYear(parcelsData);
  const parcelsByYear = sortParcelsByYearForEconomicGroupCalculations(
    parcelsData,
  );

  return {
    parcelsEconomicInfoByYear,
    parcelsByYear,
    parcelsData,
  };
};

export const setAverageParcelLandTypeDataByGroup = async (
  queryManager: EntityManager,
) => {
  const landTypesData = await queryManager.query(
    `SELECT * FROM sc_niva_extra.land_type_data`,
  );

  landTypesData.map((landTypeData) => {
    const {
      period,
      land_type_en: landTypeEn,
      land_type_et: landTypeEt,
      economic_class: economicClass,
      production_type: productionType,
    } = landTypeData;

    let { hectares } = landTypeData;
    hectares = new BigNumber(hectares);

    const index = store.parcelsLandTypeDataByGroup.findIndex(
      (parcelLandTypeData) => {
        return (
          parcelLandTypeData.period === period &&
          parcelLandTypeData.landTypeEn === landTypeEn &&
          parcelLandTypeData.landTypeEt === landTypeEt &&
          parcelLandTypeData.economicClass === economicClass &&
          parcelLandTypeData.productionType === productionType
        );
      },
    );

    if (index !== -1) {
      store.parcelsLandTypeDataByGroup[
        index
      ].clientCount = store.parcelsLandTypeDataByGroup[index].clientCount.plus(
        new BigNumber(1),
      );

      store.parcelsLandTypeDataByGroup[
        index
      ].hectares = store.parcelsLandTypeDataByGroup[index].hectares.plus(
        hectares,
      );

      const averageLandSize = calculateAverageOfNumber(
        store.parcelsLandTypeDataByGroup[index].hectares,
        store.parcelsLandTypeDataByGroup[index].clientCount,
      );

      store.parcelsLandTypeDataByGroup[index].averageLandSize = averageLandSize;
    } else {
      store.parcelsLandTypeDataByGroup.push({
        period,
        landTypeEn,
        landTypeEt,
        hectares,
        averageLandSize: hectares,
        economicClass,
        productionType,
        clientCount: new BigNumber(1),
      });
    }
  });

  calculateLandTypePercentages();
};

export const calculateLandTypePercentages = () => {
  const totalLandSizesByYear = [];

  store.parcelsLandTypeDataByGroup.forEach((parcelLandTypeData) => {
    const {
      period,
      hectares,
      economicClass,
      productionType,
    } = parcelLandTypeData;

    const index = totalLandSizesByYear.findIndex((totalLandSizeData) => {
      return (
        totalLandSizeData.period === period &&
        totalLandSizeData.economicClass === economicClass &&
        totalLandSizeData.productionType === productionType
      );
    });

    if (index !== -1) {
      totalLandSizesByYear[index].hectares = totalLandSizesByYear[
        index
      ].hectares.plus(hectares);
    } else {
      totalLandSizesByYear.push({
        period,
        economicClass,
        productionType,
        hectares,
      });
    }
  });

  store.parcelsLandTypeDataByGroup.forEach((parcelLandTypeData) => {
    const {
      period,
      hectares,
      economicClass,
      productionType,
    } = parcelLandTypeData;

    const index = totalLandSizesByYear.findIndex((totalLandSizeData) => {
      return (
        totalLandSizeData.period === period &&
        totalLandSizeData.economicClass === economicClass &&
        totalLandSizeData.productionType === productionType
      );
    });

    const landTypePercentage = hectares
      .dividedBy(totalLandSizesByYear[index].hectares)
      .times(new BigNumber(100));

    parcelLandTypeData.landTypePercentage = landTypePercentage;
    parcelLandTypeData.allLandTypesTotalSize =
      totalLandSizesByYear[index].hectares;
  });
};

export const setAverageParcelSizesByGroup = async (queryManager) => {
  const averageParcelSizes = await queryManager.query(
    `SELECT * FROM sc_niva_extra.avg_parcel_sizes`,
  );

  averageParcelSizes.map((parcelData) => {
    const {
      period,
      name_et: nameEt,
      name_en: nameEn,
      economic_class: economicClass,
      production_type: productionType,
    } = parcelData;

    let { avg_parcel_size: averageParcelSize } = parcelData;
    averageParcelSize = new BigNumber(averageParcelSize);

    const index = store.averageParcelSizesByGroup.findIndex(
      (averageParcelSize) => {
        return (
          averageParcelSize.period === period &&
          averageParcelSize.nameEt === nameEt &&
          averageParcelSize.nameEn === nameEn &&
          averageParcelSize.economicClass === economicClass &&
          averageParcelSize.productionType === productionType
        );
      },
    );

    if (index !== -1) {
      store.averageParcelSizesByGroup[
        index
      ].clientCount = store.averageParcelSizesByGroup[index].clientCount.plus(
        new BigNumber(1),
      );

      store.averageParcelSizesByGroup[
        index
      ].averageParcelSize = store.averageParcelSizesByGroup[
        index
      ].averageParcelSize
        .plus(averageParcelSize)
        .dividedBy(store.averageParcelSizesByGroup[index].clientCount);
    } else {
      store.averageParcelSizesByGroup.push({
        period,
        nameEt,
        nameEn,
        averageParcelSize,
        economicClass,
        productionType,
        clientCount: new BigNumber(1),
      });
    }
  });
};

export const sortParcelsLandTypeDataByYearAndGroup = async (
  parcelsData,
  economicInfoByYear,
) => {
  const landTypesData = [];

  parcelsData.forEach((parcel) => {
    const { landTypeEn, landTypeEt } = parcel.landType;
    const economicInfo = economicInfoByYear.find((economicInfo) => {
      return economicInfo.period === parcel.period;
    });
    const { economicClass, productionType } = economicInfo;

    const index = landTypesData.findIndex((landTypeData) => {
      return (
        landTypeData.landTypeEn === landTypeEn &&
        landTypeData.landTypeEt === landTypeEt &&
        landTypeData.period === parcel.period
      );
    });

    if (index !== -1) {
      landTypesData[index].hectares = landTypesData[index].hectares.plus(
        parcel.hectares,
      );
    } else {
      landTypesData.push({
        period: parcel.period,
        landTypeEn,
        landTypeEt,
        hectares: parcel.hectares,
        economicClass,
        productionType,
      });
    }
  });

  await Promise.all(
    landTypesData.map(async (landTypeData) => {
      const {
        period,
        landTypeEn,
        landTypeEt,
        hectares,
        economicClass,
        productionType,
      } = landTypeData;

      await getConnection()
        .createQueryBuilder()
        .delete()
        .from(LandTypeData)
        .execute();

      await getConnection()
        .createQueryBuilder()
        .insert()
        .into(LandTypeData)
        .values({
          period,
          landTypeEn,
          landTypeEt,
          hectares: hectares.toNumber(),
          economicClass,
          productionType,
        })
        .execute();
    }),
  );

  return landTypesData;
};

const addtotalLandSizesToLandTypeData = (landTypesData) => {
  const totalLandSizesByYear = [];

  landTypesData.forEach((landTypeData) => {
    const index = totalLandSizesByYear.findIndex((totalLandSizeData) => {
      return totalLandSizeData.period === landTypeData.period;
    });

    if (index !== -1) {
      totalLandSizesByYear[index].totalLandSize = totalLandSizesByYear[
        index
      ].totalLandSize.plus(landTypeData.hectares);
    } else {
      totalLandSizesByYear.push({
        period: landTypeData.period,
        totalLandSize: landTypeData.hectares,
      });
    }
  });

  landTypesData.forEach((landTypeData) => {
    const totalLandSizeData = totalLandSizesByYear.find((totalLandSizeData) => {
      return totalLandSizeData.period === landTypeData.period;
    });

    const { totalLandSize } = totalLandSizeData;
    landTypeData.totalLandSize = totalLandSize;
  });

  return landTypesData;
};

const sortParcelsLandTypeData = (parcelsData) => {
  const landTypesData = [];

  parcelsData.forEach((parcel) => {
    const { landTypeEn, landTypeEt } = parcel.landType;

    const index = landTypesData.findIndex((landTypeData) => {
      return (
        landTypeData.landTypeEn === landTypeEn &&
        landTypeData.landTypeEt === landTypeEt &&
        landTypeData.period === parcel.period
      );
    });

    if (index !== -1) {
      landTypesData[index].hectares = landTypesData[index].hectares.plus(
        parcel.hectares,
      );
    } else {
      landTypesData.push({
        period: parcel.period,
        landTypeEn,
        landTypeEt,
        hectares: parcel.hectares,
      });
    }
  });

  return addtotalLandSizesToLandTypeData(landTypesData);
};

export const setParcelsLandTypeTableData = (parcels, economicInfo) => {
  const { parcelsLandTypeData } = parcels;
  const { economicInfoByYearEn } = economicInfo;
  const parcelsLandTypeTableDataEn = [];
  const parcelsLandTypeTableDataEt = [];

  parcelsLandTypeData.forEach((landTypeData) => {
    const currentYearEconomicInfo = economicInfoByYearEn.find(
      (economicInfo) => {
        return economicInfo.period === landTypeData.period;
      },
    );
    const { economicClass, productionType, period } = currentYearEconomicInfo;
    const { landTypeEn, landTypeEt, hectares } = landTypeData;

    const economicGroupInfo = store.parcelsLandTypeDataByGroup.find(
      (parcelLandTypeData) => {
        return (
          parcelLandTypeData.period === period &&
          parcelLandTypeData.economicClass === economicClass &&
          parcelLandTypeData.productionType === productionType &&
          parcelLandTypeData.landTypeEn === landTypeEn &&
          parcelLandTypeData.landTypeEt === landTypeEt
        );
      },
    );

    if (economicGroupInfo) {
      const landTypePercentage = landTypeData.hectares
        .dividedBy(landTypeData.totalLandSize)
        .times(new BigNumber(100))
        .decimalPlaces(2);

      const indexEt = parcelsLandTypeTableDataEt.findIndex(
        (parcelLandTypeTableDataEt) => {
          return parcelLandTypeTableDataEt.period === period;
        },
      );

      if (indexEt !== -1) {
        parcelsLandTypeTableDataEt[indexEt].unitsData.push([
          landTypeEt,
          hectares,
          landTypePercentage,
          economicGroupInfo.averageLandSize,
          economicGroupInfo.landTypePercentage.decimalPlaces(2),
        ]);
        parcelsLandTypeTableDataEt[
          indexEt
        ].landCount = parcelsLandTypeTableDataEt[indexEt].landCount.plus(
          new BigNumber(1),
        );
        parcelsLandTypeTableDataEt[
          indexEt
        ].totals[3] = parcelsLandTypeTableDataEt[indexEt].totals[3]
          .plus(economicGroupInfo.averageLandSize)
          .dividedBy(parcelsLandTypeTableDataEt[indexEt].landCount);

        parcelsLandTypeTableDataEt[
          indexEt
        ].totals[1] = parcelsLandTypeTableDataEt[indexEt].totals[1]
          .plus(hectares)
          .dividedBy(parcelsLandTypeTableDataEt[indexEt].landCount);
      } else {
        const headers = [
          'Maakasutustüüp',
          'Pindala keskmine (ha)',
          'Keskmine osakaal (%)',
          'Keskmine hektarite arv minu grupis',
          'Keskmine osakaal minu grupis (%)',
        ];

        parcelsLandTypeTableDataEt.push({
          period,
          headers,
          unitsData: [
            [
              landTypeEt,
              hectares,
              landTypePercentage,
              economicGroupInfo.averageLandSize,
              economicGroupInfo.landTypePercentage.decimalPlaces(2),
            ],
          ],
          totals: [
            '',
            hectares,
            '100',
            economicGroupInfo.averageLandSize,
            '100',
          ],
          landCount: new BigNumber(1),
        });
      }

      const indexEn = parcelsLandTypeTableDataEn.findIndex(
        (parcelLandTypeTableDataEn) => {
          return parcelLandTypeTableDataEn.period === period;
        },
      );

      if (indexEn !== -1) {
        parcelsLandTypeTableDataEn[indexEn].unitsData.push([
          landTypeEn,
          hectares,
          landTypePercentage,
          economicGroupInfo.averageLandSize,
          economicGroupInfo.landTypePercentage.decimalPlaces(2),
        ]);

        parcelsLandTypeTableDataEn[
          indexEn
        ].landCount = parcelsLandTypeTableDataEn[indexEn].landCount.plus(
          new BigNumber(1),
        );
        parcelsLandTypeTableDataEn[
          indexEn
        ].totals[3] = parcelsLandTypeTableDataEn[indexEn].totals[3]
          .plus(economicGroupInfo.averageLandSize)
          .dividedBy(parcelsLandTypeTableDataEn[indexEn].landCount);

        parcelsLandTypeTableDataEn[
          indexEn
        ].totals[1] = parcelsLandTypeTableDataEn[indexEn].totals[1]
          .plus(hectares)
          .dividedBy(parcelsLandTypeTableDataEn[indexEn].landCount);
      } else {
        const headers = [
          'Land cover type',
          'Average area (ha)',
          'Average proportion (%)',
          'Average hectares in my group',
          'Average proportion in my group (%)',
        ];

        parcelsLandTypeTableDataEn.push({
          period,
          headers,
          unitsData: [
            [
              landTypeEn,
              hectares,
              landTypePercentage,
              economicGroupInfo.averageLandSize,
              economicGroupInfo.landTypePercentage.decimalPlaces(2),
            ],
          ],
          totals: [
            '',
            hectares,
            '100',
            economicGroupInfo.averageLandSize,
            '100',
          ],
          landCount: new BigNumber(1),
        });
      }
    }
  });

  return { parcelsLandTypeTableDataEn, parcelsLandTypeTableDataEt };
};

export const sortParcelsData = async (
  punId: string,
  queryManager: EntityManager,
) => {
  const parcelsData = await queryParcelsData(punId, queryManager);
  const parcelsByYear = sortParcelsByYear(parcelsData);
  const parcelsPieChartTableData = sortParcelsPieChartTableData(parcelsData);
  const parcelsLandTypeData = sortParcelsLandTypeData(parcelsData);
  const parcelsEconomicInfoByYear = sortParcelsEconomicInfoByYear(parcelsData);

  const parcelsByYearAndPieChartTableData = addParcelsUnitsDataForPieChartTableAndCovertValueToNumber(
    parcelsPieChartTableData,
    parcelsByYear,
  );

  return {
    parcelsData,
    parcelsLandTypeData,
    unit: 'ha',
    parcelsEconomicInfoByYear,
    ...parcelsByYearAndPieChartTableData,
  };
};
