/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import BigNumber from 'bignumber.js';
import { LocCoPunittype as loc_co_punittype } from '../../../../entities/schemas/sc_niva_extra/loc/loc_co_punittype.entity';
import { LocCoPphytosanitary as loc_co_pphytosanitary } from '../../../../entities/schemas/sc_niva_extra/loc/loc_co_pphytosanitary.entity';
import { EntityManager } from 'typeorm';
import { Parcels, Parcel } from './parcels';

interface PlantProtectionChemicalQueryData {
  phy_id: number;
  cph_startdate: Date;
  cph_dose: string;
  uni_id: number;
}

interface PlantProtectionChemical {
  phyId: number;
  period: string;
  cphDose: BigNumber;
  phyNameEt: string;
  phyNameEn: string;
  phyFormulationEt: string | null;
  phyFormulationEn: string | null;
  uniNameEt: string;
  uniNameEn: string;
  unitNameEt: string;
  unitNameEn: string;
  cphStartDate: Date;
}

interface PlantProtectionChemicalByYear {
  period: string;
  plantProtectionChemicals: any[];
}

interface PlantProtectionChemialInChartFormat {
  period: string;
  value: number;
  name: string;
}

interface PlantProtectionChemicalsTableData {
  period: string;
  headers: string[];
  unitsData: any[];
  totals: any[];
}

export const queryPlantProtectionChemicals = async (
  rcpId: string,
  queryManager: EntityManager,
) => {
  const plantProtectionChemicalsQuery: PlantProtectionChemicalQueryData[] = await queryManager.query(
    `SELECT phy_id, cph_startdate, cph_dose, uni_id FROM sc_niva.rp_dcropphytosanit WHERE rcp_id = '${rcpId}'`,
  );
  const plantProtectionChemicals: PlantProtectionChemical[] = [];

  if (plantProtectionChemicalsQuery.length !== 0) {
    await Promise.all(
      plantProtectionChemicalsQuery.map(async (plantChemical) => {
        const {
          phy_id: phyId,
          cph_dose: cphDose,
          uni_id: uniId,
          cph_startdate: cphStartDate,
        } = plantChemical;

        const chemicalUnitTypeQuery = await loc_co_punittype.find({
          select: ['uniNameEt', 'uniNameEn', 'unitNameEt', 'unitNameEn'],
          where: { uniId },
        });

        const {
          uniNameEt,
          uniNameEn,
          unitNameEt,
          unitNameEn,
        } = chemicalUnitTypeQuery[0];

        const chemicalNamesQuery = await loc_co_pphytosanitary.find({
          select: [
            'phyNameEt',
            'phyNameEn',
            'phyFormulationEt',
            'phyFormulationEn',
          ],
          where: { phyId },
        });

        const {
          phyNameEt,
          phyNameEn,
          phyFormulationEt,
          phyFormulationEn,
        } = chemicalNamesQuery[0];

        const chemical = {
          phyId,
          period: cphStartDate.getFullYear().toString(),
          cphDose: new BigNumber(cphDose),
          phyNameEt,
          phyNameEn,
          phyFormulationEt,
          phyFormulationEn,
          uniNameEt,
          uniNameEn,
          unitNameEt,
          unitNameEn,
          cphStartDate,
        };

        plantProtectionChemicals.push(chemical);
      }),
    );
  }
  return plantProtectionChemicals;
};

const sortPlantChemicalsByYear = (parcelsData: Parcel[]) => {
  const plantProtectionChemicalsByYear: PlantProtectionChemicalByYear[] = [];

  parcelsData.forEach((cropItem) => {
    const plantProtectionChemicalsByYearIndex = plantProtectionChemicalsByYear.findIndex(
      (plantChemical) => {
        return plantChemical.period === cropItem.period;
      },
    );

    if (plantProtectionChemicalsByYearIndex !== -1) {
      if (cropItem.plantProtectionChemicals.length !== 0) {
        const plantProtectionChemicalsExistingIds: number[] = [];

        plantProtectionChemicalsByYear[
          plantProtectionChemicalsByYearIndex
        ].plantProtectionChemicals.map((chemical) => {
          return plantProtectionChemicalsExistingIds.push(
            chemical.plantChemical.phyId,
          );
        });

        cropItem.plantProtectionChemicals.map(
          (plantChemical: PlantProtectionChemical) => {
            if (
              plantProtectionChemicalsExistingIds.includes(plantChemical.phyId)
            ) {
              const chemicalIndex = plantProtectionChemicalsByYear[
                plantProtectionChemicalsByYearIndex
              ].plantProtectionChemicals.findIndex((chemical) => {
                return chemical.plantChemical.phyId === plantChemical.phyId;
              });

              plantProtectionChemicalsByYear[
                plantProtectionChemicalsByYearIndex
              ].plantProtectionChemicals[
                chemicalIndex
              ].plantChemical.cphDose = plantProtectionChemicalsByYear[
                plantProtectionChemicalsByYearIndex
              ].plantProtectionChemicals[
                chemicalIndex
              ].plantChemical.cphDose.plus(plantChemical.cphDose);

              plantProtectionChemicalsByYear[
                plantProtectionChemicalsByYearIndex
              ].plantProtectionChemicals[
                chemicalIndex
              ].plantChemical.parcelCount = plantProtectionChemicalsByYear[
                plantProtectionChemicalsByYearIndex
              ].plantProtectionChemicals[
                chemicalIndex
              ].plantChemical.parcelCount.plus(new BigNumber(1));

              plantProtectionChemicalsByYear[
                plantProtectionChemicalsByYearIndex
              ].plantProtectionChemicals[
                chemicalIndex
              ].plantChemical.hectares = plantProtectionChemicalsByYear[
                plantProtectionChemicalsByYearIndex
              ].plantProtectionChemicals[
                chemicalIndex
              ].plantChemical.hectares.plus(cropItem.hectares);

              if (
                !plantProtectionChemicalsByYear[
                  plantProtectionChemicalsByYearIndex
                ].plantProtectionChemicals[
                  chemicalIndex
                ].plantChemical.clanNameEn.includes(cropItem.clanNameEn) ||
                !plantProtectionChemicalsByYear[
                  plantProtectionChemicalsByYearIndex
                ].plantProtectionChemicals[
                  chemicalIndex
                ].plantChemical.clanNameEt.includes(cropItem.clanNameEt)
              ) {
                plantProtectionChemicalsByYear[
                  plantProtectionChemicalsByYearIndex
                ].plantProtectionChemicals[
                  chemicalIndex
                ].plantChemical.clanNameEt.push(cropItem.clanNameEt);

                plantProtectionChemicalsByYear[
                  plantProtectionChemicalsByYearIndex
                ].plantProtectionChemicals[
                  chemicalIndex
                ].plantChemical.clanNameEn.push(cropItem.clanNameEn);
              }
            } else {
              const plantChemicalToAdd = {
                ...plantChemical,
                parcelCount: new BigNumber(1),
                hectares: cropItem.hectares,
                clanNameEn: [cropItem.clanNameEn],
                clanNameEt: [cropItem.clanNameEt],
              };

              plantProtectionChemicalsByYear[
                plantProtectionChemicalsByYearIndex
              ].plantProtectionChemicals.push({
                plantChemical: {
                  ...plantChemicalToAdd,
                },
              });
            }
          },
        );
      }
    } else {
      const plantProtectionChemicals: PlantProtectionChemical[] = [];
      if (cropItem.plantProtectionChemicals.length !== 0) {
        cropItem.plantProtectionChemicals.map(
          (chemical: PlantProtectionChemical) => {
            const plantChemicalToAdd = {
              ...chemical,
              parcelCount: new BigNumber(1),
              hectares: cropItem.hectares,
              clanNameEn: [cropItem.clanNameEn],
              clanNameEt: [cropItem.clanNameEt],
            };

            plantProtectionChemicals.push(plantChemicalToAdd);
          },
        );
      }

      plantProtectionChemicalsByYear.push({
        period: cropItem.period,
        plantProtectionChemicals,
      });
    }
  });
  return plantProtectionChemicalsByYear;
};

const sortPlantChemicalsToChartFormat = (
  plantChemicalsByYear: PlantProtectionChemicalByYear[],
) => {
  const plantProtectionChemicalsEt: PlantProtectionChemialInChartFormat[] = [];
  const plantProtectionChemicalsEn: PlantProtectionChemialInChartFormat[] = [];

  plantChemicalsByYear.forEach((chemical) => {
    if (chemical.plantProtectionChemicals.length !== 0) {
      chemical.plantProtectionChemicals.map((chem) => {
        const {
          phyFormulationEt,
          phyFormulationEn,
          cphDose,
          hectares,
        } = chem.plantChemical;
        let { clanNameEn, clanNameEt } = chem.plantChemical;

        const cphDoseUsagePerHectarValue = cphDose
          .dividedBy(hectares)
          .decimalPlaces(2)
          .toNumber();

        if (clanNameEn.length > 1 || clanNameEt.length > 1) {
          clanNameEn = clanNameEn.join(', ');
          clanNameEt = clanNameEt.join(', ');
        }

        plantProtectionChemicalsEt.push({
          period: chemical.period,
          value: cphDoseUsagePerHectarValue,
          name: phyFormulationEt,
        });

        plantProtectionChemicalsEn.push({
          period: chemical.period,
          value: cphDoseUsagePerHectarValue,
          name: phyFormulationEn,
        });
      });
    }
  });

  return {
    plantProtectionChemicalsEt,
    plantProtectionChemicalsEn,
  };
};

const setChemicalsTableData = (
  plantChemicalsByYear: PlantProtectionChemicalByYear[],
) => {
  const plantProtectionChemicalsTableDataEt: PlantProtectionChemicalsTableData[] = [];
  const plantProtectionChemicalsTableDataEn: PlantProtectionChemicalsTableData[] = [];

  plantChemicalsByYear.forEach((chemical) => {
    if (chemical.plantProtectionChemicals.length !== 0) {
      chemical.plantProtectionChemicals.map((chem) => {
        const {
          phyNameEt,
          phyNameEn,
          phyFormulationEt,
          phyFormulationEn,
          cphDose,
          hectares,
          uniNameEt,
          uniNameEn,
          unitNameEt,
          unitNameEn,
        } = chem.plantChemical;
        let { clanNameEn, clanNameEt } = chem.plantChemical;

        const cphDoseUsagePerHectarValue = cphDose
          .dividedBy(hectares)
          .decimalPlaces(2)
          .toNumber();

        if (clanNameEn.length > 1 || clanNameEt.length > 1) {
          clanNameEn = clanNameEn.join(', ');
          clanNameEt = clanNameEt.join(', ');
        }

        const cphDoseUsagePerHectarEt = `${cphDoseUsagePerHectarValue} ${uniNameEt}`;
        const cphDoseUsagePerHectarEn = `${cphDoseUsagePerHectarValue} ${uniNameEn}`;
        const cphDoseUsageDuringYearEt = `${cphDose} ${unitNameEt}`;
        const cphDoseUsageDuringYearEn = `${cphDose} ${unitNameEn}`;

        const tableDataIndexEt = plantProtectionChemicalsTableDataEt.findIndex(
          (protectionChemical) => protectionChemical.period === chemical.period,
        );

        if (tableDataIndexEt !== -1) {
          plantProtectionChemicalsTableDataEt[tableDataIndexEt].unitsData.push([
            phyNameEt,
            phyFormulationEt,
            cphDoseUsageDuringYearEt,
            cphDoseUsagePerHectarEt,
            clanNameEt,
          ]);
        } else {
          plantProtectionChemicalsTableDataEt.push({
            period: chemical.period,
            headers: [
              'Taimekaitsevahendi nimetus',
              'Toimeaine',
              'Aasta jooksul kokku kasutatud vahendi kogus',
              'Keskmine vahendi kogus hektari kohta',
              'Seotud kultuurid',
            ],
            unitsData: [
              [
                phyNameEt,
                phyFormulationEt,
                cphDoseUsageDuringYearEt,
                cphDoseUsagePerHectarEt,
                clanNameEt,
              ],
            ],
            totals: [],
          });
        }

        const tableDataIndexEn = plantProtectionChemicalsTableDataEn.findIndex(
          (protectionChemical) => protectionChemical.period === chemical.period,
        );

        if (tableDataIndexEn !== -1) {
          plantProtectionChemicalsTableDataEn[tableDataIndexEn].unitsData.push([
            phyNameEn,
            phyFormulationEn,
            cphDoseUsageDuringYearEn,
            cphDoseUsagePerHectarEn,
            clanNameEn,
          ]);
        } else {
          plantProtectionChemicalsTableDataEn.push({
            period: chemical.period,
            headers: [
              'Plant protection name',
              'Active substance',
              'Total amount used during the year',
              'Average amount per hectare',
              'Related cultures',
            ],
            unitsData: [
              [
                phyNameEn,
                phyFormulationEn,
                cphDoseUsageDuringYearEn,
                cphDoseUsagePerHectarEn,
                clanNameEn,
              ],
            ],
            totals: [],
          });
        }
      });
    }
  });

  return {
    plantProtectionChemicalsTableDataEt,
    plantProtectionChemicalsTableDataEn,
  };
};

export const sortPlantChemicalsData = (parcels: Parcels) => {
  const { parcelsData } = parcels;
  const plantChemicalsByYear = sortPlantChemicalsByYear(parcelsData);
  const plantChemicalsInChartFormat = sortPlantChemicalsToChartFormat(
    plantChemicalsByYear,
  );
  const plantChemicalsTableData = setChemicalsTableData(plantChemicalsByYear);

  return {
    ...plantChemicalsInChartFormat,
    ...plantChemicalsTableData,
  };
};
