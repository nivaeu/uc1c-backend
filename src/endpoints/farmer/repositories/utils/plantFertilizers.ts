/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import BigNumber from 'bignumber.js';
import { EntityManager } from 'typeorm';
import { Parcel, Parcels } from './parcels';

interface PlantFertilizersQueryData {
  fet_id: number;
  cfe_dose: string;
  cfe_startdate: Date;
  fer_id: number;
  uni_id: number;
}

interface PlantFertilizer {
  ferId: number;
  cfeDose: BigNumber;
  period: string;
  fetNameEt: string;
  fetNameEn: string;
  ferNameEt: string;
  ferNameEn: string;
  ferActiveSubstanceEt: string;
  ferActiveSubstanceEn: string;
  uniNameEt: string;
  uniNameEn: string;
  unitNameEt: string;
  unitNameEn: string;
}

interface PlantFertilizerByYear {
  period: string;
  plantFertilizers: any[];
}

interface PlantFertilizerChartFormat {
  period: string;
  value: number;
  name: string;
}

interface PlantFertilizerTableData {
  period: string;
  headers: string[];
  unitsData: any[];
  totals: any[];
}

export const queryPlantFertilizers = async (
  rcpId: string,
  queryManager: EntityManager,
) => {
  const plantFertilizersQuery: PlantFertilizersQueryData[] = await queryManager.query(
    `SELECT fet_id, cfe_dose, cfe_startdate, fer_id, uni_id FROM sc_niva.RP_DCROPFERTILIZER WHERE rcp_id = '${rcpId}'`,
  );
  const plantFertilizers: PlantFertilizer[] = [];

  if (plantFertilizersQuery.length !== 0) {
    await Promise.all(
      plantFertilizersQuery.map(async (plantFertilizer) => {
        const {
          fet_id: fetId,
          cfe_dose: cfeDose,
          cfe_startdate: cfeStartDate,
          fer_id: ferId,
          uni_id: uniId,
        } = plantFertilizer;

        const fertilizerUnitTypeQuery = await queryManager.query(
          `SELECT uni_name_et, uni_name_en, unit_name_et, unit_name_en FROM sc_niva_extra.loc_co_punittype WHERE uni_id = '${uniId}'`,
        );

        const {
          uni_name_et: uniNameEt,
          uni_name_en: uniNameEn,
          unit_name_et: unitNameEt,
          unit_name_en: unitNameEn,
        } = fertilizerUnitTypeQuery[0];

        const fertilizerTypeQuery = await queryManager.query(
          `SELECT fet_name_et, fet_name_en FROM sc_niva_extra.loc_co_pfertilizertype WHERE fet_id = '${fetId}'`,
        );

        const {
          fet_name_et: fetNameEt,
          fet_name_en: fetNameEn,
        } = fertilizerTypeQuery[0];

        const fertilizerNameQuery = await queryManager.query(
          `SELECT fer_name_et, fer_name_en, fer_active_substance_et, fer_active_substance_en FROM sc_niva_extra.LOC_CO_PFERTILIZER where fer_id = '${ferId}'`,
        );

        const {
          fer_name_et: ferNameEt,
          fer_name_en: ferNameEn,
          fer_active_substance_et: ferActiveSubstanceEt,
          fer_active_substance_en: ferActiveSubstanceEn,
        } = fertilizerNameQuery[0];

        const fertilizer = {
          ferId,
          cfeDose: new BigNumber(cfeDose),
          period: cfeStartDate.getFullYear().toString(),
          fetNameEt,
          fetNameEn,
          ferNameEt,
          ferNameEn,
          ferActiveSubstanceEt,
          ferActiveSubstanceEn,
          uniNameEt,
          uniNameEn,
          unitNameEt,
          unitNameEn,
        };

        plantFertilizers.push(fertilizer);
      }),
    );
  }
  return plantFertilizers;
};

const sortPlantFertilizersByYear = (parcelsData: Parcel[]) => {
  const plantFertilizersByYear: PlantFertilizerByYear[] = [];

  parcelsData.forEach((cropItem) => {
    const plantFertilizersByYearIndex = plantFertilizersByYear.findIndex(
      (plantFertilizer) => {
        return plantFertilizer.period === cropItem.period;
      },
    );

    if (plantFertilizersByYearIndex !== -1) {
      if (cropItem.plantFertilizers.length !== 0) {
        const plantFertilizerExistingIds: number[] = [];

        plantFertilizersByYear[
          plantFertilizersByYearIndex
        ].plantFertilizers.forEach((fertilizer) => {
          if (
            !plantFertilizerExistingIds.includes(
              fertilizer.plantFertilizer.ferId,
            )
          ) {
            return plantFertilizerExistingIds.push(
              fertilizer.plantFertilizer.ferId,
            );
          } else {
            return plantFertilizerExistingIds;
          }
        });

        cropItem.plantFertilizers.forEach(
          (plantFertilizer: PlantFertilizer) => {
            if (plantFertilizerExistingIds.includes(plantFertilizer.ferId)) {
              const plantFertilizerIndex = plantFertilizersByYear[
                plantFertilizersByYearIndex
              ].plantFertilizers.findIndex((fertilizer) => {
                return (
                  fertilizer.plantFertilizer.ferId === plantFertilizer.ferId
                );
              });

              plantFertilizersByYear[
                plantFertilizersByYearIndex
              ].plantFertilizers[
                plantFertilizerIndex
              ].plantFertilizer.cfeDose = plantFertilizersByYear[
                plantFertilizersByYearIndex
              ].plantFertilizers[
                plantFertilizerIndex
              ].plantFertilizer.cfeDose.plus(plantFertilizer.cfeDose);

              plantFertilizersByYear[
                plantFertilizersByYearIndex
              ].plantFertilizers[
                plantFertilizerIndex
              ].plantFertilizer.parcelCount = plantFertilizersByYear[
                plantFertilizersByYearIndex
              ].plantFertilizers[
                plantFertilizerIndex
              ].plantFertilizer.parcelCount.plus(new BigNumber(1));

              plantFertilizersByYear[
                plantFertilizersByYearIndex
              ].plantFertilizers[
                plantFertilizerIndex
              ].plantFertilizer.hectares = plantFertilizersByYear[
                plantFertilizersByYearIndex
              ].plantFertilizers[
                plantFertilizerIndex
              ].plantFertilizer.hectares.plus(cropItem.hectares);

              if (
                !plantFertilizersByYear[
                  plantFertilizersByYearIndex
                ].plantFertilizers[
                  plantFertilizerIndex
                ].plantFertilizer.clanNameEn.includes(cropItem.clanNameEn) ||
                !plantFertilizersByYear[
                  plantFertilizersByYearIndex
                ].plantFertilizers[
                  plantFertilizerIndex
                ].plantFertilizer.clanNameEt.includes(cropItem.clanNameEt)
              ) {
                plantFertilizersByYear[
                  plantFertilizersByYearIndex
                ].plantFertilizers[
                  plantFertilizerIndex
                ].plantFertilizer.clanNameEt.push(cropItem.clanNameEt);

                plantFertilizersByYear[
                  plantFertilizersByYearIndex
                ].plantFertilizers[
                  plantFertilizerIndex
                ].plantFertilizer.clanNameEn.push(cropItem.clanNameEn);
              }
            } else {
              const plantFertilizerToAdd = {
                ...plantFertilizer,
                parcelCount: new BigNumber(1),
                hectares: cropItem.hectares,
                clanNameEn: [cropItem.clanNameEn],
                clanNameEt: [cropItem.clanNameEt],
              };

              plantFertilizersByYear[
                plantFertilizersByYearIndex
              ].plantFertilizers.push({
                plantFertilizer: {
                  ...plantFertilizerToAdd,
                },
              });
            }
          },
        );
      }
    } else {
      const plantFertilizers: PlantFertilizer[] = [];

      if (cropItem.plantFertilizers.length !== 0) {
        cropItem.plantFertilizers.forEach((fertilizer: PlantFertilizer) => {
          const plantFertilizerToAdd = {
            ...fertilizer,
            parcelCount: new BigNumber(1),
            hectares: cropItem.hectares,
            clanNameEn: [cropItem.clanNameEn],
            clanNameEt: [cropItem.clanNameEt],
          };

          plantFertilizers.push(plantFertilizerToAdd);
        });
      }

      plantFertilizersByYear.push({
        period: cropItem.period,
        plantFertilizers,
      });
    }
  });

  return plantFertilizersByYear;
};

const sortPlantFertilizersToChartFormat = (
  plantFertilizersByYear: PlantFertilizerByYear[],
) => {
  const plantFertilizersEt: PlantFertilizerChartFormat[] = [];
  const plantFertilizersEn: PlantFertilizerChartFormat[] = [];

  plantFertilizersByYear.forEach((fertilizer) => {
    if (fertilizer.plantFertilizers.length !== 0) {
      fertilizer.plantFertilizers.forEach((fert) => {
        if (fert.plantFertilizer) {
          const { cfeDose, ferNameEt, ferNameEn } = fert.plantFertilizer;
          plantFertilizersEt.push({
            period: fertilizer.period,
            value: cfeDose.toNumber(),
            name: ferNameEt,
          });
          plantFertilizersEn.push({
            period: fertilizer.period,
            value: cfeDose.toNumber(),
            name: ferNameEn,
          });
        }
      });
    }
  });

  return { plantFertilizersEt, plantFertilizersEn };
};

const setPlantFertilizersTableData = (
  plantFertilizersByYear: PlantFertilizerByYear[],
) => {
  const plantFertilizersTableDataEt: PlantFertilizerTableData[] = [];
  const plantFertilizersTableDataEn: PlantFertilizerTableData[] = [];

  plantFertilizersByYear.forEach((fertilizer) => {
    if (fertilizer.plantFertilizers.length !== 0) {
      fertilizer.plantFertilizers.forEach((fert) => {
        if (fert.plantFertilizer) {
          const {
            cfeDose,
            fetNameEt,
            fetNameEn,
            ferNameEt,
            ferNameEn,
            ferActiveSubstanceEt,
            ferActiveSubstanceEn,
            uniNameEt,
            uniNameEn,
            unitNameEt,
            unitNameEn,
            hectares,
          } = fert.plantFertilizer;
          let { clanNameEn, clanNameEt } = fert.plantFertilizer;
          const cfeDoseUsagePerHectarValue = cfeDose
            .dividedBy(hectares)
            .decimalPlaces(2)
            .toNumber();
          const cfeDoseUsagePerHectarEt = `${cfeDoseUsagePerHectarValue} ${uniNameEt}`;
          const cfeDoseUsagePerHectarEn = `${cfeDoseUsagePerHectarValue} ${uniNameEn}`;
          const cfeDoseUsageDuringYearEt = `${cfeDose} ${unitNameEt}`;
          const cfeDoseUsageDuringYearEn = `${cfeDose} ${unitNameEn}`;
          if (clanNameEn.length > 1 || clanNameEt.length > 1) {
            clanNameEn = clanNameEn.join(', ');
            clanNameEt = clanNameEt.join(', ');
          }
          const tableDataIndexEt = plantFertilizersTableDataEt.findIndex(
            (plantFertilizer) => plantFertilizer.period === fertilizer.period,
          );
          if (tableDataIndexEt !== -1) {
            plantFertilizersTableDataEt[tableDataIndexEt].unitsData.push([
              fetNameEt,
              ferNameEt,
              ferActiveSubstanceEt,
              cfeDoseUsageDuringYearEt,
              cfeDoseUsagePerHectarEt,
              clanNameEt,
            ]);
          } else {
            plantFertilizersTableDataEt.push({
              period: fertilizer.period,
              headers: [
                'Väetise tüüp',
                'Nimetus',
                'Toimeaine',
                'Aasta jooksul kokku kasutatud vahendi kogus',
                'Keskmine vahendi kogus hektari kohta',
                'Seotud kultuurid',
              ],
              unitsData: [
                [
                  fetNameEt,
                  ferNameEt,
                  ferActiveSubstanceEt,
                  cfeDoseUsageDuringYearEt,
                  cfeDoseUsagePerHectarEt,
                  clanNameEt,
                ],
              ],
              totals: [],
            });
          }
          const tableDataIndexEn = plantFertilizersTableDataEn.findIndex(
            (plantFertilizer) => plantFertilizer.period === fertilizer.period,
          );
          if (tableDataIndexEn !== -1) {
            plantFertilizersTableDataEn[tableDataIndexEn].unitsData.push([
              fetNameEn,
              ferNameEn,
              ferActiveSubstanceEn,
              cfeDoseUsageDuringYearEn,
              cfeDoseUsagePerHectarEn,
              clanNameEn,
            ]);
          } else {
            plantFertilizersTableDataEn.push({
              period: fertilizer.period,
              headers: [
                'Type of fertilizer',
                'Name',
                'Active substance',
                'Total amount used during the year',
                'Average amount per hectare',
                'Related cultures',
              ],
              unitsData: [
                [
                  fetNameEn,
                  ferNameEn,
                  ferActiveSubstanceEn,
                  cfeDoseUsageDuringYearEn,
                  cfeDoseUsagePerHectarEn,
                  clanNameEn,
                ],
              ],
              totals: [],
            });
          }
        }
      });
    }
  });
  return { plantFertilizersTableDataEt, plantFertilizersTableDataEn };
};

export const sortPlantFertilizersData = (parcels: Parcels) => {
  const { parcelsData } = parcels;
  const plantFertilizersByYear = sortPlantFertilizersByYear(parcelsData);
  const plantFertilizersInChartFormat = sortPlantFertilizersToChartFormat(
    plantFertilizersByYear,
  );
  const plantFertilizersTableData = setPlantFertilizersTableData(
    plantFertilizersByYear,
  );

  return {
    ...plantFertilizersInChartFormat,
    ...plantFertilizersTableData,
  };
};
