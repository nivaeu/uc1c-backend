/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import BigNumber from 'bignumber.js';
import { EntityManager } from 'typeorm';
import { CoSupportSchemes as co_supportschemes } from '../../../../entities/schemas/sc_niva_extra/co_supportschemes.entity';

interface Subsidy {
  amount: BigNumber;
  period: string;
  susId: string;
  susNameEn: string;
  susNameEt: string;
  susNameShortEt: string;
  susNameShortEn: string;
}

interface SubsidyQueryData {
  fap_amount: string;
  fap_year: string;
  sus_id: string;
}

interface SubsidyTableData {
  period: string;
  headers: string[];
  unitsData: any[];
  totals: any[];
}

interface SubsidiesTableData {
  subsidiesTableDataEn: SubsidyTableData[];
  subsidiesTableDataEt: SubsidyTableData[];
}

interface SubsidyByYear {
  period: string;
  name: string;
  shortName: string;
  value: any;
  unit: string;
}

interface SubsidiesByYear {
  subsidiesByYearEn: SubsidyByYear[];
  subsidiesByYearEt: SubsidyByYear[];
}

const querySubsidiesData = async (
  punId: string,
  queryManager: EntityManager,
) => {
  const subsidiesDataQuery = await queryManager.query(
    `SELECT fap_amount, fap_year, sus_id FROM sc_niva_extra.far_dpayment WHERE pun_id = '${punId}'`,
  );

  const subsidiesData: Subsidy[] = [];
  await Promise.all(
    subsidiesDataQuery.map(async (subsidy: SubsidyQueryData) => {
      const { fap_amount: amount, fap_year: period, sus_id: susId } = subsidy;
      const subsidyNamesQuery = await co_supportschemes.find({
        select: ['susNameEt', 'susNameEn', 'susNameShortEt', 'susNameShortEn'],
        where: { susId },
      });

      const {
        susNameEn,
        susNameEt,
        susNameShortEt,
        susNameShortEn,
      } = subsidyNamesQuery[0];

      const subsidyData = {
        amount: new BigNumber(amount),
        period: period.toString(),
        susId,
        susNameEn,
        susNameEt,
        susNameShortEt,
        susNameShortEn,
      };

      subsidiesData.push(subsidyData);
    }),
  );

  return subsidiesData;
};

const convertToCurrencyFormat = (amount: BigNumber) => {
  return new Intl.NumberFormat('fr-FR', {
    style: 'currency',
    currency: 'EUR',
  }).format(amount.toNumber());
};

const addSubsidyUnitsDataForTableAndCovertValueToNumber = (
  subsidiesTableData: SubsidiesTableData,
  subsidiesByYear: SubsidiesByYear,
) => {
  const { subsidiesTableDataEt, subsidiesTableDataEn } = subsidiesTableData;
  const { subsidiesByYearEt, subsidiesByYearEn } = subsidiesByYear;

  subsidiesByYearEt.forEach((subsidy, subsidyIndex) => {
    const tableDataIndex = subsidiesTableDataEt.findIndex(
      (subsidyData) => subsidyData.period === subsidy.period,
    );

    subsidiesTableDataEt[tableDataIndex].unitsData.push([
      subsidy.name,
      convertToCurrencyFormat(subsidy.value),
    ]);

    subsidiesByYearEt[subsidyIndex].value = subsidiesByYearEt[
      subsidyIndex
    ].value.toNumber();
  });

  subsidiesByYearEn.forEach((subsidy, subsidyIndex) => {
    const tableDataIndex = subsidiesTableDataEt.findIndex(
      (subsidyData) => subsidyData.period === subsidy.period,
    );

    subsidiesTableDataEn[tableDataIndex].unitsData.push([
      subsidy.name,
      convertToCurrencyFormat(subsidy.value),
    ]);

    subsidiesByYearEn[subsidyIndex].value = subsidiesByYearEn[
      subsidyIndex
    ].value.toNumber();
  });

  return {
    subsidiesTableDataEt,
    subsidiesTableDataEn,
    subsidiesByYearEt,
    subsidiesByYearEn,
  };
};

const setSubsidiesTableData = (subsidiesData: Subsidy[]) => {
  const subsidiesTableDataEt: SubsidyTableData[] = [];
  const subsidiesTableDataEn: SubsidyTableData[] = [];

  subsidiesData.forEach((subsidyData) => {
    const subsidiesTableDataIndexEt = subsidiesTableDataEt.findIndex(
      (subsidy) => {
        return subsidy.period === subsidyData.period;
      },
    );

    if (subsidiesTableDataIndexEt !== -1) {
      subsidiesTableDataEt[
        subsidiesTableDataIndexEt
      ].totals[1] = subsidiesTableDataEt[
        subsidiesTableDataIndexEt
      ].totals[1].plus(subsidyData.amount);
    } else {
      subsidiesTableDataEt.push({
        period: subsidyData.period,
        headers: ['Toetuse tüüp', 'Summa'],
        unitsData: [],
        totals: ['', subsidyData.amount],
      });
    }

    const subsidiesTableDataIndexEn = subsidiesTableDataEn.findIndex(
      (subsidy) => {
        return subsidy.period === subsidyData.period;
      },
    );

    if (subsidiesTableDataIndexEn !== -1) {
      subsidiesTableDataEn[
        subsidiesTableDataIndexEn
      ].totals[1] = subsidiesTableDataEn[
        subsidiesTableDataIndexEn
      ].totals[1].plus(subsidyData.amount);
    } else {
      subsidiesTableDataEn.push({
        period: subsidyData.period,
        headers: ['Subsidy type', 'Amount'],
        unitsData: [],
        totals: ['', subsidyData.amount],
      });
    }
  });

  subsidiesTableDataEt.forEach((subsidy) => {
    subsidy.totals[1] = convertToCurrencyFormat(subsidy.totals[1]);
  });

  subsidiesTableDataEn.forEach((subsidy) => {
    subsidy.totals[1] = convertToCurrencyFormat(subsidy.totals[1]);
  });

  return {
    subsidiesTableDataEt,
    subsidiesTableDataEn,
  };
};

const sortSubsidiesByYear = (subsidiesData: Subsidy[]) => {
  const subsidiesByYearEn: SubsidyByYear[] = [];
  const subsidiesByYearEt: SubsidyByYear[] = [];

  subsidiesData.forEach((subsidyData) => {
    const indexEt = subsidiesByYearEt.findIndex((subsidy) => {
      return (
        subsidy.period === subsidyData.period &&
        subsidy.name === subsidyData.susNameEt
      );
    });

    const indexEn = subsidiesByYearEn.findIndex((subsidy) => {
      return (
        subsidy.period === subsidyData.period &&
        subsidy.name === subsidyData.susNameEn
      );
    });

    if (indexEt !== -1) {
      subsidiesByYearEt[indexEt].value = subsidiesByYearEt[indexEt].value.plus(
        subsidyData.amount,
      );
    } else {
      subsidiesByYearEt.push({
        period: subsidyData.period,
        name: subsidyData.susNameEt,
        shortName: subsidyData.susNameShortEt,
        value: subsidyData.amount,
        unit: 'eur',
      });
    }

    if (indexEn !== -1) {
      subsidiesByYearEn[indexEn].value = subsidiesByYearEn[indexEn].value.plus(
        subsidyData.amount,
      );
    } else {
      subsidiesByYearEn.push({
        period: subsidyData.period,
        name: subsidyData.susNameEn,
        shortName: subsidyData.susNameShortEn,
        value: subsidyData.amount,
        unit: 'eur',
      });
    }
  });

  return {
    subsidiesByYearEt,
    subsidiesByYearEn,
  };
};

export const sortSubsidiesData = async (
  punId: string,
  queryManager: EntityManager,
) => {
  const subsidiesData = await querySubsidiesData(punId, queryManager);
  const subsidiesTableData = setSubsidiesTableData(subsidiesData);
  const subsidiesByYear = sortSubsidiesByYear(subsidiesData);
  const subsidies = addSubsidyUnitsDataForTableAndCovertValueToNumber(
    subsidiesTableData,
    subsidiesByYear,
  );

  return {
    ...subsidies,
    unit: 'eur',
  };
};
