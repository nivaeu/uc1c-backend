/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import BigNumber from 'bignumber.js';

export const calculateAverageOfNumber = (
  total: BigNumber,
  count: BigNumber,
) => {
  return total.dividedBy(count).decimalPlaces(2);
};
