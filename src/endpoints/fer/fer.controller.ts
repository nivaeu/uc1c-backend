/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Controller, Get } from '@nestjs/common';
import { FerService } from './fer.service';
import { ApiNotFoundResponse, ApiOperation, ApiTags } from '@nestjs/swagger';

@ApiTags('List of fertilizers')
@Controller('fer')
export class FerController {
  constructor(private ferService: FerService) {}

  @Get()
  @ApiOperation({
    summary: 'Get list of fertilizers',
  })
  @ApiNotFoundResponse({
    description: 'Unable to find list of fertilizers data',
  })
  queryFerData() {
    return this.ferService.queryFerData();
  }
}
