/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FerService } from './fer.service';
import { FerController } from './fer.controller';
import { Loc_copfertilizerRepository } from './repositories/loc_co_pfertilizer.repository';

@Module({
  imports: [TypeOrmModule.forFeature([Loc_copfertilizerRepository])],
  providers: [FerService],
  controllers: [FerController],
})
export class FerModule {}
