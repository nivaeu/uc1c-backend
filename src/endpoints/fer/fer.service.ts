/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Loc_copfertilizerRepository } from './repositories/loc_co_pfertilizer.repository';

@Injectable()
export class FerService {
  constructor(
    @InjectRepository(Loc_copfertilizerRepository)
    private loc_copfertilizerRepository: Loc_copfertilizerRepository,
  ) {}

  async queryFerData() {
    const results = await this.loc_copfertilizerRepository.queryFerData();
    return results;
  }
}
