/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Controller, Get } from '@nestjs/common';
import { LabService } from './lab.service';
import { ApiNotFoundResponse, ApiOperation, ApiTags } from '@nestjs/swagger';

@ApiTags('List of agricultural activity types')
@Controller('lab')
export class LabController {
  constructor(private labService: LabService) {}

  @Get()
  @ApiOperation({
    summary: 'Get list of agricultural activity types',
  })
  @ApiNotFoundResponse({
    description: 'Unable to find list of agricultural activity types data',
  })
  queryCrpData() {
    return this.labService.queryLabData();
  }
}
