/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LabService } from './lab.service';
import { LabController } from './lab.controller';
import { Loc_coplabortypeRepository } from './repositories/loc_co_plabortype.repository';

@Module({
  imports: [TypeOrmModule.forFeature([Loc_coplabortypeRepository])],
  providers: [LabService],
  controllers: [LabController],
})
export class LabModule {}
