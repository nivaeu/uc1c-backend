/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { EntityRepository, Repository } from 'typeorm';
import { LocCoPlabortype as loc_co_plabortype } from '../../../entities/schemas/sc_niva_extra/loc/loc_co_plabortype.entity';

@EntityRepository(loc_co_plabortype)
export class Loc_coplabortypeRepository extends Repository<loc_co_plabortype> {
  async queryLabData() {
    const labData = await loc_co_plabortype.find();
    return labData;
  }
}
