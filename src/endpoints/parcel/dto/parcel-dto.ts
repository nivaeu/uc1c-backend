/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { IsNotEmpty, IsOptional } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class QueryAgriCulturalParcelDto {
  @IsOptional()
  @IsNotEmpty()
  @ApiProperty({
    description:
      'Applicant personal-/business code (fad_personalid from the pr_dfarmerdata table)',
    default: '39107184489',
  })
  id: number;

  @IsOptional()
  @ApiProperty({
    example: '2020',
    required: false,
    description: 'Claim year of parcels',
  })
  year: string;
}

export class CreateParcelActivity {
  @IsOptional()
  @IsNotEmpty()
  @ApiProperty({
    description:
      'Sender (identification of the third party application sending the data, ex. PRIA)',
    example: 'PRIA',
  })
  ecrReportSenderParty: string;

  @IsOptional()
  @IsNotEmpty()
  @ApiProperty({
    description:
      'Parcel Id (crpreg_pa_id from the loc_rp_dcropparcel table, ex. 12678208)',
    example: '12678208',
  })
  agriculturalParcelID: string;

  @IsOptional()
  @IsNotEmpty()
  @ApiProperty({
    description:
      'Labor activity id (labId from list of agricultural activity types, ex. 1)',
    example: 1,
  })
  agriCulturalActivityLaborTypeId: string;

  @IsOptional()
  @IsNotEmpty()
  @ApiProperty({
    description: 'Activity start date (ex. 2021-10-10)',
    example: '2021-10-10',
  })
  agriCulturalActivityStartDate: string;

  @IsOptional()
  @IsNotEmpty()
  @ApiProperty({
    description: 'Activity end date (ex. 2021-10-10)',
    example: '2021-10-10',
  })
  agriCulturalActivityEndDate: string;

  @IsOptional()
  @IsNotEmpty()
  @ApiProperty({
    description: 'Applied product dose (ex. 3)',
    example: '3',
  })
  appliedProductDose: string;

  @IsOptional()
  @IsNotEmpty()
  @ApiProperty({
    description:
      'Applied product dose unit id (uniId from the list of measurement units, ex. 10)',
    example: 10,
  })
  appliedProductDoseUnitId: string;

  @IsOptional()
  @IsNotEmpty()
  @ApiProperty({
    description:
      'Applied product id (ferId from the list of fertilizers or phyId from the list of phytosanitary products, ex. 386)',
    example: '386',
  })
  appliedProductId: string;
}
