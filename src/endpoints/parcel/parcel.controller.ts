/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import {
  Controller,
  Get,
  Query,
  ValidationPipe,
  Post,
  Body,
} from '@nestjs/common';
import { ParcelService } from './parcel.service';
import {
  QueryAgriCulturalParcelDto,
  CreateParcelActivity,
} from './dto/parcel-dto';
import {
  ApiNotFoundResponse,
  ApiTags,
  ApiOperation,
  ApiCreatedResponse,
  ApiConsumes,
  ApiConflictResponse,
} from '@nestjs/swagger';

@ApiTags('Parcels data')
@Controller('parcels')
export class ParcelController {
  constructor(private parcelService: ParcelService) {}

  @Get()
  @ApiOperation({
    description:
      'Returns agricultural parcels data by applicant personal-/business code.',
  })
  @ApiNotFoundResponse({ description: 'Unable to find parcels data' })
  queryAgriCulturalParcelsData(
    @Query(ValidationPipe) queryDto: QueryAgriCulturalParcelDto,
  ) {
    return this.parcelService.queryAgriCulturalParcelsData(queryDto);
  }

  @Post()
  @ApiOperation({
    description:
      'Update agricultural parcel data with activities performed and fertilizers/plant protection products used.',
  })
  @ApiCreatedResponse({ description: 'Agricultural parcel activity added' })
  @ApiConflictResponse({
    description: 'Failed to add agricultural parcel activity',
  })
  @ApiConsumes()
  create(@Body() body: CreateParcelActivity) {
    return this.parcelService.createParcelActivity(body);
  }
}
