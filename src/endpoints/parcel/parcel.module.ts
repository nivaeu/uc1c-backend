/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ParcelService } from './parcel.service';
import { ParcelController } from './parcel.controller';
import { RP_dcropparcelRepository } from './repositories/rp_dcropparcel.repository';

@Module({
  imports: [TypeOrmModule.forFeature([RP_dcropparcelRepository])],
  providers: [ParcelService],
  controllers: [ParcelController],
})
export class ParcelModule {}
