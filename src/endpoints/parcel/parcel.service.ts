/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Injectable, NotFoundException, Catch } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { RP_dcropparcelRepository } from './repositories/rp_dcropparcel.repository';
import {
  QueryAgriCulturalParcelDto,
  CreateParcelActivity,
} from './dto/parcel-dto';

@Catch()
@Injectable()
export class ParcelService {
  constructor(
    @InjectRepository(RP_dcropparcelRepository)
    private rp_dcropparcelRepository: RP_dcropparcelRepository,
  ) {}

  async queryAgriCulturalParcelsData(queryDto: QueryAgriCulturalParcelDto) {
    const results = await this.rp_dcropparcelRepository.queryAgriCulturalParcelsData(
      queryDto,
    );

    if (results.length !== 0) {
      return results;
    } else {
      throw new NotFoundException('Parcel(s) not found.');
    }
  }

  async createParcelActivity(body: CreateParcelActivity) {
    const response = await this.rp_dcropparcelRepository.createParcelActivity(
      body,
    );
    return response;
  }
}
