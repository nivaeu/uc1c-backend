/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { EntityManager, EntityRepository, Repository } from 'typeorm';
import { BadRequestException } from '@nestjs/common';
import { RpDcropparcel as rp_dcropparcel } from '../../../entities/schemas/sc_niva/rp_dcropparcel.entity';
import {
  QueryAgriCulturalParcelDto,
  CreateParcelActivity,
} from '../dto/parcel-dto';
import {
  queryParcelHectares,
  queryAblIdsAndKeys,
  queryRfpId,
  queryGeneralParcelData,
} from 'src/endpoints/farmer/repositories/utils/parcels';
import { queryPunId } from 'src/endpoints/farmer/repositories/pr_dfarmerdata.repository';
import { PrDproductionunit as pr_dproductionunit } from '../../../entities/schemas/sc_niva/pr_dproductionunit.entity';
import { AbDagrblock as ab_dagrblock } from '../../../entities/schemas/sc_niva/ab_dagrblock.entity';
import { RpDrefplot as rp_drefplot } from '../../../entities/schemas/sc_niva/rp_drefplot.entity';
import { getConnection } from 'typeorm';
import { RpDcropfertilizer } from 'src/entities/schemas/sc_niva/rp_dcropfertilizer.entity';
import { RpDcropphytosanit } from 'src/entities/schemas/sc_niva/rp_dcropphytosanit.entity';
import { AblIdsAndKeysQueryData } from '../../farmer/repositories/utils/parcels';

const queryAgriculturalParcelId = async (
  rcpId: string,
  queryManager: EntityManager,
) => {
  const agriculturalParcelIdQuery = await queryManager.query(
    `SELECT crpreg_pa_id FROM sc_niva_extra.loc_rp_dcropparcel WHERE rcp_id = '${rcpId}'`,
  );

  const { crpreg_pa_id: agriculturalParcelId } = agriculturalParcelIdQuery[0];
  return agriculturalParcelId;
};

const queryFarIdByFadPersonalId = async (
  fadPersonalId: number,
  queryManager: EntityManager,
) => {
  const farIdQuery = await queryManager.query(
    `SELECT far_id FROM sc_niva.pr_dfarmerdata WHERE fad_personalid = '${fadPersonalId}'`,
  );

  if (farIdQuery.length !== 0) {
    const { far_id: farId } = farIdQuery[0];
    return farId;
  }
};

const queryCropVariety = async (crvId: string, queryManager: EntityManager) => {
  const cropVarietyQuery = await queryManager.query(
    `SELECT crv_name FROM sc_niva.co_pcropvariety WHERE crv_id = '${crvId}'`,
  );

  if (cropVarietyQuery.length !== 0) {
    const { crv_name: crvName } = cropVarietyQuery[0];
    return crvName;
  }
};

const queryParcelGeometryInGeoJSONFormat = async (
  rcpId: string,
  queryManager: EntityManager,
) => {
  const geometryQuery = await queryManager.query(
    `SELECT ST_AsGeoJSON(cpg_geometry) FROM sc_niva.rp_dcropparcelgeom WHERE rcp_id = '${rcpId}'`,
  );

  if (geometryQuery.length !== 0) {
    const { st_asgeojson: geometry } = geometryQuery[0];
    return geometry;
  }
};

const queryCropType = async (crpId: string, queryManager: EntityManager) => {
  const cropTypeQuery = await queryManager.query(
    `SELECT crp_name_en FROM sc_niva_extra.loc_co_pcropproduct WHERE crp_id = '${crpId}'`,
  );

  if (cropTypeQuery.length !== 0) {
    const { crp_name_en: cropType } = cropTypeQuery[0];
    return cropType;
  }
};

const queryFilteredParcelsData = async (
  queryDto: QueryAgriCulturalParcelDto,
  queryManager: EntityManager,
) => {
  const { id: fadPersonalId, year } = queryDto;
  const farId = await queryFarIdByFadPersonalId(fadPersonalId, queryManager);
  const punId = await queryPunId(pr_dproductionunit, farId);
  const ablIdsAndKeys: AblIdsAndKeysQueryData[] = await queryAblIdsAndKeys(
    ab_dagrblock,
    punId,
  );

  const parcelsData: any[] = [];

  await Promise.all(
    ablIdsAndKeys.map(async (abl) => {
      const { ablId, ablKey: referenceParcelNo } = abl;
      const rfpId = await queryRfpId(rp_drefplot, ablId);
      const generalParcelData = await queryGeneralParcelData(
        rfpId,
        queryManager,
      );

      if (generalParcelData.length !== 0) {
        await Promise.all(
          generalParcelData.map(async (parcelData) => {
            const {
              crpId,
              crvId,
              period: claimYear,
              rcpId,
              rcpKey: agriculturalParcelNo,
            } = parcelData;

            const agriculturalParcelID = await queryAgriculturalParcelId(
              rcpId,
              queryManager,
            );
            const area = await queryParcelHectares(rcpId, queryManager);

            const cropType = await queryCropType(crpId, queryManager);

            const cropVariety = await queryCropVariety(crvId, queryManager);
            const geometry = await queryParcelGeometryInGeoJSONFormat(
              rcpId,
              queryManager,
            );

            const parcel = {
              claimYear,
              agriculturalParcelID,
              agriculturalParcelNo,
              area,
              cropType,
              cropVariety,
              referenceParcelNo,
              geometry,
            };

            parcelsData.push(parcel);
          }),
        );
      }
    }),
  );

  if (year) {
    return parcelsData.filter((parcel) => parcel.claimYear === year);
  } else {
    return parcelsData;
  }
};

const checkIfFertilizerLaborType = (appliedProductId: string) => {
  const fertilizerLabnorTypeIds = [
    '20',
    '21',
    '22',
    '29',
    '30',
    '23',
    '24',
    '25',
    '26',
    '27',
    '28',
  ];
  if (fertilizerLabnorTypeIds.includes(appliedProductId)) {
    return true;
  }
};

const queryRcpIdByParcelId = async (
  agriculturalParcelID: string,
  queryManager: EntityManager,
) => {
  const rcpIdQuery = await queryManager.query(
    `SELECT rcp_id FROM sc_niva_extra.loc_rp_dcropparcel WHERE crpreg_pa_id = '${agriculturalParcelID}'`,
  );

  if (rcpIdQuery.length !== 0) {
    const { rcp_id: rcpId } = rcpIdQuery[0];
    return rcpId;
  }
};

const queryFetId = async (rcpId: string, queryManager: EntityManager) => {
  const fetIdQuery = await queryManager.query(
    `SELECT fet_id FROM sc_niva.rp_dcropfertilizer WHERE rcp_id = '${rcpId}'`,
  );

  if (fetIdQuery.length !== 0) {
    const { fet_id: fetId } = fetIdQuery[0];
    return fetId;
  }
};

@EntityRepository(rp_dcropparcel)
export class RP_dcropparcelRepository extends Repository<rp_dcropparcel> {
  async queryAgriCulturalParcelsData(queryDto: QueryAgriCulturalParcelDto) {
    const queryManager = this.manager;
    const parcelsData = await queryFilteredParcelsData(queryDto, queryManager);
    return parcelsData;
  }

  async createParcelActivity(body: CreateParcelActivity) {
    const {
      agriculturalParcelID,
      agriCulturalActivityStartDate,
      agriCulturalActivityEndDate,
      appliedProductDose,
      appliedProductId,
      appliedProductDoseUnitId,
      agriCulturalActivityLaborTypeId,
    } = body;

    const queryManager = this.manager;

    const rcpId = await queryRcpIdByParcelId(
      agriculturalParcelID,
      queryManager,
    );

    try {
      if (rcpId) {
        const isFertilizerLaborType = checkIfFertilizerLaborType(
          agriCulturalActivityLaborTypeId,
        );

        if (isFertilizerLaborType) {
          const fetId = await queryFetId(rcpId, queryManager);
          const result = await getConnection()
            .createQueryBuilder()
            .insert()
            .into(RpDcropfertilizer)
            .values({
              rcpId,
              fetId,
              cfeDose: appliedProductDose,
              cfeStartdate: agriCulturalActivityStartDate,
              cfeEnddate: agriCulturalActivityEndDate,
              uniId: Number(appliedProductDoseUnitId),
              ferId: appliedProductId,
            })
            .returning('*')
            .execute();

          return result.raw[0];
        } else {
          const result = await getConnection()
            .createQueryBuilder()
            .insert()
            .into(RpDcropphytosanit)
            .values({
              rcpId,
              phyId: appliedProductId,
              cphDose: appliedProductDose,
              cphStartdate: agriCulturalActivityStartDate,
              cphEnddate: agriCulturalActivityEndDate,
              uniId: Number(appliedProductDoseUnitId),
              couId: 'EE',
            })
            .returning('*')
            .execute();

          return result.raw[0];
        }
      }
    } catch (error: any) {
      if (error.name === 'QueryFailedError') {
        if (
          /^duplicate key value violates unique constraint/.test(error.message)
        ) {
          throw new BadRequestException(error.detail);
        } else if (/violates foreign key constraint/.test(error.message)) {
          throw new BadRequestException(error.detail);
        } else {
          throw error;
        }
      } else {
        throw error;
      }
    }
  }
}
