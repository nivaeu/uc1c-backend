/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { IsNotEmpty, IsOptional } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ParcelActivitiesDto {
  @IsOptional()
  @IsNotEmpty()
  @ApiProperty({
    description:
      'Parcel Id (crpreg_pa_id from the loc_rp_dcropparcel table, ex. 12678208)',
  })
  parcelId: string;
}
