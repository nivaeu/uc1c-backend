/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Controller, Get, Query, ValidationPipe } from '@nestjs/common';
import { ParcelActivitiesService } from './parcelActivities.service';
import { ApiNotFoundResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { ParcelActivitiesDto } from './dto/parcel-activities-dto';

@ApiTags('Get list of agricultural parcel activities')
@Controller('parcelActivities')
export class ParcelActivitiesController {
  constructor(private parcelActivitiesService: ParcelActivitiesService) {}

  @Get()
  @ApiOperation({
    summary: 'Get agricultural parcel activities',
  })
  @ApiNotFoundResponse({
    description: 'Unable to find agricultural parcel activities',
  })
  queryParcelActivities(@Query(ValidationPipe) queryDto: ParcelActivitiesDto) {
    return this.parcelActivitiesService.queryParcelActivities(queryDto);
  }
}
