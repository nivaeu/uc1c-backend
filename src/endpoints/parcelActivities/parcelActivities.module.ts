/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ParcelActivitiesService } from './parcelActivities.service';
import { ParcelActivitiesController } from './parcelActivities.controller';
import { Loc_rpdcropparcelRepository } from './repositories/loc_rp_dcropparcel.repository';

@Module({
  imports: [TypeOrmModule.forFeature([Loc_rpdcropparcelRepository])],
  providers: [ParcelActivitiesService],
  controllers: [ParcelActivitiesController],
})
export class ParcelActivitiesModule {}
