/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Loc_rpdcropparcelRepository } from './repositories/loc_rp_dcropparcel.repository';
import { ParcelActivitiesDto } from './dto/parcel-activities-dto';

@Injectable()
export class ParcelActivitiesService {
  constructor(
    @InjectRepository(Loc_rpdcropparcelRepository)
    private loc_rpdcropparcelRepository: Loc_rpdcropparcelRepository,
  ) {}

  async queryParcelActivities(queryDto: ParcelActivitiesDto) {
    const result = await this.loc_rpdcropparcelRepository.queryParcelActivities(
      queryDto,
    );
    return result;
  }
}
