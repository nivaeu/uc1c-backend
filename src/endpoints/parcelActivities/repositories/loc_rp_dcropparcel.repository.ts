/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { EntityManager, EntityRepository, Repository } from 'typeorm';
import { LocRpDcropParcel as loc_rp_dcropparcel } from 'src/entities/schemas/sc_niva_extra/loc/loc_rp_dcropparcel.entity';
import { ParcelActivitiesDto } from '../dto/parcel-activities-dto';

const queryRcpId = async (parcelId: string) => {
  const rcpIdQuery = await loc_rp_dcropparcel.find({
    select: ['rcpId'],
    where: [{ crpegPaId: parcelId }],
  });

  if (rcpIdQuery.length !== 0) {
    return rcpIdQuery[0].rcpId;
  }
};

const queryFertilizerActivities = async (
  rcpId: string,
  queryManager: EntityManager,
) => {
  const fertilizerActivities = await queryManager.query(
    `SELECT * FROM sc_niva.rp_dcropfertilizer WHERE rcp_id = '${rcpId}'`,
  );
  return fertilizerActivities;
};

const queryPhytoSanitaryActivities = async (
  rcpId: string,
  queryManager: EntityManager,
) => {
  const phytoSanitaryActivities = await queryManager.query(
    `SELECT * FROM sc_niva.rp_dcropphytosanit WHERE rcp_id = '${rcpId}'`,
  );
  return phytoSanitaryActivities;
};

@EntityRepository(loc_rp_dcropparcel)
export class Loc_rpdcropparcelRepository extends Repository<loc_rp_dcropparcel> {
  async queryParcelActivities(queryDto: ParcelActivitiesDto) {
    const { parcelId } = queryDto;
    const queryManager = this.manager;
    const rcpId = await queryRcpId(parcelId);
    if (rcpId) {
      const fertilizerActivities = await queryFertilizerActivities(
        rcpId,
        queryManager,
      );
      const phytoSanitaryActivities = await queryPhytoSanitaryActivities(
        rcpId,
        queryManager,
      );

      return {
        fertilizerActivities,
        phytoSanitaryActivities,
      };
    }
  }
}
