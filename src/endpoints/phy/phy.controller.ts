/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Controller, Get } from '@nestjs/common';
import { PhyService } from './phy.service';
import { ApiNotFoundResponse, ApiOperation, ApiTags } from '@nestjs/swagger';

@ApiTags('List of phytosanitary products')
@Controller('phy')
export class PhyController {
  constructor(private phyService: PhyService) {}

  @Get()
  @ApiOperation({
    summary: 'Get list of phytosanitary products',
  })
  @ApiNotFoundResponse({
    description: 'Unable to find list of phytosanitary products data',
  })
  queryPhyData() {
    return this.phyService.queryPhyData();
  }
}
