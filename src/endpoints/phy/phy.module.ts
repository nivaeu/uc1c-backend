/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PhyService } from './phy.service';
import { PhyController } from './phy.controller';
import { Loc_copphytosanitaryRepository } from './repositories/loc_co_pphytosanitary.repository';

@Module({
  imports: [TypeOrmModule.forFeature([Loc_copphytosanitaryRepository])],
  providers: [PhyService],
  controllers: [PhyController],
})
export class PhyModule {}
