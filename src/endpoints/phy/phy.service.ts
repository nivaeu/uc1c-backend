/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Loc_copphytosanitaryRepository } from './repositories/loc_co_pphytosanitary.repository';

@Injectable()
export class PhyService {
  constructor(
    @InjectRepository(Loc_copphytosanitaryRepository)
    private loc_copphytosanitaryRepository: Loc_copphytosanitaryRepository,
  ) {}

  async queryPhyData() {
    const results = await this.loc_copphytosanitaryRepository.queryPhyData();
    return results;
  }
}
