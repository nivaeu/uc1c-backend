/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { EntityRepository, Repository } from 'typeorm';
import { LocCoPunittype as loc_co_punittype } from '../../../entities/schemas/sc_niva_extra/loc/loc_co_punittype.entity';

@EntityRepository(loc_co_punittype)
export class Loc_copunittypeRepository extends Repository<loc_co_punittype> {
  async queryUniData() {
    const uniData = await loc_co_punittype.find();
    return uniData;
  }
}
