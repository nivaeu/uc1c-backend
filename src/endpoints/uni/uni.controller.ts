/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Controller, Get } from '@nestjs/common';
import { UniService } from './uni.service';
import { ApiNotFoundResponse, ApiOperation, ApiTags } from '@nestjs/swagger';

@ApiTags('List of measurement units')
@Controller('uni')
export class UniController {
  constructor(private uniService: UniService) {}

  @Get()
  @ApiOperation({
    summary: 'Get list of measurement units',
  })
  @ApiNotFoundResponse({
    description: 'Unable to find list of measurement units data',
  })
  queryUniData() {
    return this.uniService.queryUniData();
  }
}
