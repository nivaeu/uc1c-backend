/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UniService } from './uni.service';
import { UniController } from './uni.controller';
import { Loc_copunittypeRepository } from './repositories/loc_co_punittype.repository';

@Module({
  imports: [TypeOrmModule.forFeature([Loc_copunittypeRepository])],
  providers: [UniService],
  controllers: [UniController],
})
export class UniModule {}
