/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Loc_copunittypeRepository } from './repositories/loc_co_punittype.repository';

@Injectable()
export class UniService {
  constructor(
    @InjectRepository(Loc_copunittypeRepository)
    private loc_copunittypeRepository: Loc_copunittypeRepository,
  ) {}

  async queryUniData() {
    const results = await this.loc_copunittypeRepository.queryUniData();
    return results;
  }
}
