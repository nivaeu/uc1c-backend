/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { PrDproductionunit } from './pr_dproductionunit.entity';
import { AbDgeometry } from './ab_dgeometry.entity';
import { AbDlandcover } from './ab_dlandcover.entity';
import { RpDrefplot } from './rp_drefplot.entity';

@Index('pk_ab_dagrblock', ['ablId'], { unique: true })
@Entity('ab_dagrblock', { schema: 'sc_niva' })
export class AbDagrblock extends BaseEntity {
  @Column('uuid', { primary: true, name: 'abl_id' })
  ablId: string;

  @Column('character varying', { name: 'abl_key', length: 50 })
  ablKey: string;

  @Column('date', { name: 'abl_datefrom', nullable: true })
  ablDatefrom: string | null;

  @Column('date', { name: 'abl_dateto', nullable: true })
  ablDateto: string | null;

  @ManyToOne(
    () => PrDproductionunit,
    (prDproductionunit) => prDproductionunit.abDagrblocks,
  )
  @JoinColumn([{ name: 'pun_id', referencedColumnName: 'punId' }])
  punId: PrDproductionunit;

  @OneToMany(() => AbDgeometry, (abDgeometry) => abDgeometry.abl)
  abDgeometries: AbDgeometry[];

  @OneToMany(() => AbDlandcover, (abDlandcover) => abDlandcover.abl)
  abDlandcovers: AbDlandcover[];

  @OneToMany(() => RpDrefplot, (rpDrefplot) => rpDrefplot.abl)
  rpDrefplots: RpDrefplot[];
}
