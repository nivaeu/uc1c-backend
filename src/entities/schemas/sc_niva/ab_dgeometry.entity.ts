/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
} from 'typeorm';
import { AbDagrblock } from './ab_dagrblock.entity';

@Index('pk_ab_dgeometry', ['ablId', 'ageDatefrom'], { unique: true })
@Entity('ab_dgeometry', { schema: 'sc_niva' })
export class AbDgeometry extends BaseEntity {
  @Column('uuid', { primary: true, name: 'abl_id' })
  ablId: string;

  @Column('geometry', { name: 'age_geometry', srid: 3301 })
  ageGeometry: string;

  @Column('numeric', {
    name: 'age_surface',
    nullable: true,
    precision: 10,
    scale: 2,
  })
  ageSurface: string | null;

  @Column('date', { primary: true, name: 'age_datefrom' })
  ageDatefrom: string;

  @Column('date', { name: 'age_dateto', nullable: true })
  ageDateto: string | null;

  @ManyToOne(() => AbDagrblock, (abDagrblock) => abDagrblock.abDgeometries)
  @JoinColumn([{ name: 'abl_id', referencedColumnName: 'ablId' }])
  abl: AbDagrblock;
}
