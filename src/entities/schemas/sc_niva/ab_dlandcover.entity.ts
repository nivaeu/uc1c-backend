/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
} from 'typeorm';
import { AbDagrblock } from './ab_dagrblock.entity';
import { CoPlandcover } from './co_plandcover.entity';

@Index('pk_ab_dlandcover', ['ablId', 'alcDatefrom', 'lanId'], { unique: true })
@Entity('ab_dlandcover', { schema: 'sc_niva' })
export class AbDlandcover extends BaseEntity {
  @Column('uuid', { primary: true, name: 'abl_id' })
  ablId: string;

  @Column('smallint', { primary: true, name: 'lan_id' })
  lanId: number;

  @Column('date', { primary: true, name: 'alc_datefrom' })
  alcDatefrom: string;

  @Column('date', { name: 'alc_dateto', nullable: true })
  alcDateto: string | null;

  @ManyToOne(() => AbDagrblock, (abDagrblock) => abDagrblock.abDlandcovers)
  @JoinColumn([{ name: 'abl_id', referencedColumnName: 'ablId' }])
  abl: AbDagrblock;

  @ManyToOne(() => CoPlandcover, (coPlandcover) => coPlandcover.abDlandcovers)
  @JoinColumn([{ name: 'lan_id', referencedColumnName: 'lanId' }])
  lan: CoPlandcover;
}
