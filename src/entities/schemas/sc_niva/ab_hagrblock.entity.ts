/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { RevDrevinfo } from './rev_drevinfo.entity';

@Index('pk_ab_hagrblock', ['ablId', 'rev'], { unique: true })
@Entity('ab_hagrblock', { schema: 'sc_niva' })
export class AbHagrblock {
  @Column('uuid', { primary: true, name: 'abl_id' })
  ablId: string;

  @Column('character varying', { name: 'abl_key', length: 50 })
  ablKey: string;

  @Column('uuid', { name: 'pun_id' })
  punId: string;

  @Column('date', { name: 'abl_datefrom', nullable: true })
  ablDatefrom: string | null;

  @Column('date', { name: 'abl_dateto', nullable: true })
  ablDateto: string | null;

  @Column('integer', { primary: true, name: 'rev' })
  rev: number;

  @Column('smallint', { name: 'revtype', nullable: true })
  revtype: number | null;

  @ManyToOne(() => RevDrevinfo, (revDrevinfo) => revDrevinfo.abHagrblocks)
  @JoinColumn([{ name: 'rev', referencedColumnName: 'rev' }])
  rev2: RevDrevinfo;
}
