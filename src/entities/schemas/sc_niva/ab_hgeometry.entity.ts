/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { RevDrevinfo } from './rev_drevinfo.entity';

@Index('pk_ab_hgeometry', ['ablId', 'ageDatefrom', 'rev'], { unique: true })
@Entity('ab_hgeometry', { schema: 'sc_niva' })
export class AbHgeometry {
  @Column('uuid', { primary: true, name: 'abl_id' })
  ablId: string;

  @Column('geometry', { name: 'age_geometry', srid: 3301 })
  ageGeometry: string;

  @Column('numeric', {
    name: 'age_surface',
    nullable: true,
    precision: 10,
    scale: 2,
  })
  ageSurface: string | null;

  @Column('date', { name: 'age_datefrom' })
  ageDatefrom: string;

  @Column('date', { name: 'age_dateto', nullable: true })
  ageDateto: string | null;

  @Column('integer', { primary: true, name: 'rev' })
  rev: number;

  @Column('smallint', { name: 'revtype', nullable: true })
  revtype: number | null;

  @ManyToOne(() => RevDrevinfo, (revDrevinfo) => revDrevinfo.abHgeometries)
  @JoinColumn([{ name: 'rev', referencedColumnName: 'rev' }])
  rev2: RevDrevinfo;
}
