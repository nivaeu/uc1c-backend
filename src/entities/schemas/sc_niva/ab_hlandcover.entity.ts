/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { RevDrevinfo } from './rev_drevinfo.entity';

@Index('pk_ab_hlandcover', ['ablId', 'alcDatefrom', 'lanId', 'rev'], {
  unique: true,
})
@Entity('ab_hlandcover', { schema: 'sc_niva' })
export class AbHlandcover {
  @Column('uuid', { primary: true, name: 'abl_id' })
  ablId: string;

  @Column('smallint', { primary: true, name: 'lan_id' })
  lanId: number;

  @Column('date', { primary: true, name: 'alc_datefrom' })
  alcDatefrom: string;

  @Column('date', { name: 'alc_dateto', nullable: true })
  alcDateto: string | null;

  @Column('integer', { primary: true, name: 'rev' })
  rev: number;

  @Column('smallint', { name: 'revtype', nullable: true })
  revtype: number | null;

  @ManyToOne(() => RevDrevinfo, (revDrevinfo) => revDrevinfo.abHlandcovers)
  @JoinColumn([{ name: 'rev', referencedColumnName: 'rev' }])
  rev2: RevDrevinfo;
}
