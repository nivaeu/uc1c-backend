/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, OneToMany } from 'typeorm';
import { CoPcropprodgroup } from './co_pcropprodgroup.entity';
import { CoPcropvariety } from './co_pcropvariety.entity';
import { CoPphytosanitary } from './co_pphytosanitary.entity';
import { PrDfarmer } from './pr_dfarmer.entity';
import { PrDproductionunit } from './pr_dproductionunit.entity';
import { RpDcropphytosanit } from './rp_dcropphytosanit.entity';
import { SecDuser } from './sec_duser.entity';
import { CoNutsclassification } from '../sc_niva_extra/co_nutsclassification.entity';

@Index('pk_co_country', ['couId'], { unique: true })
@Entity('co_pcountry', { schema: 'sc_niva' })
export class CoPcountry {
  @Column('character varying', { primary: true, name: 'cou_id', length: 2 })
  couId: string;

  @Column('character varying', { name: 'cou_name', length: 255 })
  couName: string;

  @Column('date', { name: 'cou_expirydate', nullable: true })
  couExpirydate: string | null;

  @OneToMany(() => CoPcropprodgroup, (coPcropprodgroup) => coPcropprodgroup.cou)
  coPcropprodgroups: CoPcropprodgroup[];

  @OneToMany(() => CoPcropvariety, (coPcropvariety) => coPcropvariety.cou)
  coPcropvarieties: CoPcropvariety[];

  @OneToMany(() => CoPphytosanitary, (coPphytosanitary) => coPphytosanitary.cou)
  coPphytosanitaries: CoPphytosanitary[];

  @OneToMany(() => PrDfarmer, (prDfarmer) => prDfarmer.cou)
  prDfarmers: PrDfarmer[];

  @OneToMany(
    () => CoNutsclassification,
    (coNutsclassification) => coNutsclassification.couId,
  )
  coNutsclassifications: CoNutsclassification[];

  @OneToMany(
    () => PrDproductionunit,
    (prDproductionunit) => prDproductionunit.cou,
  )
  prDproductionunits: PrDproductionunit[];

  @OneToMany(
    () => RpDcropphytosanit,
    (rpDcropphytosanit) => rpDcropphytosanit.cou,
  )
  rpDcropphytosanits: RpDcropphytosanit[];

  @OneToMany(() => SecDuser, (secDuser) => secDuser.cou)
  secDusers: SecDuser[];
}
