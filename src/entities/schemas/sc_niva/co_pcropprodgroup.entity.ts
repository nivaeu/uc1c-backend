/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { CoPcountry } from './co_pcountry.entity';
import { CoPcropproduct } from './co_pcropproduct.entity';

@Index('pk_co_pcropprodgroup', ['cpgId'], { unique: true })
@Entity('co_pcropprodgroup', { schema: 'sc_niva' })
export class CoPcropprodgroup {
  @Column('smallint', { primary: true, name: 'cpg_id' })
  cpgId: number;

  @Column('character varying', { name: 'cpg_name', length: 255 })
  cpgName: string;

  @Column('date', { name: 'cpg_expirydate', nullable: true })
  cpgExpirydate: string | null;

  @ManyToOne(() => CoPcountry, (coPcountry) => coPcountry.coPcropprodgroups)
  @JoinColumn([{ name: 'cou_id', referencedColumnName: 'couId' }])
  cou: CoPcountry;

  @OneToMany(() => CoPcropproduct, (coPcropproduct) => coPcropproduct.cpg)
  coPcropproducts: CoPcropproduct[];
}
