/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { CoPcropprodgroup } from './co_pcropprodgroup.entity';
import { CoPcropvariety } from './co_pcropvariety.entity';
import { RpDcropparcel } from './rp_dcropparcel.entity';

@Index('pk_co_pcropproduct', ['crpId'], { unique: true })
@Entity('co_pcropproduct', { schema: 'sc_niva' })
export class CoPcropproduct {
  @Column('character varying', { primary: true, name: 'crp_id', length: 6 })
  crpId: string;

  @Column('character varying', { name: 'crp_name', length: 255 })
  crpName: string;

  @Column('character varying', {
    name: 'crp_botanicalname',
    nullable: true,
    length: 255,
  })
  crpBotanicalname: string | null;

  @Column('date', { name: 'crp_expirydate', nullable: true })
  crpExpirydate: string | null;

  @ManyToOne(
    () => CoPcropprodgroup,
    (coPcropprodgroup) => coPcropprodgroup.coPcropproducts,
  )
  @JoinColumn([{ name: 'cpg_id', referencedColumnName: 'cpgId' }])
  cpg: CoPcropprodgroup;

  @OneToMany(() => CoPcropvariety, (coPcropvariety) => coPcropvariety.crp)
  coPcropvarieties: CoPcropvariety[];

  @OneToMany(() => RpDcropparcel, (rpDcropparcel) => rpDcropparcel.crp)
  rpDcropparcels: RpDcropparcel[];
}
