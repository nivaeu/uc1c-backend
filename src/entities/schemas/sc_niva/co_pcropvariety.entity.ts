/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToOne,
} from 'typeorm';
import { CoPcountry } from './co_pcountry.entity';
import { CoPcropproduct } from './co_pcropproduct.entity';
import { LocCoPcropvariety } from '../sc_niva_extra/loc/loc_co_pcropvariety.entity';

@Index('pk_co_pcropvariety', ['crvId'], { unique: true })
@Entity('co_pcropvariety', { schema: 'sc_niva' })
export class CoPcropvariety {
  @Column('character varying', { primary: true, name: 'crp_id', length: 6 })
  crpId: string;

  @Column('character varying', { primary: true, name: 'crv_id', length: 5 })
  crvId: string;

  @Column('character varying', { primary: true, name: 'cou_id', length: 2 })
  couId: string;

  @Column('character varying', { name: 'crv_name', length: 255 })
  crvName: string;

  @Column('date', { name: 'crv_expirydate', nullable: true })
  crvExpirydate: string | null;

  @ManyToOne(() => CoPcountry, (coPcountry) => coPcountry.coPcropvarieties)
  @JoinColumn([{ name: 'cou_id', referencedColumnName: 'couId' }])
  cou: CoPcountry;

  @ManyToOne(
    () => CoPcropproduct,
    (coPcropproduct) => coPcropproduct.coPcropvarieties,
  )
  @JoinColumn([{ name: 'crp_id', referencedColumnName: 'crpId' }])
  crp: CoPcropproduct;

  @OneToOne(
    () => LocCoPcropvariety,
    (locCoPcropvariety) => locCoPcropvariety.coPcropvariety,
  )
  locCoPcropvariety: LocCoPcropvariety;
}
