/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, OneToMany } from 'typeorm';
import { RpDcropactivity } from './rp_dcropactivity.entity';

@Index('pk_co_pcultivationdet', ['cudId'], { unique: true })
@Entity('co_pcultivationdet', { schema: 'sc_niva' })
export class CoPcultivationdet {
  @Column('smallint', { primary: true, name: 'cud_id' })
  cudId: number;

  @Column('character varying', { name: 'cud_name', length: 255 })
  cudName: string;

  @Column('date', { name: 'cud_expirydate', nullable: true })
  cudExpirydate: string | null;

  @OneToMany(() => RpDcropactivity, (rpDcropactivity) => rpDcropactivity.cud)
  rpDcropactivities: RpDcropactivity[];
}
