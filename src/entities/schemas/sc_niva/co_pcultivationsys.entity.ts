/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, OneToMany } from 'typeorm';
import { RpDcropactivity } from './rp_dcropactivity.entity';

@Index('pk_co_pcultivationsys', ['cusId'], { unique: true })
@Entity('co_pcultivationsys', { schema: 'sc_niva' })
export class CoPcultivationsys {
  @Column('smallint', { primary: true, name: 'cus_id' })
  cusId: number;

  @Column('character varying', { name: 'cus_name', length: 255 })
  cusName: string;

  @Column('date', { name: 'cus_expirydate', nullable: true })
  cusExpirydate: string | null;

  @OneToMany(() => RpDcropactivity, (rpDcropactivity) => rpDcropactivity.cus)
  rpDcropactivities: RpDcropactivity[];
}
