/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, OneToMany } from 'typeorm';
import { PrDfarmerdata } from './pr_dfarmerdata.entity';

@Index('pk_co_pfarmertype', ['fatId'], { unique: true })
@Entity('co_pfarmertype', { schema: 'sc_niva' })
export class CoPfarmertype {
  @Column('smallint', { primary: true, name: 'fat_id' })
  fatId: number;

  @Column('character varying', { name: 'fat_name', length: 255 })
  fatName: string;

  @Column('date', { name: 'fat_expirydate', nullable: true })
  fatExpirydate: string | null;

  @OneToMany(() => PrDfarmerdata, (prDfarmerdata) => prDfarmerdata.fatId)
  prDfarmerdata: PrDfarmerdata[];
}
