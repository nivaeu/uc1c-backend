/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, OneToMany } from 'typeorm';
import { PrDfarmingtype } from './pr_dfarmingtype.entity';

@Index('pk_co_pfarmingtype', ['frtId'], { unique: true })
@Entity('co_pfarmingtype', { schema: 'sc_niva' })
export class CoPfarmingtype {
  @Column('character varying', { primary: true, name: 'frt_id', length: 3 })
  frtId: string;

  @Column('character varying', { name: 'frt_name', length: 255 })
  frtName: string;

  @Column('date', { name: 'frt_expirydate', nullable: true })
  frtExpirydate: string | null;

  @OneToMany(() => PrDfarmingtype, (prDfarmingtype) => prDfarmingtype.frt)
  prDfarmingtypes: PrDfarmingtype[];
}
