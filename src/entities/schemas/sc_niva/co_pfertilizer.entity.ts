/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { CoPfertilizertype } from './co_pfertilizertype.entity';
import { RpDcropfertilizer } from './rp_dcropfertilizer.entity';

@Index('pk_co_pfertilizer', ['ferId'], { unique: true })
@Entity('co_pfertilizer', { schema: 'sc_niva' })
export class CoPfertilizer {
  @Column('smallint', { primary: true, name: 'fer_id' })
  ferId: number;

  @Column('character varying', { name: 'fer_name', length: 255 })
  ferName: string;

  @Column('date', { name: 'fer_expirydate', nullable: true })
  ferExpirydate: string | null;

  @ManyToOne(
    () => CoPfertilizertype,
    (coPfertilizertype) => coPfertilizertype.coPfertilizers,
  )
  @JoinColumn([{ name: 'fet_id', referencedColumnName: 'fetId' }])
  fet: CoPfertilizertype;

  @OneToMany(
    () => RpDcropfertilizer,
    (rpDcropfertilizer) => rpDcropfertilizer.fer,
  )
  rpDcropfertilizers: RpDcropfertilizer[];
}
