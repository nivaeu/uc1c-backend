/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, OneToMany } from 'typeorm';
import { CoPfertilizer } from './co_pfertilizer.entity';
import { RpDcropfertilizer } from './rp_dcropfertilizer.entity';

@Index('pk_co_pfertilizertype', ['fetId'], { unique: true })
@Entity('co_pfertilizertype', { schema: 'sc_niva' })
export class CoPfertilizertype {
  @Column('smallint', { primary: true, name: 'fet_id' })
  fetId: number;

  @Column('character varying', { name: 'fet_name', length: 255 })
  fetName: string;

  @Column('date', { name: 'fet_expirydate', nullable: true })
  fetExpirydate: string | null;

  @OneToMany(() => CoPfertilizer, (coPfertilizer) => coPfertilizer.fet)
  coPfertilizers: CoPfertilizer[];

  @OneToMany(
    () => RpDcropfertilizer,
    (rpDcropfertilizer) => rpDcropfertilizer.fet,
  )
  rpDcropfertilizers: RpDcropfertilizer[];
}
