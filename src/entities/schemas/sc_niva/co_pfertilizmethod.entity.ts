/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, OneToMany } from 'typeorm';
import { RpDcropfertilizer } from './rp_dcropfertilizer.entity';

@Index('pk_co_pfertilizmethod', ['femId'], { unique: true })
@Entity('co_pfertilizmethod', { schema: 'sc_niva' })
export class CoPfertilizmethod {
  @Column('smallint', { primary: true, name: 'fem_id' })
  femId: number;

  @Column('character varying', { name: 'fem_name', length: 255 })
  femName: string;

  @Column('date', { name: 'fem_expirydate', nullable: true })
  femExpirydate: string | null;

  @OneToMany(
    () => RpDcropfertilizer,
    (rpDcropfertilizer) => rpDcropfertilizer.fem,
  )
  rpDcropfertilizers: RpDcropfertilizer[];
}
