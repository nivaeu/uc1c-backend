/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, OneToMany } from 'typeorm';
import { RpDcropfertilizer } from './rp_dcropfertilizer.entity';

@Index('pk_co_pfertiliztype', ['frtId'], { unique: true })
@Entity('co_pfertiliztype', { schema: 'sc_niva' })
export class CoPfertiliztype {
  @Column('smallint', { primary: true, name: 'frt_id' })
  frtId: number;

  @Column('character varying', { name: 'frt_name', length: 255 })
  frtName: string;

  @Column('date', { name: 'frt_expirydate', nullable: true })
  frtExpirydate: string | null;

  @OneToMany(
    () => RpDcropfertilizer,
    (rpDcropfertilizer) => rpDcropfertilizer.frt,
  )
  rpDcropfertilizers: RpDcropfertilizer[];
}
