/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, OneToMany } from 'typeorm';
import { RpDcropirrigation } from './rp_dcropirrigation.entity';

@Index('pk_co_pirrigationtype', ['irrId'], { unique: true })
@Entity('co_pirrigationtype', { schema: 'sc_niva' })
export class CoPirrigationtype {
  @Column('smallint', { primary: true, name: 'irr_id' })
  irrId: number;

  @Column('character varying', { name: 'irr_name', length: 255 })
  irrName: string;

  @Column('date', { name: 'irr_expirydate', nullable: true })
  irrExpirydate: string | null;

  @OneToMany(
    () => RpDcropirrigation,
    (rpDcropirrigation) => rpDcropirrigation.irr,
  )
  rpDcropirrigations: RpDcropirrigation[];
}
