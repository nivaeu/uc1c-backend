/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, OneToMany } from 'typeorm';
import { RpDcropactivitydet } from './rp_dcropactivitydet.entity';

@Index('pk_co_plabortype', ['labId'], { unique: true })
@Entity('co_plabortype', { schema: 'sc_niva' })
export class CoPlabortype {
  @Column('smallint', { primary: true, name: 'lab_id' })
  labId: number;

  @Column('character varying', { name: 'lab_name', length: 255 })
  labName: string;

  @Column('date', { name: 'lab_expirydate', nullable: true })
  labExpirydate: string | null;

  @OneToMany(
    () => RpDcropactivitydet,
    (rpDcropactivitydet) => rpDcropactivitydet.lab,
  )
  rpDcropactivitydets: RpDcropactivitydet[];
}
