/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { BaseEntity, Column, Entity, Index, OneToMany } from 'typeorm';
import { AbDlandcover } from './ab_dlandcover.entity';

@Index('pk_co_plandcover', ['lanId'], { unique: true })
@Entity('co_plandcover', { schema: 'sc_niva' })
export class CoPlandcover extends BaseEntity {
  @Column('smallint', { primary: true, name: 'lan_id' })
  lanId: number;

  @Column('character varying', { name: 'lan_name', length: 255 })
  lanName: string;

  @Column('date', { name: 'lan_expirydate', nullable: true })
  lanExpirydate: string | null;

  @OneToMany(() => AbDlandcover, (abDlandcover) => abDlandcover.lan)
  abDlandcovers: AbDlandcover[];
}
