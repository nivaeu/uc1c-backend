/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, OneToMany } from 'typeorm';
import { RpDcropparcel } from './rp_dcropparcel.entity';

@Index('pk_co_plandtenure', ['lteId'], { unique: true })
@Entity('co_plandtenure', { schema: 'sc_niva' })
export class CoPlandtenure {
  @Column('smallint', { primary: true, name: 'lte_id' })
  lteId: number;

  @Column('character varying', { name: 'lte_name', length: 255 })
  lteName: string;

  @Column('date', { name: 'lte_expirydate', nullable: true })
  lteExpirydate: string | null;

  @OneToMany(() => RpDcropparcel, (rpDcropparcel) => rpDcropparcel.lte)
  rpDcropparcels: RpDcropparcel[];
}
