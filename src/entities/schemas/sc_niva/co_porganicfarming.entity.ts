/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, OneToMany } from 'typeorm';
import { RpDcropactivity } from './rp_dcropactivity.entity';

@Index('pk_co_porganicfarming', ['orfId'], { unique: true })
@Entity('co_porganicfarming', { schema: 'sc_niva' })
export class CoPorganicfarming {
  @Column('smallint', { primary: true, name: 'orf_id' })
  orfId: number;

  @Column('character varying', { name: 'orf_name', length: 255 })
  orfName: string;

  @Column('date', { name: 'orf_expirydate', nullable: true })
  orfExpirydate: string | null;

  @OneToMany(() => RpDcropactivity, (rpDcropactivity) => rpDcropactivity.orf)
  rpDcropactivities: RpDcropactivity[];
}
