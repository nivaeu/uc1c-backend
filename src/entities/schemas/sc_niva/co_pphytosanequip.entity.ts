/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, OneToMany } from 'typeorm';
import { RpDcropphytosanit } from './rp_dcropphytosanit.entity';

@Index('pk_co_pphytosanequip', ['pheId'], { unique: true })
@Entity('co_pphytosanequip', { schema: 'sc_niva' })
export class CoPphytosanequip {
  @Column('smallint', { primary: true, name: 'phe_id' })
  pheId: number;

  @Column('character varying', { name: 'phe_name', length: 255 })
  pheName: string;

  @Column('date', { name: 'phe_expirydate', nullable: true })
  pheExpirydate: string | null;

  @OneToMany(
    () => RpDcropphytosanit,
    (rpDcropphytosanit) => rpDcropphytosanit.phe,
  )
  rpDcropphytosanits: RpDcropphytosanit[];
}
