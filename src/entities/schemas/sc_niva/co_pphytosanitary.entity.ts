/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { CoPcountry } from './co_pcountry.entity';
import { RpDcropphytosanit } from './rp_dcropphytosanit.entity';

@Index('pk_co_pphytosanitary', ['phyId'], { unique: true })
@Entity('co_pphytosanitary', { schema: 'sc_niva' })
export class CoPphytosanitary {
  @Column('character varying', { primary: true, name: 'phy_id', length: 8 })
  phyId: string;

  @Column('character varying', { name: 'phy_name', length: 255 })
  phyName: string;

  @Column('character varying', {
    name: 'phy_formulation',
    nullable: true,
    length: 255,
  })
  phyFormulation: string | null;

  @Column('date', { name: 'phy_expirydate', nullable: true })
  phyExpirydate: string | null;

  @ManyToOne(() => CoPcountry, (coPcountry) => coPcountry.coPphytosanitaries)
  @JoinColumn([{ name: 'cou_id', referencedColumnName: 'couId' }])
  cou: CoPcountry;

  @OneToMany(
    () => RpDcropphytosanit,
    (rpDcropphytosanit) => rpDcropphytosanit.phy,
  )
  rpDcropphytosanits: RpDcropphytosanit[];
}
