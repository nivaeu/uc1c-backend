/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, OneToMany } from 'typeorm';
import { RpDcropactivity } from './rp_dcropactivity.entity';

@Index('pk_co_pseedtype', ['seeId'], { unique: true })
@Entity('co_pseedtype', { schema: 'sc_niva' })
export class CoPseedtype {
  @Column('smallint', { primary: true, name: 'see_id' })
  seeId: number;

  @Column('character varying', { name: 'see_name', length: 255 })
  seeName: string;

  @Column('date', { name: 'see_expirydate', nullable: true })
  seeExpirydate: string | null;

  @OneToMany(() => RpDcropactivity, (rpDcropactivity) => rpDcropactivity.see)
  rpDcropactivities: RpDcropactivity[];
}
