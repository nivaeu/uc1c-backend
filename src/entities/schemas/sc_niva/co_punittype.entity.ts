/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, OneToMany } from 'typeorm';
import { RpDcropfertilizer } from './rp_dcropfertilizer.entity';
import { RpDcropirrigation } from './rp_dcropirrigation.entity';
import { RpDcropphytosanit } from './rp_dcropphytosanit.entity';
import { RpDirrigationrec } from './rp_dirrigationrec.entity';

@Index('pk_co_punittype', ['uniId'], { unique: true })
@Entity('co_punittype', { schema: 'sc_niva' })
export class CoPunittype {
  @Column('smallint', { primary: true, name: 'uni_id' })
  uniId: number;

  @Column('character varying', { name: 'uni_code', length: 5 })
  uniCode: string;

  @Column('character varying', { name: 'uni_name', length: 255 })
  uniName: string;

  @Column('date', { name: 'uni_expirydate', nullable: true })
  uniExpirydate: string | null;

  @OneToMany(
    () => RpDcropfertilizer,
    (rpDcropfertilizer) => rpDcropfertilizer.uni,
  )
  rpDcropfertilizers: RpDcropfertilizer[];

  @OneToMany(
    () => RpDcropirrigation,
    (rpDcropirrigation) => rpDcropirrigation.uni,
  )
  rpDcropirrigations: RpDcropirrigation[];

  @OneToMany(
    () => RpDcropphytosanit,
    (rpDcropphytosanit) => rpDcropphytosanit.uni,
  )
  rpDcropphytosanits: RpDcropphytosanit[];

  @OneToMany(() => RpDirrigationrec, (rpDirrigationrec) => rpDirrigationrec.uni)
  rpDirrigationrecs: RpDirrigationrec[];
}
