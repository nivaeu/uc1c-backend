/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, OneToMany } from 'typeorm';
import { RpDcropirrigation } from './rp_dcropirrigation.entity';

@Index('pk_co_pwaterorigin', ['waoId'], { unique: true })
@Entity('co_pwaterorigin', { schema: 'sc_niva' })
export class CoPwaterorigin {
  @Column('smallint', { primary: true, name: 'wao_id' })
  waoId: number;

  @Column('character varying', { name: 'wao_name', length: 255 })
  waoName: string;

  @Column('date', { name: 'wao_expirydate', nullable: true })
  waoExpirydate: string | null;

  @OneToMany(
    () => RpDcropirrigation,
    (rpDcropirrigation) => rpDcropirrigation.wao,
  )
  rpDcropirrigations: RpDcropirrigation[];
}
