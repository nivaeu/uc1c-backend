/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, OneToMany } from 'typeorm';
import { PrDprodworkunit } from './pr_dprodworkunit.entity';

@Index('pk_co_pworkunittype', ['wutId'], { unique: true })
@Entity('co_pworkunittype', { schema: 'sc_niva' })
export class CoPworkunittype {
  @Column('smallint', { primary: true, name: 'wut_id' })
  wutId: number;

  @Column('character varying', { name: 'wut_name', length: 255 })
  wutName: string;

  @Column('date', { name: 'wut_expirydate', nullable: true })
  wutExpirydate: string | null;

  @OneToMany(() => PrDprodworkunit, (prDprodworkunit) => prDprodworkunit.wut)
  prDprodworkunits: PrDprodworkunit[];
}
