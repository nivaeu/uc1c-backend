/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index } from 'typeorm';

@Index('pk_co_rcou_crp', ['ccpCouproduct', 'couId', 'crpId'], { unique: true })
@Entity('co_rcou_crp', { schema: 'sc_niva' })
export class CoRcouCrp {
  @Column('character varying', { primary: true, name: 'crp_id', length: 6 })
  crpId: string;

  @Column('character varying', { primary: true, name: 'cou_id', length: 2 })
  couId: string;

  @Column('character varying', {
    primary: true,
    name: 'ccp_couproduct',
    length: 5,
  })
  ccpCouproduct: string;
}
