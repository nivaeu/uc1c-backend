/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index } from 'typeorm';

@Index('pk_ecr_dreportdoc', ['ecrId'], { unique: true })
@Entity('ecr_dreportdoc', { schema: 'sc_niva' })
export class EcrDreportdoc {
  @Column('integer', { primary: true, name: 'ecr_id' })
  ecrId: number;

  @Column('smallint', { name: 'ecr_reportcount', nullable: true })
  ecrReportcount: number | null;

  @Column('character varying', {
    name: 'ecr_description',
    nullable: true,
    length: 255,
  })
  ecrDescription: string | null;

  @Column('timestamp with time zone', { name: 'ecr_issue' })
  ecrIssue: Date;

  @Column('smallint', { name: 'ecr_type', nullable: true })
  ecrType: number | null;

  @Column('boolean', { name: 'ecr_copy', nullable: true })
  ecrCopy: boolean | null;

  @Column('boolean', { name: 'ecr_ctrlrequirement', nullable: true })
  ecrCtrlrequirement: boolean | null;

  @Column('smallint', { name: 'ecr_purpose', nullable: true })
  ecrPurpose: number | null;

  @Column('smallint', { name: 'ecr_linecount', nullable: true })
  ecrLinecount: number | null;

  @Column('character varying', {
    name: 'ecr_information',
    nullable: true,
    length: 255,
  })
  ecrInformation: string | null;

  @Column('smallint', { name: 'ecr_status', nullable: true })
  ecrStatus: number | null;

  @Column('smallint', { name: 'ecr_sequence', nullable: true })
  ecrSequence: number | null;
}
