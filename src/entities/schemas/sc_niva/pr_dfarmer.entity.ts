/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
} from 'typeorm';
import { CoPcountry } from './co_pcountry.entity';
import { PrDfarmerdata } from './pr_dfarmerdata.entity';
import { PrDproductionunit } from './pr_dproductionunit.entity';
import { FarDpayment } from '../sc_niva_extra/far_dpayment.entity';
import { FarDanimal } from '../sc_niva_extra/far_danimal.entity';

@Index('ux_pr_dfarmer_code', ['farCode'], { unique: true })
@Index('pk_pr_dfarmer', ['farId'], { unique: true })
@Entity('pr_dfarmer', { schema: 'sc_niva' })
export class PrDfarmer {
  @Column('uuid', { primary: true, name: 'far_id' })
  farId: string;

  @Column('character varying', { name: 'far_code', unique: true, length: 14 })
  farCode: string;

  @Column('uuid', { name: 'fac_id', nullable: true })
  facId: string | null;

  @ManyToOne(() => CoPcountry, (coPcountry) => coPcountry.prDfarmers)
  @JoinColumn([{ name: 'cou_id', referencedColumnName: 'couId' }])
  cou: CoPcountry;

  @OneToOne(() => PrDfarmerdata, (prDfarmerdata) => prDfarmerdata.far)
  prDfarmerdata: PrDfarmerdata;

  @OneToMany(
    () => PrDproductionunit,
    (prDproductionunit) => prDproductionunit.farId,
  )
  prDproductionunits: PrDproductionunit[];

  @OneToMany(() => FarDpayment, (farDpayment) => farDpayment.farId)
  farDpayments: FarDpayment[];

  @OneToMany(() => FarDanimal, (farDanimal) => farDanimal.farId)
  farDanimals: FarDanimal[];
}
