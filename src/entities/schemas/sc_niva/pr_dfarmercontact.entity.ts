/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index } from 'typeorm';

@Index('pk_pr_dfarmercontact', ['facId'], { unique: true })
@Entity('pr_dfarmercontact', { schema: 'sc_niva' })
export class PrDfarmercontact {
  @Column('uuid', { primary: true, name: 'fac_id' })
  facId: string;

  @Column('character varying', { name: 'cou_id', nullable: true, length: 2 })
  couId: string | null;

  @Column('character varying', {
    name: 'fac_city',
    nullable: true,
    length: 255,
  })
  facCity: string | null;

  @Column('character varying', {
    name: 'fac_street',
    nullable: true,
    length: 255,
  })
  facStreet: string | null;

  @Column('character varying', {
    name: 'fac_number',
    nullable: true,
    length: 255,
  })
  facNumber: string | null;

  @Column('character varying', {
    name: 'fac_postcode',
    nullable: true,
    length: 255,
  })
  facPostcode: string | null;

  @Column('character varying', {
    name: 'fac_additional',
    nullable: true,
    length: 255,
  })
  facAdditional: string | null;

  @Column('character varying', {
    name: 'fac_email1',
    nullable: true,
    length: 255,
  })
  facEmail1: string | null;

  @Column('character varying', {
    name: 'fac_email2',
    nullable: true,
    length: 255,
  })
  facEmail2: string | null;

  @Column('character varying', {
    name: 'fac_phone1',
    nullable: true,
    length: 255,
  })
  facPhone1: string | null;

  @Column('character varying', {
    name: 'fac_phone2',
    nullable: true,
    length: 255,
  })
  facPhone2: string | null;
}
