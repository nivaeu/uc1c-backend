/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToOne,
} from 'typeorm';
import { PrDfarmer } from './pr_dfarmer.entity';
import { CoPfarmertype } from './co_pfarmertype.entity';

@Index('pk_pr_dfarmerdata', ['farId'], { unique: true })
@Entity('pr_dfarmerdata', { schema: 'sc_niva' })
export class PrDfarmerdata extends BaseEntity {
  @Column('uuid', { primary: true, name: 'far_id' })
  farId: string;

  @Column('character varying', { name: 'fad_personalid', length: 255 })
  fadPersonalId: string;

  @Column('character varying', {
    name: 'fad_name',
    nullable: true,
    length: 255,
  })
  fadName: string | null;

  @OneToOne(() => PrDfarmer, (prDfarmer) => prDfarmer.prDfarmerdata)
  @JoinColumn([{ name: 'far_id', referencedColumnName: 'farId' }])
  far: PrDfarmer;

  @ManyToOne(
    () => CoPfarmertype,
    (coPfarmertype) => coPfarmertype.prDfarmerdata,
  )
  @JoinColumn([{ name: 'fat_id', referencedColumnName: 'fatId' }])
  fatId: CoPfarmertype;
}
