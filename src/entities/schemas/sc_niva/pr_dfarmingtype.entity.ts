/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { CoPfarmingtype } from './co_pfarmingtype.entity';
import { PrDproductionunit } from './pr_dproductionunit.entity';

@Index('pk_pr_dfarmingtype', ['frtId', 'pftDatefrom', 'punId'], {
  unique: true,
})
@Entity('pr_dfarmingtype', { schema: 'sc_niva' })
export class PrDfarmingtype {
  @Column('uuid', { primary: true, name: 'pun_id' })
  punId: string;

  @Column('character varying', { primary: true, name: 'frt_id', length: 3 })
  frtId: string;

  @Column('date', { primary: true, name: 'pft_datefrom' })
  pftDatefrom: string;

  @Column('date', { name: 'pft_dateto', nullable: true })
  pftDateto: string | null;

  @ManyToOne(
    () => CoPfarmingtype,
    (coPfarmingtype) => coPfarmingtype.prDfarmingtypes,
  )
  @JoinColumn([{ name: 'frt_id', referencedColumnName: 'frtId' }])
  frt: CoPfarmingtype;

  @ManyToOne(
    () => PrDproductionunit,
    (prDproductionunit) => prDproductionunit.prDfarmingtypes,
  )
  @JoinColumn([{ name: 'pun_id', referencedColumnName: 'punId' }])
  pun: PrDproductionunit;
}
