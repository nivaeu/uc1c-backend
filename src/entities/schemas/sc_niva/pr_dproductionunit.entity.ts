/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  BaseEntity,
} from 'typeorm';
import { AbDagrblock } from './ab_dagrblock.entity';
import { PrDfarmingtype } from './pr_dfarmingtype.entity';
import { CoPcountry } from './co_pcountry.entity';
import { PrDfarmer } from './pr_dfarmer.entity';
import { PrDprodworkunit } from './pr_dprodworkunit.entity';
import { FarDpayment } from '../sc_niva_extra/far_dpayment.entity';
import { FarDanimal } from '../sc_niva_extra/far_danimal.entity';

@Index('ux_pr_dproductionunit_code', ['punCode'], { unique: true })
@Index('pk_pr_dproductionunit', ['punId'], { unique: true })
@Entity('pr_dproductionunit', { schema: 'sc_niva' })
export class PrDproductionunit extends BaseEntity {
  @Column('uuid', { primary: true, name: 'pun_id' })
  punId: string;

  @Column('character varying', { name: 'pun_code', unique: true, length: 14 })
  punCode: string;

  @Column('character varying', { name: 'pun_key', length: 50 })
  punKey: string;

  @Column('uuid', { name: 'fac_id', nullable: true })
  facId: string | null;

  @Column('date', { name: 'pun_datefrom', nullable: true })
  punDatefrom: string | null;

  @Column('date', { name: 'pun_dateto', nullable: true })
  punDateto: string | null;

  @OneToMany(() => AbDagrblock, (abDagrblock) => abDagrblock.punId)
  abDagrblocks: AbDagrblock[];

  @OneToMany(() => FarDpayment, (farDpayment) => farDpayment.punId)
  farDpayments: FarDpayment[];

  @OneToMany(() => FarDanimal, (farDanimal) => farDanimal.punId)
  farDanimals: FarDanimal[];

  @OneToMany(() => PrDfarmingtype, (prDfarmingtype) => prDfarmingtype.pun)
  prDfarmingtypes: PrDfarmingtype[];

  @ManyToOne(() => CoPcountry, (coPcountry) => coPcountry.prDproductionunits)
  @JoinColumn([{ name: 'cou_id', referencedColumnName: 'couId' }])
  cou: CoPcountry;

  @ManyToOne(() => PrDfarmer, (prDfarmer) => prDfarmer.prDproductionunits)
  @JoinColumn([{ name: 'far_id', referencedColumnName: 'farId' }])
  farId: PrDfarmer;

  @OneToMany(() => PrDprodworkunit, (prDprodworkunit) => prDprodworkunit.pun)
  prDprodworkunits: PrDprodworkunit[];
}
