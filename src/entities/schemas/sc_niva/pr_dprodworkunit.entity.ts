/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { PrDproductionunit } from './pr_dproductionunit.entity';
import { CoPworkunittype } from './co_pworkunittype.entity';

@Index('pk_pr_dprodworkunit', ['punId', 'puwDatefrom', 'wutId'], {
  unique: true,
})
@Entity('pr_dprodworkunit', { schema: 'sc_niva' })
export class PrDprodworkunit {
  @Column('uuid', { primary: true, name: 'pun_id' })
  punId: string;

  @Column('smallint', { primary: true, name: 'wut_id' })
  wutId: number;

  @Column('numeric', {
    name: 'puw_awu',
    nullable: true,
    precision: 8,
    scale: 2,
  })
  puwAwu: string | null;

  @Column('date', { primary: true, name: 'puw_datefrom' })
  puwDatefrom: string;

  @Column('date', { name: 'puw_dateto', nullable: true })
  puwDateto: string | null;

  @ManyToOne(
    () => PrDproductionunit,
    (prDproductionunit) => prDproductionunit.prDprodworkunits,
  )
  @JoinColumn([{ name: 'pun_id', referencedColumnName: 'punId' }])
  pun: PrDproductionunit;

  @ManyToOne(
    () => CoPworkunittype,
    (coPworkunittype) => coPworkunittype.prDprodworkunits,
  )
  @JoinColumn([{ name: 'wut_id', referencedColumnName: 'wutId' }])
  wut: CoPworkunittype;
}
