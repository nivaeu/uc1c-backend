/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { RevDrevinfo } from './rev_drevinfo.entity';

@Index('pk_pr_hfarmer', ['farId', 'rev'], { unique: true })
@Entity('pr_hfarmer', { schema: 'sc_niva' })
export class PrHfarmer {
  @Column('uuid', { primary: true, name: 'far_id' })
  farId: string;

  @Column('character varying', { name: 'far_code', length: 14 })
  farCode: string;

  @Column('uuid', { name: 'fac_id', nullable: true })
  facId: string | null;

  @Column('character varying', { name: 'cou_id', length: 2 })
  couId: string;

  @Column('integer', { primary: true, name: 'rev' })
  rev: number;

  @Column('smallint', { name: 'revtype', nullable: true })
  revtype: number | null;

  @ManyToOne(() => RevDrevinfo, (revDrevinfo) => revDrevinfo.prHfarmers)
  @JoinColumn([{ name: 'rev', referencedColumnName: 'rev' }])
  rev2: RevDrevinfo;
}
