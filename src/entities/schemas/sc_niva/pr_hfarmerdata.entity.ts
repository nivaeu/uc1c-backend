/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { RevDrevinfo } from './rev_drevinfo.entity';

@Index('pk_pr_hfarmerdata', ['farId', 'rev'], { unique: true })
@Entity('pr_hfarmerdata', { schema: 'sc_niva' })
export class PrHfarmerdata {
  @Column('uuid', { primary: true, name: 'far_id' })
  farId: string;

  @Column('smallint', { name: 'fat_id', nullable: true })
  fatId: number | null;

  @Column('character varying', { name: 'fad_personalid', length: 255 })
  fadPersonalId: string;

  @Column('character varying', {
    name: 'fad_name',
    nullable: true,
    length: 255,
  })
  fadName: string | null;

  @Column('integer', { primary: true, name: 'rev' })
  rev: number;

  @Column('smallint', { name: 'revtype', nullable: true })
  revtype: number | null;

  @ManyToOne(() => RevDrevinfo, (revDrevinfo) => revDrevinfo.prHfarmerdata)
  @JoinColumn([{ name: 'rev', referencedColumnName: 'rev' }])
  rev2: RevDrevinfo;
}
