/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { RevDrevinfo } from './rev_drevinfo.entity';

@Index('pk_pr_hfarmingtype', ['frtId', 'pftDatefrom', 'punId', 'rev'], {
  unique: true,
})
@Entity('pr_hfarmingtype', { schema: 'sc_niva' })
export class PrHfarmingtype {
  @Column('uuid', { primary: true, name: 'pun_id' })
  punId: string;

  @Column('character varying', { primary: true, name: 'frt_id', length: 3 })
  frtId: string;

  @Column('date', { primary: true, name: 'pft_datefrom' })
  pftDatefrom: string;

  @Column('date', { name: 'pft_dateto', nullable: true })
  pftDateto: string | null;

  @Column('integer', { primary: true, name: 'rev' })
  rev: number;

  @Column('smallint', { name: 'revtype', nullable: true })
  revtype: number | null;

  @ManyToOne(() => RevDrevinfo, (revDrevinfo) => revDrevinfo.prHfarmingtypes)
  @JoinColumn([{ name: 'rev', referencedColumnName: 'rev' }])
  rev2: RevDrevinfo;
}
