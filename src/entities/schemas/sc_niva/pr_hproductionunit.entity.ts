/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { RevDrevinfo } from './rev_drevinfo.entity';

@Index('pk_pr_hproductionunit', ['punId', 'rev'], { unique: true })
@Entity('pr_hproductionunit', { schema: 'sc_niva' })
export class PrHproductionunit {
  @Column('uuid', { primary: true, name: 'pun_id' })
  punId: string;

  @Column('character varying', { name: 'pun_code', length: 14 })
  punCode: string;

  @Column('character varying', { name: 'pun_key', length: 50 })
  punKey: string;

  @Column('uuid', { name: 'far_id' })
  farId: string;

  @Column('uuid', { name: 'fac_id', nullable: true })
  facId: string | null;

  @Column('character varying', { name: 'cou_id', length: 2 })
  couId: string;

  @Column('date', { name: 'pun_datefrom', nullable: true })
  punDatefrom: string | null;

  @Column('date', { name: 'pun_dateto', nullable: true })
  punDateto: string | null;

  @Column('integer', { primary: true, name: 'rev' })
  rev: number;

  @Column('smallint', { name: 'revtype', nullable: true })
  revtype: number | null;

  @ManyToOne(() => RevDrevinfo, (revDrevinfo) => revDrevinfo.prHproductionunits)
  @JoinColumn([{ name: 'rev', referencedColumnName: 'rev' }])
  rev2: RevDrevinfo;
}
