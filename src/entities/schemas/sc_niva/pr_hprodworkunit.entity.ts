/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { RevDrevinfo } from './rev_drevinfo.entity';

@Index('pk_pr_hprodworkunit', ['punId', 'puwDatefrom', 'rev', 'wutId'], {
  unique: true,
})
@Entity('pr_hprodworkunit', { schema: 'sc_niva' })
export class PrHprodworkunit {
  @Column('uuid', { primary: true, name: 'pun_id' })
  punId: string;

  @Column('smallint', { primary: true, name: 'wut_id' })
  wutId: number;

  @Column('numeric', {
    name: 'puw_awu',
    nullable: true,
    precision: 8,
    scale: 2,
  })
  puwAwu: string | null;

  @Column('date', { primary: true, name: 'puw_datefrom' })
  puwDatefrom: string;

  @Column('date', { name: 'puw_dateto', nullable: true })
  puwDateto: string | null;

  @Column('integer', { primary: true, name: 'rev' })
  rev: number;

  @Column('smallint', { name: 'revtype', nullable: true })
  revtype: number | null;

  @ManyToOne(() => RevDrevinfo, (revDrevinfo) => revDrevinfo.prHprodworkunits)
  @JoinColumn([{ name: 'rev', referencedColumnName: 'rev' }])
  rev2: RevDrevinfo;
}
