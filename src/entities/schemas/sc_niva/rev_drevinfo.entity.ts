/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, OneToMany } from 'typeorm';
import { AbHagrblock } from './ab_hagrblock.entity';
import { AbHgeometry } from './ab_hgeometry.entity';
import { AbHlandcover } from './ab_hlandcover.entity';
import { PrHfarmer } from './pr_hfarmer.entity';
import { PrHfarmercontact } from './pr_hfarmercontact.entity';
import { PrHfarmerdata } from './pr_hfarmerdata.entity';
import { PrHfarmingtype } from './pr_hfarmingtype.entity';
import { PrHproductionunit } from './pr_hproductionunit.entity';
import { PrHprodworkunit } from './pr_hprodworkunit.entity';
import { RpHcropactivity } from './rp_hcropactivity.entity';
import { RpHcropactivitydet } from './rp_hcropactivitydet.entity';
import { RpHcropfertilizer } from './rp_hcropfertilizer.entity';
import { RpHcropirrigation } from './rp_hcropirrigation.entity';
import { RpHcropparcel } from './rp_hcropparcel.entity';
import { RpHcropparcelgeom } from './rp_hcropparcelgeom.entity';
import { RpHcropphytosanit } from './rp_hcropphytosanit.entity';
import { RpHgeometry } from './rp_hgeometry.entity';
import { RpHrefplot } from './rp_hrefplot.entity';

@Index('pk_rev_drevinfo', ['rev'], { unique: true })
@Entity('rev_drevinfo', { schema: 'sc_niva' })
export class RevDrevinfo {
  @Column('integer', { primary: true, name: 'rev' })
  rev: number;

  @Column('bigint', { name: 'rev_tstmp' })
  revTstmp: string;

  @Column('smallint', { name: 'rev_user' })
  revUser: number;

  @OneToMany(() => AbHagrblock, (abHagrblock) => abHagrblock.rev2)
  abHagrblocks: AbHagrblock[];

  @OneToMany(() => AbHgeometry, (abHgeometry) => abHgeometry.rev2)
  abHgeometries: AbHgeometry[];

  @OneToMany(() => AbHlandcover, (abHlandcover) => abHlandcover.rev2)
  abHlandcovers: AbHlandcover[];

  @OneToMany(() => PrHfarmer, (prHfarmer) => prHfarmer.rev2)
  prHfarmers: PrHfarmer[];

  @OneToMany(
    () => PrHfarmercontact,
    (prHfarmercontact) => prHfarmercontact.rev2,
  )
  prHfarmercontacts: PrHfarmercontact[];

  @OneToMany(() => PrHfarmerdata, (prHfarmerdata) => prHfarmerdata.rev2)
  prHfarmerdata: PrHfarmerdata[];

  @OneToMany(() => PrHfarmingtype, (prHfarmingtype) => prHfarmingtype.rev2)
  prHfarmingtypes: PrHfarmingtype[];

  @OneToMany(
    () => PrHproductionunit,
    (prHproductionunit) => prHproductionunit.rev2,
  )
  prHproductionunits: PrHproductionunit[];

  @OneToMany(() => PrHprodworkunit, (prHprodworkunit) => prHprodworkunit.rev2)
  prHprodworkunits: PrHprodworkunit[];

  @OneToMany(() => RpHcropactivity, (rpHcropactivity) => rpHcropactivity.rev2)
  rpHcropactivities: RpHcropactivity[];

  @OneToMany(
    () => RpHcropactivitydet,
    (rpHcropactivitydet) => rpHcropactivitydet.rev2,
  )
  rpHcropactivitydets: RpHcropactivitydet[];

  @OneToMany(
    () => RpHcropfertilizer,
    (rpHcropfertilizer) => rpHcropfertilizer.rev2,
  )
  rpHcropfertilizers: RpHcropfertilizer[];

  @OneToMany(
    () => RpHcropirrigation,
    (rpHcropirrigation) => rpHcropirrigation.rev2,
  )
  rpHcropirrigations: RpHcropirrigation[];

  @OneToMany(() => RpHcropparcel, (rpHcropparcel) => rpHcropparcel.rev2)
  rpHcropparcels: RpHcropparcel[];

  @OneToMany(
    () => RpHcropparcelgeom,
    (rpHcropparcelgeom) => rpHcropparcelgeom.rev2,
  )
  rpHcropparcelgeoms: RpHcropparcelgeom[];

  @OneToMany(
    () => RpHcropphytosanit,
    (rpHcropphytosanit) => rpHcropphytosanit.rev2,
  )
  rpHcropphytosanits: RpHcropphytosanit[];

  @OneToMany(() => RpHgeometry, (rpHgeometry) => rpHgeometry.rev2)
  rpHgeometries: RpHgeometry[];

  @OneToMany(() => RpHrefplot, (rpHrefplot) => rpHrefplot.rev2)
  rpHrefplots: RpHrefplot[];
}
