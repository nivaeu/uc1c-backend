/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToOne,
} from 'typeorm';
import { CoPcultivationdet } from './co_pcultivationdet.entity';
import { CoPcultivationsys } from './co_pcultivationsys.entity';
import { CoPorganicfarming } from './co_porganicfarming.entity';
import { RpDcropparcel } from './rp_dcropparcel.entity';
import { CoPseedtype } from './co_pseedtype.entity';

@Index('pk_rp_dcropactivity', ['rcpId'], { unique: true })
@Entity('rp_dcropactivity', { schema: 'sc_niva' })
export class RpDcropactivity {
  @Column('uuid', { primary: true, name: 'rcp_id' })
  rcpId: string;

  @Column('boolean', { name: 'aga_agriactivity', nullable: true })
  agaAgriactivity: boolean | null;

  @ManyToOne(
    () => CoPcultivationdet,
    (coPcultivationdet) => coPcultivationdet.rpDcropactivities,
  )
  @JoinColumn([{ name: 'cud_id', referencedColumnName: 'cudId' }])
  cud: CoPcultivationdet;

  @ManyToOne(
    () => CoPcultivationsys,
    (coPcultivationsys) => coPcultivationsys.rpDcropactivities,
  )
  @JoinColumn([{ name: 'cus_id', referencedColumnName: 'cusId' }])
  cus: CoPcultivationsys;

  @ManyToOne(
    () => CoPorganicfarming,
    (coPorganicfarming) => coPorganicfarming.rpDcropactivities,
  )
  @JoinColumn([{ name: 'orf_id', referencedColumnName: 'orfId' }])
  orf: CoPorganicfarming;

  @OneToOne(
    () => RpDcropparcel,
    (rpDcropparcel) => rpDcropparcel.rpDcropactivity,
  )
  @JoinColumn([{ name: 'rcp_id', referencedColumnName: 'rcpId' }])
  rcp: RpDcropparcel;

  @ManyToOne(() => CoPseedtype, (coPseedtype) => coPseedtype.rpDcropactivities)
  @JoinColumn([{ name: 'see_id', referencedColumnName: 'seeId' }])
  see: CoPseedtype;
}
