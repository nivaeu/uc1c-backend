/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { CoPlabortype } from './co_plabortype.entity';
import { RpDcropparcel } from './rp_dcropparcel.entity';

@Index('pk_rp_dcropactivitydet', ['cadStartdate', 'labId', 'rcpId'], {
  unique: true,
})
@Entity('rp_dcropactivitydet', { schema: 'sc_niva' })
export class RpDcropactivitydet {
  @Column('uuid', { primary: true, name: 'rcp_id' })
  rcpId: string;

  @Column('smallint', { primary: true, name: 'lab_id' })
  labId: number;

  @Column('date', { primary: true, name: 'cad_startdate' })
  cadStartdate: string;

  @Column('date', { name: 'cad_enddate', nullable: true })
  cadEnddate: string | null;

  @ManyToOne(
    () => CoPlabortype,
    (coPlabortype) => coPlabortype.rpDcropactivitydets,
  )
  @JoinColumn([{ name: 'lab_id', referencedColumnName: 'labId' }])
  lab: CoPlabortype;

  @ManyToOne(
    () => RpDcropparcel,
    (rpDcropparcel) => rpDcropparcel.rpDcropactivitydets,
  )
  @JoinColumn([{ name: 'rcp_id', referencedColumnName: 'rcpId' }])
  rcp: RpDcropparcel;
}
