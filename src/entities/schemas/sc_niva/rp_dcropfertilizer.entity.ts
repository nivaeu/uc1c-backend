/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { CoPfertilizmethod } from './co_pfertilizmethod.entity';
import { CoPfertilizer } from './co_pfertilizer.entity';
import { CoPfertilizertype } from './co_pfertilizertype.entity';
import { CoPfertiliztype } from './co_pfertiliztype.entity';
import { RpDcropparcel } from './rp_dcropparcel.entity';
import { CoPunittype } from './co_punittype.entity';

@Index('pk_rp_dcropfertilizer', ['cfeStartdate', 'fetId', 'rcpId'], {
  unique: true,
})
@Entity('rp_dcropfertilizer', { schema: 'sc_niva' })
export class RpDcropfertilizer {
  @Column('uuid', { primary: true, name: 'rcp_id' })
  rcpId: string;

  @Column('smallint', { primary: true, name: 'fet_id' })
  fetId: number;

  @Column('numeric', {
    name: 'cfe_dose',
    nullable: true,
    precision: 8,
    scale: 2,
  })
  cfeDose: string | null;

  @Column('date', { primary: true, name: 'cfe_startdate' })
  cfeStartdate: string;

  @Column('date', { name: 'cfe_enddate', nullable: true })
  cfeEnddate: string | null;

  @Column('date', { name: 'cfe_expirydate', nullable: true })
  cfeExpirydate: string | null;

  @ManyToOne(
    () => CoPfertilizmethod,
    (coPfertilizmethod) => coPfertilizmethod.rpDcropfertilizers,
  )
  @JoinColumn([{ name: 'fem_id', referencedColumnName: 'femId' }])
  fem: CoPfertilizmethod;

  @ManyToOne(
    () => CoPfertilizer,
    (coPfertilizer) => coPfertilizer.rpDcropfertilizers,
  )
  @JoinColumn([{ name: 'fer_id', referencedColumnName: 'ferId' }])
  fer: CoPfertilizer;

  @Column({ name: 'fer_id' })
  ferId: string;

  @ManyToOne(
    () => CoPfertilizertype,
    (coPfertilizertype) => coPfertilizertype.rpDcropfertilizers,
  )
  @JoinColumn([{ name: 'fet_id', referencedColumnName: 'fetId' }])
  fet: CoPfertilizertype;

  @ManyToOne(
    () => CoPfertiliztype,
    (coPfertiliztype) => coPfertiliztype.rpDcropfertilizers,
  )
  @JoinColumn([{ name: 'frt_id', referencedColumnName: 'frtId' }])
  frt: CoPfertiliztype;

  @ManyToOne(
    () => RpDcropparcel,
    (rpDcropparcel) => rpDcropparcel.rpDcropfertilizers,
  )
  @JoinColumn([{ name: 'rcp_id', referencedColumnName: 'rcpId' }])
  rcp: RpDcropparcel;

  @ManyToOne(() => CoPunittype, (coPunittype) => coPunittype.rpDcropfertilizers)
  @JoinColumn([{ name: 'uni_id', referencedColumnName: 'uniId' }])
  uni: CoPunittype;

  @Column('smallint', { name: 'uni_id' })
  uniId: number;
}
