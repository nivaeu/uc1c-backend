/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { CoPirrigationtype } from './co_pirrigationtype.entity';
import { RpDcropparcel } from './rp_dcropparcel.entity';
import { CoPunittype } from './co_punittype.entity';
import { CoPwaterorigin } from './co_pwaterorigin.entity';

@Index('pk_rp_dcropirrigation', ['criIrrigationdate', 'rcpId'], {
  unique: true,
})
@Entity('rp_dcropirrigation', { schema: 'sc_niva' })
export class RpDcropirrigation {
  @Column('uuid', { primary: true, name: 'rcp_id' })
  rcpId: string;

  @Column('numeric', {
    name: 'cri_volume',
    nullable: true,
    precision: 8,
    scale: 2,
  })
  criVolume: string | null;

  @Column('date', { primary: true, name: 'cri_irrigationdate' })
  criIrrigationdate: string;

  @ManyToOne(
    () => CoPirrigationtype,
    (coPirrigationtype) => coPirrigationtype.rpDcropirrigations,
  )
  @JoinColumn([{ name: 'irr_id', referencedColumnName: 'irrId' }])
  irr: CoPirrigationtype;

  @ManyToOne(
    () => RpDcropparcel,
    (rpDcropparcel) => rpDcropparcel.rpDcropirrigations,
  )
  @JoinColumn([{ name: 'rcp_id', referencedColumnName: 'rcpId' }])
  rcp: RpDcropparcel;

  @ManyToOne(() => CoPunittype, (coPunittype) => coPunittype.rpDcropirrigations)
  @JoinColumn([{ name: 'uni_id', referencedColumnName: 'uniId' }])
  uni: CoPunittype;

  @ManyToOne(
    () => CoPwaterorigin,
    (coPwaterorigin) => coPwaterorigin.rpDcropirrigations,
  )
  @JoinColumn([{ name: 'wao_id', referencedColumnName: 'waoId' }])
  wao: CoPwaterorigin;
}
