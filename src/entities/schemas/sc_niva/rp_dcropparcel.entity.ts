/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
} from 'typeorm';
import { RpDcropactivity } from './rp_dcropactivity.entity';
import { RpDcropactivitydet } from './rp_dcropactivitydet.entity';
import { RpDcropfertilizer } from './rp_dcropfertilizer.entity';
import { RpDcropirrigation } from './rp_dcropirrigation.entity';
import { CoPcropproduct } from './co_pcropproduct.entity';
import { CoPlandtenure } from './co_plandtenure.entity';
import { RpDrefplot } from './rp_drefplot.entity';
import { RpDcropparcelgeom } from './rp_dcropparcelgeom.entity';
import { RpDcropphytosanit } from './rp_dcropphytosanit.entity';
import { RpDfertilizrec } from './rp_dfertilizrec.entity';
import { RpDirrigationrec } from './rp_dirrigationrec.entity';

@Index('pk_rp_dcropparcel', ['rcpId'], { unique: true })
@Entity('rp_dcropparcel', { schema: 'sc_niva' })
export class RpDcropparcel extends BaseEntity {
  @Column('uuid', { primary: true, name: 'rcp_id' })
  rcpId: string;

  @Column('character varying', { name: 'rcp_key', length: 50 })
  rcpKey: string;

  @Column('boolean', { name: 'rcp_irrigation' })
  rcpIrrigation: boolean;

  @Column('character varying', { name: 'crv_id', nullable: true, length: 5 })
  crvId: string | null;

  @Column('boolean', { name: 'rcp_landscfeatures', nullable: true })
  rcpLandscfeatures: boolean | null;

  @Column('date', { name: 'rcp_datefrom', nullable: true })
  rcpDatefrom: string | null;

  @Column('date', { name: 'rcp_dateto', nullable: true })
  rcpDateto: string | null;

  @OneToOne(() => RpDcropactivity, (rpDcropactivity) => rpDcropactivity.rcp)
  rpDcropactivity: RpDcropactivity;

  @OneToMany(
    () => RpDcropactivitydet,
    (rpDcropactivitydet) => rpDcropactivitydet.rcp,
  )
  rpDcropactivitydets: RpDcropactivitydet[];

  @OneToMany(
    () => RpDcropfertilizer,
    (rpDcropfertilizer) => rpDcropfertilizer.rcp,
  )
  rpDcropfertilizers: RpDcropfertilizer[];

  @OneToMany(
    () => RpDcropirrigation,
    (rpDcropirrigation) => rpDcropirrigation.rcp,
  )
  rpDcropirrigations: RpDcropirrigation[];

  @ManyToOne(
    () => CoPcropproduct,
    (coPcropproduct) => coPcropproduct.rpDcropparcels,
  )
  @JoinColumn([{ name: 'crp_id', referencedColumnName: 'crpId' }])
  crp: CoPcropproduct;

  @ManyToOne(
    () => CoPlandtenure,
    (coPlandtenure) => coPlandtenure.rpDcropparcels,
  )
  @JoinColumn([{ name: 'lte_id', referencedColumnName: 'lteId' }])
  lte: CoPlandtenure;

  @ManyToOne(() => RpDrefplot, (rpDrefplot) => rpDrefplot.rpDcropparcels)
  @JoinColumn([{ name: 'rfp_id', referencedColumnName: 'rfpId' }])
  rfp: RpDrefplot;

  @OneToMany(
    () => RpDcropparcelgeom,
    (rpDcropparcelgeom) => rpDcropparcelgeom.rcp,
  )
  rpDcropparcelgeoms: RpDcropparcelgeom[];

  @OneToMany(
    () => RpDcropphytosanit,
    (rpDcropphytosanit) => rpDcropphytosanit.rcp,
  )
  rpDcropphytosanits: RpDcropphytosanit[];

  @OneToMany(() => RpDfertilizrec, (rpDfertilizrec) => rpDfertilizrec.rcp)
  rpDfertilizrecs: RpDfertilizrec[];

  @OneToMany(() => RpDirrigationrec, (rpDirrigationrec) => rpDirrigationrec.rcp)
  rpDirrigationrecs: RpDirrigationrec[];
}
