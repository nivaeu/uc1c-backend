/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { RpDcropparcel } from './rp_dcropparcel.entity';

@Index('pk_rp_dcropparcelgeom', ['cpgDatefrom', 'rcpId'], { unique: true })
@Entity('rp_dcropparcelgeom', { schema: 'sc_niva' })
export class RpDcropparcelgeom {
  @Column('uuid', { primary: true, name: 'rcp_id' })
  rcpId: string;

  @Column('geometry', {
    name: 'cpg_geometry',
    nullable: true,
    srid: 3301,
  })
  cpgGeometry: string | null;

  @Column('numeric', {
    name: 'cpg_surface',
    nullable: true,
    precision: 10,
    scale: 2,
  })
  cpgSurface: string | null;

  @Column('date', { primary: true, name: 'cpg_datefrom' })
  cpgDatefrom: string;

  @Column('date', { name: 'cpg_dateto', nullable: true })
  cpgDateto: string | null;

  @ManyToOne(
    () => RpDcropparcel,
    (rpDcropparcel) => rpDcropparcel.rpDcropparcelgeoms,
  )
  @JoinColumn([{ name: 'rcp_id', referencedColumnName: 'rcpId' }])
  rcp: RpDcropparcel;
}
