/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
} from 'typeorm';
import { CoPcountry } from './co_pcountry.entity';
import { CoPphytosanequip } from './co_pphytosanequip.entity';
import { CoPphytosanitary } from './co_pphytosanitary.entity';
import { RpDcropparcel } from './rp_dcropparcel.entity';
import { CoPunittype } from './co_punittype.entity';

@Index('pk_rp_dcropphytosanit', ['couId', 'cphStartdate', 'phyId', 'rcpId'], {
  unique: true,
})
@Entity('rp_dcropphytosanit', { schema: 'sc_niva' })
export class RpDcropphytosanit extends BaseEntity {
  @Column('uuid', { primary: true, name: 'rcp_id' })
  rcpId: string;

  @Column('character varying', { primary: true, name: 'phy_id', length: 8 })
  phyId: string;

  @Column('character varying', { primary: true, name: 'cou_id', length: 2 })
  couId: string;

  @Column('numeric', {
    name: 'cph_dose',
    nullable: true,
    precision: 8,
    scale: 2,
  })
  cphDose: string | null;

  @Column('date', { primary: true, name: 'cph_startdate' })
  cphStartdate: string;

  @Column('date', { name: 'cph_enddate', nullable: true })
  cphEnddate: string | null;

  @Column('date', { name: 'cph_expirydate', nullable: true })
  cphExpirydate: string | null;

  @ManyToOne(() => CoPcountry, (coPcountry) => coPcountry.rpDcropphytosanits)
  @JoinColumn([{ name: 'cou_id', referencedColumnName: 'couId' }])
  cou: CoPcountry;

  @ManyToOne(
    () => CoPphytosanequip,
    (coPphytosanequip) => coPphytosanequip.rpDcropphytosanits,
  )
  @JoinColumn([{ name: 'phe_id', referencedColumnName: 'pheId' }])
  phe: CoPphytosanequip;

  @ManyToOne(
    () => CoPphytosanitary,
    (coPphytosanitary) => coPphytosanitary.rpDcropphytosanits,
  )
  @JoinColumn([{ name: 'phy_id', referencedColumnName: 'phyId' }])
  phy: CoPphytosanitary;

  @ManyToOne(
    () => RpDcropparcel,
    (rpDcropparcel) => rpDcropparcel.rpDcropphytosanits,
  )
  @JoinColumn([{ name: 'rcp_id', referencedColumnName: 'rcpId' }])
  rcp: RpDcropparcel;

  @ManyToOne(() => CoPunittype, (coPunittype) => coPunittype.rpDcropphytosanits)
  @JoinColumn([{ name: 'uni_id', referencedColumnName: 'uniId' }])
  uni: CoPunittype;

  @Column('smallint', { name: 'uni_id' })
  uniId: number;
}
