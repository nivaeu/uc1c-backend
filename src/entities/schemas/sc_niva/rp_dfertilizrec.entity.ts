/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { RpDcropparcel } from './rp_dcropparcel.entity';

@Index('pk_rp_dfertilizrec', ['freEntrydate', 'rcpId'], { unique: true })
@Entity('rp_dfertilizrec', { schema: 'sc_niva' })
export class RpDfertilizrec {
  @Column('uuid', { primary: true, name: 'rcp_id' })
  rcpId: string;

  @Column('numeric', { name: 'fre_n', nullable: true, precision: 5, scale: 2 })
  freN: string | null;

  @Column('numeric', { name: 'fre_p', nullable: true, precision: 5, scale: 2 })
  freP: string | null;

  @Column('numeric', { name: 'fre_k', nullable: true, precision: 5, scale: 2 })
  freK: string | null;

  @Column('date', { name: 'fre_fertilizdate', nullable: true })
  freFertilizdate: string | null;

  @Column('date', { primary: true, name: 'fre_entrydate' })
  freEntrydate: string;

  @Column('date', { name: 'fre_expirydate', nullable: true })
  freExpirydate: string | null;

  @ManyToOne(
    () => RpDcropparcel,
    (rpDcropparcel) => rpDcropparcel.rpDfertilizrecs,
  )
  @JoinColumn([{ name: 'rcp_id', referencedColumnName: 'rcpId' }])
  rcp: RpDcropparcel;
}
