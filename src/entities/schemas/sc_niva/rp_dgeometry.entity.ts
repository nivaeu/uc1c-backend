/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { RpDrefplot } from './rp_drefplot.entity';

@Index('pk_rp_dgeometry', ['rfpId', 'rgeDatefrom'], { unique: true })
@Entity('rp_dgeometry', { schema: 'sc_niva' })
export class RpDgeometry {
  @Column('uuid', { primary: true, name: 'rfp_id' })
  rfpId: string;

  @Column('geometry', { name: 'rge_geometry', srid: 3301 })
  rgeGeometry: string;

  @Column('numeric', {
    name: 'rge_surface',
    nullable: true,
    precision: 10,
    scale: 2,
  })
  rgeSurface: string | null;

  @Column('date', { primary: true, name: 'rge_datefrom' })
  rgeDatefrom: string;

  @Column('date', { name: 'rge_dateto', nullable: true })
  rgeDateto: string | null;

  @ManyToOne(() => RpDrefplot, (rpDrefplot) => rpDrefplot.rpDgeometries)
  @JoinColumn([{ name: 'rfp_id', referencedColumnName: 'rfpId' }])
  rfp: RpDrefplot;
}
