/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { RpDcropparcel } from './rp_dcropparcel.entity';
import { CoPunittype } from './co_punittype.entity';

@Index('pk_rp_dirrigationrec', ['ireIrrigationdate', 'rcpId'], { unique: true })
@Entity('rp_dirrigationrec', { schema: 'sc_niva' })
export class RpDirrigationrec {
  @Column('uuid', { primary: true, name: 'rcp_id' })
  rcpId: string;

  @Column('numeric', {
    name: 'ire_volume',
    nullable: true,
    precision: 8,
    scale: 2,
  })
  ireVolume: string | null;

  @Column('date', { primary: true, name: 'ire_irrigationdate' })
  ireIrrigationdate: string;

  @Column('date', { name: 'ire_expirydate', nullable: true })
  ireExpirydate: string | null;

  @ManyToOne(
    () => RpDcropparcel,
    (rpDcropparcel) => rpDcropparcel.rpDirrigationrecs,
  )
  @JoinColumn([{ name: 'rcp_id', referencedColumnName: 'rcpId' }])
  rcp: RpDcropparcel;

  @ManyToOne(() => CoPunittype, (coPunittype) => coPunittype.rpDirrigationrecs)
  @JoinColumn([{ name: 'uni_id', referencedColumnName: 'uniId' }])
  uni: CoPunittype;
}
