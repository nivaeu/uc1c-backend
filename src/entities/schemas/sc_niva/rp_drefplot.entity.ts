/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { RpDcropparcel } from './rp_dcropparcel.entity';
import { RpDgeometry } from './rp_dgeometry.entity';
import { AbDagrblock } from './ab_dagrblock.entity';

@Index('pk_rp_drefplot', ['rfpId'], { unique: true })
@Entity('rp_drefplot', { schema: 'sc_niva' })
export class RpDrefplot extends BaseEntity {
  @Column('uuid', { primary: true, name: 'rfp_id' })
  rfpId: string;

  @Column('character varying', { name: 'rfp_key', length: 50 })
  rfpKey: string;

  @Column('character varying', { name: 'rfp_refparcel', length: 28 })
  rfpRefparcel: string;

  @Column('date', { name: 'rfp_datefrom', nullable: true })
  rfpDatefrom: string | null;

  @Column('date', { name: 'rfp_dateto', nullable: true })
  rfpDateto: string | null;

  @OneToMany(() => RpDcropparcel, (rpDcropparcel) => rpDcropparcel.rfp)
  rpDcropparcels: RpDcropparcel[];

  @OneToMany(() => RpDgeometry, (rpDgeometry) => rpDgeometry.rfp)
  rpDgeometries: RpDgeometry[];

  @ManyToOne(() => AbDagrblock, (abDagrblock) => abDagrblock.rpDrefplots)
  @JoinColumn([{ name: 'abl_id', referencedColumnName: 'ablId' }])
  abl: AbDagrblock;
}
