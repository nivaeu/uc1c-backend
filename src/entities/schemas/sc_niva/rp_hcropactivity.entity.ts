/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { RevDrevinfo } from './rev_drevinfo.entity';

@Index('pk_rp_hcropactivity', ['rcpId', 'rev'], { unique: true })
@Entity('rp_hcropactivity', { schema: 'sc_niva' })
export class RpHcropactivity {
  @Column('uuid', { primary: true, name: 'rcp_id' })
  rcpId: string;

  @Column('boolean', { name: 'aga_agriactivity', nullable: true })
  agaAgriactivity: boolean | null;

  @Column('smallint', { name: 'cus_id', nullable: true })
  cusId: number | null;

  @Column('smallint', { name: 'cud_id', nullable: true })
  cudId: number | null;

  @Column('smallint', { name: 'see_id', nullable: true })
  seeId: number | null;

  @Column('smallint', { name: 'orf_id', nullable: true })
  orfId: number | null;

  @Column('integer', { primary: true, name: 'rev' })
  rev: number;

  @Column('smallint', { name: 'revtype', nullable: true })
  revtype: number | null;

  @ManyToOne(() => RevDrevinfo, (revDrevinfo) => revDrevinfo.rpHcropactivities)
  @JoinColumn([{ name: 'rev', referencedColumnName: 'rev' }])
  rev2: RevDrevinfo;
}
