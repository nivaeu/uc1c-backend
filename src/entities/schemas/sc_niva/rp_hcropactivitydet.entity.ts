/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { RevDrevinfo } from './rev_drevinfo.entity';

@Index('pk_rp_hcropactivitydet', ['cadStartdate', 'labId', 'rcpId', 'rev'], {
  unique: true,
})
@Entity('rp_hcropactivitydet', { schema: 'sc_niva' })
export class RpHcropactivitydet {
  @Column('uuid', { primary: true, name: 'rcp_id' })
  rcpId: string;

  @Column('smallint', { primary: true, name: 'lab_id' })
  labId: number;

  @Column('date', { primary: true, name: 'cad_startdate' })
  cadStartdate: string;

  @Column('date', { name: 'cad_enddate', nullable: true })
  cadEnddate: string | null;

  @Column('integer', { primary: true, name: 'rev' })
  rev: number;

  @Column('smallint', { name: 'revtype', nullable: true })
  revtype: number | null;

  @ManyToOne(
    () => RevDrevinfo,
    (revDrevinfo) => revDrevinfo.rpHcropactivitydets,
  )
  @JoinColumn([{ name: 'rev', referencedColumnName: 'rev' }])
  rev2: RevDrevinfo;
}
