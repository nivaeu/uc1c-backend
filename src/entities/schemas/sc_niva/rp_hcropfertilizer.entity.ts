/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { RevDrevinfo } from './rev_drevinfo.entity';

@Index('pk_rp_hcropfertilizer', ['cfeStartdate', 'fetId', 'rcpId', 'rev'], {
  unique: true,
})
@Entity('rp_hcropfertilizer', { schema: 'sc_niva' })
export class RpHcropfertilizer {
  @Column('uuid', { primary: true, name: 'rcp_id' })
  rcpId: string;

  @Column('smallint', { primary: true, name: 'fet_id' })
  fetId: number;

  @Column('smallint', { name: 'fer_id', nullable: true })
  ferId: number | null;

  @Column('smallint', { name: 'frt_id', nullable: true })
  frtId: number | null;

  @Column('smallint', { name: 'fem_id', nullable: true })
  femId: number | null;

  @Column('numeric', {
    name: 'cfe_dose',
    nullable: true,
    precision: 8,
    scale: 2,
  })
  cfeDose: string | null;

  @Column('smallint', { name: 'uni_id', nullable: true })
  uniId: number | null;

  @Column('date', { primary: true, name: 'cfe_startdate' })
  cfeStartdate: string;

  @Column('date', { name: 'cfe_enddate', nullable: true })
  cfeEnddate: string | null;

  @Column('date', { name: 'cfe_expiry_date', nullable: true })
  cfeExpiryDate: string | null;

  @Column('integer', { primary: true, name: 'rev' })
  rev: number;

  @Column('smallint', { name: 'revtype', nullable: true })
  revtype: number | null;

  @ManyToOne(() => RevDrevinfo, (revDrevinfo) => revDrevinfo.rpHcropfertilizers)
  @JoinColumn([{ name: 'rev', referencedColumnName: 'rev' }])
  rev2: RevDrevinfo;
}
