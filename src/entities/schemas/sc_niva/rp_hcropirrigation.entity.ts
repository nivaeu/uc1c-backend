/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { RevDrevinfo } from './rev_drevinfo.entity';

@Index('pk_rp_hcropirrigation', ['criIrrigationdate', 'rcpId', 'rev'], {
  unique: true,
})
@Entity('rp_hcropirrigation', { schema: 'sc_niva' })
export class RpHcropirrigation {
  @Column('uuid', { primary: true, name: 'rcp_id' })
  rcpId: string;

  @Column('smallint', { name: 'irr_id' })
  irrId: number;

  @Column('smallint', { name: 'wao_id', nullable: true })
  waoId: number | null;

  @Column('numeric', {
    name: 'cri_volume',
    nullable: true,
    precision: 8,
    scale: 2,
  })
  criVolume: string | null;

  @Column('smallint', { name: 'uni_id', nullable: true })
  uniId: number | null;

  @Column('date', { primary: true, name: 'cri_irrigationdate' })
  criIrrigationdate: string;

  @Column('integer', { primary: true, name: 'rev' })
  rev: number;

  @Column('smallint', { name: 'revtype', nullable: true })
  revtype: number | null;

  @ManyToOne(() => RevDrevinfo, (revDrevinfo) => revDrevinfo.rpHcropirrigations)
  @JoinColumn([{ name: 'rev', referencedColumnName: 'rev' }])
  rev2: RevDrevinfo;
}
