/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { RevDrevinfo } from './rev_drevinfo.entity';

@Index('pk_rp_hcropparcel', ['rcpId', 'rev'], { unique: true })
@Entity('rp_hcropparcel', { schema: 'sc_niva' })
export class RpHcropparcel {
  @Column('uuid', { primary: true, name: 'rcp_id' })
  rcpId: string;

  @Column('character varying', { name: 'rcp_key', length: 50 })
  rcpKey: string;

  @Column('uuid', { name: 'rfp_id' })
  rfpId: string;

  @Column('boolean', { name: 'rcp_irrigation' })
  rcpIrrigation: boolean;

  @Column('character varying', { name: 'crp_id', length: 6 })
  crpId: string;

  @Column('character varying', { name: 'crv_id', nullable: true, length: 5 })
  crvId: string | null;

  @Column('boolean', { name: 'rcp_landscfeatures', nullable: true })
  rcpLandscfeatures: boolean | null;

  @Column('smallint', { name: 'lte_id', nullable: true })
  lteId: number | null;

  @Column('character varying', {
    name: 'rcp_datefrom',
    nullable: true,
    length: 50,
  })
  rcpDatefrom: string | null;

  @Column('character varying', {
    name: 'rcp_dateto',
    nullable: true,
    length: 50,
  })
  rcpDateto: string | null;

  @Column('integer', { primary: true, name: 'rev' })
  rev: number;

  @Column('smallint', { name: 'revtype', nullable: true })
  revtype: number | null;

  @ManyToOne(() => RevDrevinfo, (revDrevinfo) => revDrevinfo.rpHcropparcels)
  @JoinColumn([{ name: 'rev', referencedColumnName: 'rev' }])
  rev2: RevDrevinfo;
}
