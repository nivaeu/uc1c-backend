/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { RevDrevinfo } from './rev_drevinfo.entity';

@Index('pk_rp_hcropparcelgeom', ['cpgDateto', 'rcpId', 'rev'], { unique: true })
@Entity('rp_hcropparcelgeom', { schema: 'sc_niva' })
export class RpHcropparcelgeom {
  @Column('uuid', { primary: true, name: 'rcp_id' })
  rcpId: string;

  @Column('geometry', { name: 'cpg_geometry', nullable: true, srid: 3301 })
  cpgGeometry: string | null;

  @Column('numeric', {
    name: 'cpg_surface',
    nullable: true,
    precision: 10,
    scale: 2,
  })
  cpgSurface: string | null;

  @Column('date', { name: 'cpg_datefrom', nullable: true })
  cpgDatefrom: string | null;

  @Column('date', { primary: true, name: 'cpg_dateto' })
  cpgDateto: string;

  @Column('integer', { primary: true, name: 'rev' })
  rev: number;

  @Column('smallint', { name: 'revtype', nullable: true })
  revtype: number | null;

  @ManyToOne(() => RevDrevinfo, (revDrevinfo) => revDrevinfo.rpHcropparcelgeoms)
  @JoinColumn([{ name: 'rev', referencedColumnName: 'rev' }])
  rev2: RevDrevinfo;
}
