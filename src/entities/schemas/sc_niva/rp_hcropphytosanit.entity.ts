/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { RevDrevinfo } from './rev_drevinfo.entity';

@Index(
  'pk_rp_hcropphytosanit',
  ['couId', 'cphStartdate', 'phyId', 'rcpId', 'rev'],
  { unique: true },
)
@Entity('rp_hcropphytosanit', { schema: 'sc_niva' })
export class RpHcropphytosanit {
  @Column('uuid', { primary: true, name: 'rcp_id' })
  rcpId: string;

  @Column('character varying', { primary: true, name: 'phy_id', length: 8 })
  phyId: string;

  @Column('character varying', { primary: true, name: 'cou_id', length: 2 })
  couId: string;

  @Column('numeric', {
    name: 'cph_dose',
    nullable: true,
    precision: 8,
    scale: 2,
  })
  cphDose: string | null;

  @Column('smallint', { name: 'uni_id', nullable: true })
  uniId: number | null;

  @Column('smallint', { name: 'phe_id', nullable: true })
  pheId: number | null;

  @Column('date', { primary: true, name: 'cph_startdate' })
  cphStartdate: string;

  @Column('date', { name: 'cph_enddate', nullable: true })
  cphEnddate: string | null;

  @Column('date', { name: 'cph_expiry_date', nullable: true })
  cphExpiryDate: string | null;

  @Column('integer', { primary: true, name: 'rev' })
  rev: number;

  @Column('smallint', { name: 'revtype', nullable: true })
  revtype: number | null;

  @ManyToOne(() => RevDrevinfo, (revDrevinfo) => revDrevinfo.rpHcropphytosanits)
  @JoinColumn([{ name: 'rev', referencedColumnName: 'rev' }])
  rev2: RevDrevinfo;
}
