/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { RevDrevinfo } from './rev_drevinfo.entity';

@Index('pk_rp_hgeometry', ['rev', 'rfpId', 'rgeDatefrom'], { unique: true })
@Entity('rp_hgeometry', { schema: 'sc_niva' })
export class RpHgeometry {
  @Column('uuid', { primary: true, name: 'rfp_id' })
  rfpId: string;

  @Column('geometry', { name: 'rge_geometry', srid: 3301 })
  rgeGeometry: string;

  @Column('numeric', {
    name: 'rge_surface',
    nullable: true,
    precision: 10,
    scale: 2,
  })
  rgeSurface: string | null;

  @Column('date', { primary: true, name: 'rge_datefrom' })
  rgeDatefrom: string;

  @Column('date', { name: 'rge_dateto', nullable: true })
  rgeDateto: string | null;

  @Column('integer', { primary: true, name: 'rev' })
  rev: number;

  @Column('smallint', { name: 'revtype', nullable: true })
  revtype: number | null;

  @ManyToOne(() => RevDrevinfo, (revDrevinfo) => revDrevinfo.rpHgeometries)
  @JoinColumn([{ name: 'rev', referencedColumnName: 'rev' }])
  rev2: RevDrevinfo;
}
