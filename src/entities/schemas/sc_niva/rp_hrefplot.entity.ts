/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { RevDrevinfo } from './rev_drevinfo.entity';

@Index('pk_rp_hrefplot', ['rev', 'rfpId'], { unique: true })
@Entity('rp_hrefplot', { schema: 'sc_niva' })
export class RpHrefplot {
  @Column('uuid', { primary: true, name: 'rfp_id' })
  rfpId: string;

  @Column('character varying', { name: 'rfp_key', length: 50 })
  rfpKey: string;

  @Column('uuid', { name: 'abl_id' })
  ablId: string;

  @Column('character varying', { name: 'rfp_refparcel', length: 28 })
  rfpRefparcel: string;

  @Column('date', { name: 'rfp_datefrom', nullable: true })
  rfpDatefrom: string | null;

  @Column('date', { name: 'rfp_dateto', nullable: true })
  rfpDateto: string | null;

  @Column('integer', { primary: true, name: 'rev' })
  rev: number;

  @Column('smallint', { name: 'revtype', nullable: true })
  revtype: number | null;

  @ManyToOne(() => RevDrevinfo, (revDrevinfo) => revDrevinfo.rpHrefplots)
  @JoinColumn([{ name: 'rev', referencedColumnName: 'rev' }])
  rev2: RevDrevinfo;
}
