/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, JoinTable, ManyToMany } from 'typeorm';
import { SecDuser } from './sec_duser.entity';

@Index('pk_sec_drol', ['rolId'], { unique: true })
@Entity('sec_drol', { schema: 'sc_niva' })
export class SecDrol {
  @Column('smallint', { primary: true, name: 'rol_id' })
  rolId: number;

  @Column('character varying', { name: 'rol_name', length: 50 })
  rolName: string;

  @ManyToMany(() => SecDuser, (secDuser) => secDuser.secDrols)
  @JoinTable({
    name: 'sec_duser_rol',
    joinColumns: [{ name: 'rol_id', referencedColumnName: 'rolId' }],
    inverseJoinColumns: [{ name: 'usr_id', referencedColumnName: 'usrId' }],
    schema: 'sc_niva',
  })
  secDusers: SecDuser[];
}
