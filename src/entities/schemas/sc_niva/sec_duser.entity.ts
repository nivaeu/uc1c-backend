/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToMany,
  ManyToOne,
} from 'typeorm';
import { CoPcountry } from './co_pcountry.entity';
import { SecDrol } from './sec_drol.entity';

@Index('pk_sec_duser', ['usrId'], { unique: true })
@Entity('sec_duser', { schema: 'sc_niva' })
export class SecDuser {
  @Column('smallint', { primary: true, name: 'usr_id' })
  usrId: number;

  @Column('character varying', { name: 'usr_username', length: 50 })
  usrUsername: string;

  @Column('character varying', { name: 'usr_password', length: 255 })
  usrPassword: string;

  @ManyToOne(() => CoPcountry, (coPcountry) => coPcountry.secDusers)
  @JoinColumn([{ name: 'cou_id', referencedColumnName: 'couId' }])
  cou: CoPcountry;

  @ManyToMany(() => SecDrol, (secDrol) => secDrol.secDusers)
  secDrols: SecDrol[];
}
