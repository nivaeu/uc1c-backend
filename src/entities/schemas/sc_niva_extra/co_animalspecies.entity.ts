/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { BaseEntity, Column, Entity, Index } from 'typeorm';

@Index('pk_co_animalspecies', ['ansId'], { unique: true })
@Entity('co_animalspecies', { schema: 'sc_niva_extra' })
export class CoAnimalSpecies extends BaseEntity {
  @Column({
    primary: true,
    name: 'ans_id',
    length: 5,
    comment: 'Internal registry ID in database',
  })
  ansId: string;

  @Column({
    name: 'ans_name_et',
    length: 150,
    comment: 'Description in local language (Estonian)',
  })
  ansNameEt: string;

  @Column({
    name: 'ans_name_en',
    length: 150,
    comment: 'Description in English',
  })
  ansNameEn: string;

  @Column('character varying', {
    name: 'ans_name_short_et',
    length: 10,
    comment: 'Short description for diagrams, in local language (Estonian)',
  })
  ansNameShortEt: string;

  @Column('character varying', {
    name: 'ans_name_short_en',
    length: 10,
    comment: 'Short description for diagrams, in English',
  })
  ansNameShortEn: string;

  @Column('date', {
    name: 'ans_expirydate',
    nullable: true,
    comment: 'Expiry Date',
  })
  ansExpiryDate: string | null;
}
