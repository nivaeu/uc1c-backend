/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import {
  BaseEntity,
  Column,
  Entity,
  Index,
  ManyToOne,
  JoinColumn,
} from 'typeorm';
import { CoAnimalSpecies } from './co_animalspecies.entity';

@Index('pk_co_animalspecies_detail', ['andId'], {
  unique: true,
})
@Entity('co_animalspecies_detail', { schema: 'sc_niva_extra' })
export class CoAnimalSpeciesDetail extends BaseEntity {
  @Column({
    primary: true,
    name: 'and_id',
    length: 6,
  })
  andId: string;

  @ManyToOne(() => CoAnimalSpecies, (coAnimalSpecies) => coAnimalSpecies.ansId)
  @JoinColumn([
    {
      name: 'ans_id',
      referencedColumnName: 'ansId',
    },
  ])
  ansId: CoAnimalSpecies;

  @Column({
    name: 'and_name_et',
    length: 150,
    comment: 'Description in local language (Estonian)',
  })
  andNameEt: string;

  @Column({
    name: 'and_name_en',
    length: 150,
    comment: 'Description in English',
  })
  andNameEn: string;

  @Column('numeric', {
    name: 'and_lu',
    nullable: true,
    comment: 'Livestock unit coefficent value',
  })
  andLu: number;

  @Column('date', {
    name: 'and_expirydate',
    nullable: true,
    comment: 'Expiry Date',
  })
  andExpiryDate: string | null;
}
