/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, ManyToOne, JoinColumn } from 'typeorm';
import { CoStandardoutput } from './co_standardoutput.entity';
import { CouAnimalSoc } from './cou_animal_soc.entity';

@Index('pk_co_animalspecies_detail_soc', ['andId'], { unique: true })
@Entity('co_animalspecies_detail_soc', { schema: 'sc_niva_extra' })
export class CoAnimalSpeciesDetailSoc {
  @Column({
    primary: true,
    name: 'and_id',
    length: 6,
    comment: 'Internal registry ID related with CO_ANIMALSPECIES_DETAIL table',
  })
  andId: string;

  @ManyToOne(() => CouAnimalSoc, (couAnimalSoc) => couAnimalSoc.socId)
  @JoinColumn([{ name: 'soc_id', referencedColumnName: 'socId' }])
  socId: CouAnimalSoc;

  @ManyToOne(
    () => CoStandardoutput,
    (coStandardoutput) => coStandardoutput.soId,
  )
  @JoinColumn([{ name: 'so_id', referencedColumnName: 'soId' }])
  soId: CoStandardoutput;

  @Column('date', {
    name: 'expirydate',
    nullable: true,
    comment: 'Expiry Date',
  })
  expiryDate: string | null;
}
