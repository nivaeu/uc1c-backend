/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import {
  Column,
  Entity,
  Index,
  ManyToOne,
  JoinColumn,
  OneToMany,
} from 'typeorm';
import { CoPcountry } from '../sc_niva/co_pcountry.entity';
import { CoStandardoutput } from './co_standardoutput.entity';

@Index('pk_co_nutsclassification', ['nclId'], { unique: true })
@Entity('co_nutsclassification', {
  schema: 'sc_niva_extra',
})
export class CoNutsclassification {
  @Column('uuid', {
    primary: true,
    name: 'ncl_id',
  })
  nclId: string;

  @OneToMany(
    () => CoStandardoutput,
    (coStandardoutput) => coStandardoutput.nclId,
  )
  coStandardoutputs: CoStandardoutput[];

  @ManyToOne(() => CoPcountry, (coPcountry) => coPcountry.coPcropprodgroups)
  @JoinColumn([{ name: 'cou_id', referencedColumnName: 'couId' }])
  couId: CoPcountry;

  @Column({ length: 5, name: 'ncl_level1', comment: 'NUTS level 1 code' })
  nclLevelOne: string;

  @Column({ length: 5, name: 'ncl_level2', comment: 'NUTS level 2 code' })
  nclLevelTwo: string;

  @Column({ length: 5, name: 'ncl_level3', comment: 'NUTS level 3 code' })
  nclLevelThree: string;

  @Column('date', {
    nullable: true,
    name: 'ncl_expirydate',
    comment: 'Expiry Date',
  })
  nclExpiryDate: string | null;
}
