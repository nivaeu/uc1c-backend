/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, BaseEntity } from 'typeorm';

@Index('pk_co_pcroplandcover', ['clanId'], { unique: true })
@Entity('co_pcroplandcover', { schema: 'sc_niva_extra' })
export class CoPcropLandCover extends BaseEntity {
  @Column({ primary: true, name: 'clan_id', length: 5 })
  clanId: string;

  @Column({
    name: 'clan_name_et',
    length: 255,
    comment: 'Description, in local language (Estonian) ',
  })
  clanNameEt: string;

  @Column({ name: 'clan_name_en', length: 255, comment: 'Description' })
  clanNameEn: string;

  @Column('character varying', {
    name: 'clan_name_short_en',
    length: 5,
    comment: 'Description in short format (for diagrams)',
  })
  clanNameShortEn: string;

  @Column('character varying', {
    name: 'clan_name_short_et',
    length: 5,
    comment:
      'Description in short format (for diagrams), in local language (Estonian)',
  })
  clanNameShortEt: string;

  @Column('date', {
    name: 'clan_expirydate',
    nullable: true,
    comment: 'Expiry Date',
  })
  clanExpiryDate: string | null;
}
