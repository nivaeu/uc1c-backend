/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, ManyToOne, JoinColumn } from 'typeorm';
import { CoNutsclassification } from './co_nutsclassification.entity';

@Index('pk_co_standardoutput', ['soId'], { unique: true })
@Entity('co_standardoutput', { schema: 'sc_niva_extra' })
export class CoStandardoutput {
  @Column('character varying', { primary: true, name: 'so_id', length: 6 })
  soId: string;

  @Column({
    name: 'so_code',
    length: 10,
    comment:
      'Field ID from Eurostat Standard Output coefficents country table ',
  })
  soCode: string;

  @Column({
    name: 'so_label_en',
    length: 100,
    comment: 'Label from Eurostat Standard Output coefficents country table ',
  })
  soLabelEn: string;

  @Column({
    name: 'so_label_et',
    length: 100,
    comment:
      'Label from Eurostat Standard Output coefficents country table, in local language (Estonian)',
  })
  soLabelEt: string;

  @Column({
    name: 'so_unit_en',
    length: 100,
    comment: 'Unit from Eurostat Standard Output coefficents country table',
  })
  soUnitEn: string;

  @Column({
    name: 'so_unit_et',
    length: 100,
    comment:
      'Unit from Eurostat Standard Output coefficents country table, in local language (Estonian)',
  })
  soUnitEt: string;

  @Column({
    name: 'so_yearseries',
    length: 4,
    comment: 'Year from Eurostat Standard Output coefficents country table',
  })
  soYearSeries: string;

  @Column({
    name: 'so_value',
    precision: 10,
    scale: 2,
    type: 'numeric',
    nullable: true,
    comment:
      'Monetary value (in euros) from Eurostat Standard Output coefficents country table',
  })
  soValue: string | null;

  @Column('date', {
    name: 'so_datefrom',
    comment: 'Start date of the validity period of the data',
  })
  soDatefrom: string;

  @Column('date', {
    name: 'so_dateto',
    nullable: true,
    comment: 'End date of the validity period of the data',
  })
  soDateTo: string | null;

  @ManyToOne(
    () => CoNutsclassification,
    (coNutsclassification) => coNutsclassification.coStandardoutputs,
  )
  @JoinColumn([{ name: 'ncl_id', referencedColumnName: 'nclId' }])
  nclId: CoNutsclassification;
}
