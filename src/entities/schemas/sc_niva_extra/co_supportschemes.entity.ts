/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { BaseEntity, Column, Entity, Index, OneToMany } from 'typeorm';
import { FarDpayment } from './far_dpayment.entity';

@Index('pk_co_supportschemes', ['susId'], { unique: true })
@Entity('co_supportschemes', { schema: 'sc_niva_extra' })
export class CoSupportSchemes extends BaseEntity {
  @Column('uuid', { primary: true, name: 'sus_id' })
  susId: string;

  @Column({
    name: 'sus_name_et',
    length: 100,
    comment: 'Description in local language (Estonian)',
  })
  susNameEt: string;

  @Column({
    name: 'sus_name_en',
    length: 100,
    comment: 'Description in English',
  })
  susNameEn: string;

  @Column('date', {
    name: 'sus_expirydate',
    nullable: true,
    comment: 'Expiry Date',
  })
  susExpiryDate: string;

  @Column('character varying', {
    name: 'sus_name_short_et',
    length: 20,
    comment:
      'Description short format (for diagrams), in local language (Estonian)',
  })
  susNameShortEt: string;

  @Column('character varying', {
    name: 'sus_name_short_en',
    length: 20,
    comment: 'Description short format (for diagrams), in English',
  })
  susNameShortEn: string;

  @OneToMany(() => FarDpayment, (farDpayment) => farDpayment.susId)
  farDpayments: FarDpayment[];
}
