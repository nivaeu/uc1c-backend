/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { LocCoPcountry } from './loc/loc_co_pcountry.entity';

@Index('pk_cou_animal_soc', ['socId'], { unique: true })
@Entity('cou_animal_soc', { schema: 'sc_niva_extra' })
export class CouAnimalSoc {
  @Column({ primary: true, name: 'soc_id', length: 6 })
  socId: string;

  @Column({ name: 'soc', length: 20, comment: 'Standard Output code' })
  soc: string;

  @Column({ name: 'soc_label', length: 100, comment: 'Description' })
  socLabel: string;

  @ManyToOne(() => LocCoPcountry, (locCoPcountry) => locCoPcountry.couId)
  @JoinColumn([{ name: 'cou_id', referencedColumnName: 'couId' }])
  couId: LocCoPcountry;

  @Column('date', {
    name: 'soc_expirydate',
    nullable: true,
    comment: ' Expiry Date',
  })
  socExpiryDate: string | null;
}
