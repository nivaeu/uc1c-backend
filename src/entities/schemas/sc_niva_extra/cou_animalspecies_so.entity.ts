/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, OneToOne, JoinColumn } from 'typeorm';
import { CoAnimalSpecies } from './co_animalspecies.entity';
import { CoStandardoutput } from './co_standardoutput.entity';

@Index('pk_cou_animalspecies_so', ['ansoId'], { unique: true })
@Entity('cou_animalspecies_so', { schema: 'sc_niva_extra' })
export class CouAnimalSpeciesSo {
  @Column('uuid', {
    primary: true,
    name: 'anso_id',
    comment:
      'Internal ID to identify the register on database. Only used related with other tables',
  })
  ansoId: string;

  @OneToOne(() => CoAnimalSpecies, (coAnimalSpecies) => coAnimalSpecies.ansId)
  @JoinColumn([{ name: 'ans_id', referencedColumnName: 'ansId' }])
  CoAnimalSpecies: CoAnimalSpecies;

  @OneToOne(() => CoStandardoutput, (coStandardoutput) => coStandardoutput.soId)
  @JoinColumn([{ name: 'so_id', referencedColumnName: 'soId' }])
  CoStandardoutput: CoStandardoutput;

  @Column('date', {
    name: 'anso_datefrom',
    comment: 'Start date of the validity period of the data',
  })
  ansoDateFrom: string;

  @Column('date', {
    name: 'anso_dateto',
    nullable: true,
    comment: 'End date of the validity period of the data',
  })
  ansoDateTo: string | null;
}
