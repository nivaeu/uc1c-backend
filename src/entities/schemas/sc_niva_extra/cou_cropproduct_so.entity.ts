/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, OneToOne, JoinColumn } from 'typeorm';
import { CoStandardoutput } from './co_standardoutput.entity';
import { CoPcropproduct } from '../sc_niva/co_pcropproduct.entity';

@Index('pk_cou_cropproduct_so', ['cropsoId'], { unique: true })
@Entity('cou_cropproduct_so', { schema: 'sc_niva_extra' })
export class CouCropproductSo {
  @Column('uuid', {
    primary: true,
    name: 'cropso_id',
    comment:
      'Internal ID to identify the register on database. Only used related with other tables',
  })
  cropsoId: string;

  @OneToOne(() => CoPcropproduct, (coPcropproduct) => coPcropproduct.crpId)
  @JoinColumn([{ name: 'crp_id', referencedColumnName: 'crpId' }])
  CoPcropproduct: CoPcropproduct;

  @OneToOne(() => CoStandardoutput, (coStandardoutput) => coStandardoutput.soId)
  @JoinColumn([{ name: 'so_id', referencedColumnName: 'soId' }])
  CoStandardoutput: CoStandardoutput;

  @Column('date', {
    name: 'cropso_datefrom',
    comment: 'Start date of the validity period of the data',
  })
  cropsoDateFrom: string;

  @Column('date', {
    name: 'cropso_dateto',
    nullable: true,
    comment: 'End date of the validity period of the data',
  })
  cropsoDateTo: string | null;
}
