/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import {
  Column,
  Entity,
  Index,
  JoinColumn,
  BaseEntity,
  ManyToOne,
} from 'typeorm';

import { CoPcountry } from '../sc_niva/co_pcountry.entity';

@Index('pk_cou_cropproduct_soc', ['socId'], { unique: true })
@Entity('cou_cropproduct_soc', { schema: 'sc_niva_extra' })
export class CouCropProductSoc extends BaseEntity {
  @Column({
    primary: true,
    name: 'soc_id',
    length: 5,
    comment: 'Internal registry ID in database',
  })
  socId: string;

  @Column({ name: 'soc', length: 30, comment: 'SOC code' })
  soc: string;

  @Column({ name: 'soc_label', length: 255, comment: 'SOC heading' })
  socLabel: string;

  @ManyToOne(() => CoPcountry, (coPcountry) => coPcountry.couId)
  @JoinColumn([{ name: 'cou_id', referencedColumnName: 'couId' }])
  CoPcountry: CoPcountry;

  @Column('date', {
    name: 'soc_expirydate',
    nullable: true,
    comment: 'Expiry Date',
  })
  socExpiryDate: string;
}
