/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm';

@Index('pk_avg_parcel_sizes', ['id'], { unique: true })
@Entity('avg_parcel_sizes', { schema: 'sc_niva_extra' })
export class AverageParcelSizes {
  @PrimaryGeneratedColumn('increment', {
    name: 'id',
    comment: 'Internal registry ID in database',
  })
  id: number;

  @Column({
    name: 'period',
    length: 4,
    comment: 'Year for which calculated data applies',
  })
  period: string;

  @Column({
    name: 'name_et',
    length: 255,
    comment:
      'Land cover type, for what average parcel size has been calculated (in Estonian)',
  })
  nameEt: string;

  @Column({
    name: 'name_en',
    length: 255,
    comment:
      'Land cover type, for what average parcel size has been calculated (in English)',
  })
  nameEn: string;

  @Column('numeric', {
    name: 'avg_parcel_size',
    comment: 'Average size of a parcel, in hectares',
  })
  averageParcelSize: number;

  @Column({
    name: 'economic_class',
    length: 50,
    comment: 'Economic size class, according to Farm Typology calculation  ',
  })
  economicClass: string;

  @Column({
    name: 'production_type',
    length: 50,
    comment: 'Production type, according to Farm Typology calculation',
  })
  productionType: string;
}
