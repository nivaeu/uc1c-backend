/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm';

@Index('pk_land_type_data', ['id'], { unique: true })
@Entity('land_type_data', { schema: 'sc_niva_extra' })
export class LandTypeData {
  @PrimaryGeneratedColumn('increment', {
    name: 'id',
    comment: 'Internal registry ID in database',
  })
  id: number;

  @Column({
    name: 'period',
    length: 4,
    comment: 'Year for which calculated data applies',
  })
  period: string;

  @Column({
    name: 'land_type_en',
    length: 255,
    comment: 'Land cover type (in Estonian)',
  })
  landTypeEn: string;

  @Column({
    name: 'land_type_et',
    length: 255,
    comment: 'Land cover type (in English)',
  })
  landTypeEt: string;

  @Column('numeric', {
    name: 'hectares',
    comment: 'Total size of parcels with this land cover type, in hectares',
  })
  hectares: number;

  @Column({
    name: 'economic_class',
    length: 50,
    comment: 'Economic size class, according to farm typology calculation',
  })
  economicClass: string;

  @Column({
    name: 'production_type',
    length: 50,
    comment: 'Production type, according to farm typology calculation',
  })
  productionType: string;
}
