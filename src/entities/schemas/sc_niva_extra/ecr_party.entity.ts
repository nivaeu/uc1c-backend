/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, BaseEntity } from 'typeorm';

@Index('pk_ecr_party', ['ecrPartyId'], { unique: true })
@Entity('ecr_party', { schema: 'sc_niva_extra' })
export class EcrParty extends BaseEntity {
  @Column({
    primary: true,
    name: 'ecr_party_id',
    comment:
      'Identifier of the party (system) authorized to issue and receive crop report document',
  })
  ecrPartyId: number;

  @Column({
    name: 'ecr_party_name',
    length: 255,
    comment:
      'Name of the party (system) authorized to issue and receive crop report document',
  })
  ecrPartyName: string;

  @Column('date', {
    name: 'ecr_party_expirydate',
    nullable: true,
    comment: 'Expiry Date',
  })
  ecrPartyExpiryDate: string | null;
}
