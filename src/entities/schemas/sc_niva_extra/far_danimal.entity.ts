/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import {
  Column,
  Entity,
  Index,
  ManyToOne,
  JoinColumn,
  BaseEntity,
} from 'typeorm';
import { PrDfarmer } from '../sc_niva/pr_dfarmer.entity';
import { PrDproductionunit } from '../sc_niva/pr_dproductionunit.entity';
import { CoAnimalSpeciesDetail } from './co_animalspecies_detail.entity';

@Index('pk_far_danimal', ['fanId'], { unique: true })
@Entity('far_danimal', { schema: 'sc_niva_extra' })
export class FarDanimal extends BaseEntity {
  @Column('uuid', {
    primary: true,
    name: 'fan_id',
    comment:
      'Internal ID to identify the register on database. Only used related with other tables',
  })
  fanId: string;

  @ManyToOne(() => PrDfarmer, (prDfarmer) => prDfarmer.farDanimals, {
    nullable: false,
  })
  @JoinColumn([{ name: 'far_id', referencedColumnName: 'farId' }])
  farId: PrDfarmer;

  @ManyToOne(
    () => PrDproductionunit,
    (prDproductionunit) => prDproductionunit.farDanimals,
    {
      nullable: false,
    },
  )
  @JoinColumn([{ name: 'pun_id', referencedColumnName: 'punId' }])
  punId: PrDproductionunit;

  @ManyToOne(
    () => CoAnimalSpeciesDetail,
    (coAnimalSpeciesDetail) => coAnimalSpeciesDetail.andId,
    {
      nullable: false,
    },
  )
  @JoinColumn([{ name: 'and_id', referencedColumnName: 'andId' }])
  andId: CoAnimalSpeciesDetail;

  @Column('integer', {
    name: 'fan_number',
    comment: 'Number of animals related to Farmer',
  })
  fanNumber: number;

  @Column('date', {
    name: 'fan_datefrom',
    comment: 'Animals belong to Farmer since this date',
  })
  fanDatefrom: string;

  @Column('date', {
    name: 'fan_dateto',
    nullable: true,
    comment: 'Animals belong to Farmer until this date',
  })
  fanDateto: string | null;
}
