/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
} from 'typeorm';
import { PrDfarmer } from '../sc_niva/pr_dfarmer.entity';
import { PrDproductionunit } from '../sc_niva/pr_dproductionunit.entity';
import { CoSupportSchemes } from './co_supportschemes.entity';

@Index('pk_far_dpayment', ['fapId'], { unique: true })
@Entity('far_dpayment', { schema: 'sc_niva_extra' })
export class FarDpayment extends BaseEntity {
  @Column('uuid', {
    primary: true,
    name: 'fap_id',
    comment:
      'Internal ID to identify the register on database. Only used related with other tables',
  })
  fapId: string;

  @ManyToOne(() => PrDfarmer, (prDfarmer) => prDfarmer.farDpayments, {
    nullable: false,
  })
  @JoinColumn([{ name: 'far_id', referencedColumnName: 'farId' }])
  farId: PrDfarmer;

  @ManyToOne(
    () => PrDproductionunit,
    (prDproductionunit) => prDproductionunit.farDpayments,
    {
      nullable: false,
    },
  )
  @JoinColumn([{ name: 'pun_id', referencedColumnName: 'punId' }])
  punId: PrDproductionunit;

  @Column({
    name: 'fap_amount',
    type: 'decimal',
    comment: 'Amount of payment applied by farmer',
  })
  fapAmount: number;

  @Column({
    name: 'fap_year',
    length: 4,
    comment: 'Year of applying for the payment',
  })
  fapYear: string;

  @ManyToOne(
    () => CoSupportSchemes,
    (coSupportschemes) => coSupportschemes.farDpayments,
    {
      nullable: false,
    },
  )
  @JoinColumn([{ name: 'sus_id', referencedColumnName: 'susId' }])
  susId: CoSupportSchemes;
}
