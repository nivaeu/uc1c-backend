/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, OneToOne, JoinColumn } from 'typeorm';
import { CoPcountry } from '../../sc_niva/co_pcountry.entity';

@Index('pk_loc_co_pcountry', ['couId'], { unique: true })
@Entity('loc_co_pcountry', { schema: 'sc_niva_extra' })
export class LocCoPcountry {
  @Column('character varying', {
    primary: true,
    name: 'cou_id',
    length: 2,
    comment: 'Internal registry ID in database',
  })
  couId: string;

  @OneToOne(() => CoPcountry, (coPcountry) => coPcountry.couId)
  @JoinColumn([{ name: 'cou_id', referencedColumnName: 'couId' }])
  CoPcountry: CoPcountry;

  @Column({
    name: 'cou_name_et',
    length: 100,
    comment: ' Name in local language (Estonian)',
  })
  couNameEt: string;

  @Column({ name: 'cou_name_en', length: 100, comment: 'Name' })
  couNameEn: string;
}
