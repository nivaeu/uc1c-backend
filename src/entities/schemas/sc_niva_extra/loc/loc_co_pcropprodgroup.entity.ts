/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, OneToOne, JoinColumn } from 'typeorm';
import { CoPcropprodgroup } from '../../sc_niva/co_pcropprodgroup.entity';

@Index('pk_loc_co_pcropprodgroup', ['cpgId'], { unique: true })
@Entity('loc_co_pcropprodgroup', {
  schema: 'sc_niva_extra',
})
export class LocCoPcropprodgroup {
  @Column('smallint', {
    primary: true,
    name: 'cpg_id',
    comment: 'Internal registry ID in database',
  })
  cpgId: number;

  @OneToOne(
    () => CoPcropprodgroup,
    (coPcropprodgroup) => coPcropprodgroup.cpgId,
  )
  @JoinColumn([{ name: 'cpg_id', referencedColumnName: 'cpgId' }])
  CoPcropprodgroup: CoPcropprodgroup;

  @Column({
    name: 'cpg_name_et',
    length: 50,
    comment: 'Description in local language (Estonian)',
  })
  cpgNameEt: string;

  @Column({ name: 'cpg_name_en', length: 50, comment: 'Description' })
  cpgNameEn: string;
}
