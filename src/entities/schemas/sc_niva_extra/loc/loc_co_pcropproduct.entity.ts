/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
} from 'typeorm';
import { CoPcropproduct } from '../../sc_niva/co_pcropproduct.entity';
import { CoPcropprodgroup } from '../../sc_niva/co_pcropprodgroup.entity';
import { CouCropProductSoc } from '../cou_cropproduct_soc.entity';
import { CoStandardoutput } from '../co_standardoutput.entity';
import { CoPcropLandCover } from '../co_pcroplandcover.entity';

@Index('pk_loc_co_pcropproduct', ['crpId'])
@Entity('loc_co_pcropproduct', { schema: 'sc_niva_extra' })
export class LocCoPcropproduct extends BaseEntity {
  @Column('character varying', {
    primary: true,
    name: 'crp_id',
    length: 6,
    comment: 'Internal registry ID in database',
  })
  crpId: string;

  @ManyToOne(() => CoPcropproduct, (coPcropproduct) => coPcropproduct.crpId)
  @JoinColumn([{ name: 'crp_id', referencedColumnName: 'crpId' }])
  CoPcropproduct: CoPcropproduct;

  @ManyToOne(
    () => CoPcropLandCover,
    (coPcropLandCover) => coPcropLandCover.clanId,
  )
  @JoinColumn([{ name: 'clan_id', referencedColumnName: 'clanId' }])
  clanId: CoPcropLandCover;

  @Column({
    name: 'crp_use',
    length: 5,
    nullable: true,
    comment: 'Type of use (e.g. for forage)',
  })
  crpUse: string;

  @Column({
    name: 'crp_name_et',
    length: 200,
    comment: 'Description, in local language (Estonian)',
  })
  crpNameEt: string;

  @Column({ name: 'crp_name_en', length: 200, comment: 'Description' })
  crpNameEn: string;

  @Column({
    name: 'crp_botanicalname_et',
    length: 200,
    nullable: true,
    comment: 'Botanical Name, in local language (Estonian)',
  })
  crpBotanicalNameEt: string | null;

  @Column({
    name: 'crp_botanicalname_en',
    length: 200,
    nullable: true,
    comment: 'Botanical Name',
  })
  crpBotanicalNameEn: string | null;

  @ManyToOne(
    () => CoPcropprodgroup,
    (coPcropprodgroup) => coPcropprodgroup.coPcropproducts,
  )
  @JoinColumn([{ name: 'cpg_id', referencedColumnName: 'cpgId' }])
  cpgId: CoPcropprodgroup;

  @ManyToOne(
    () => CouCropProductSoc,
    (CouCropProductSoc) => CouCropProductSoc.socId,
  )
  @JoinColumn([{ name: 'soc_id', referencedColumnName: 'socId' }])
  socId: CouCropProductSoc;

  @ManyToOne(
    () => CoStandardoutput,
    (coStandardoutput) => coStandardoutput.soId,
  )
  @JoinColumn([{ name: 'so_id', referencedColumnName: 'soId' }])
  soId: CoStandardoutput;

  @Column('date', {
    name: 'crp_expirydate',
    nullable: true,
    comment: 'Expiry Date',
  })
  crpExpiryDate: string | null;
}
