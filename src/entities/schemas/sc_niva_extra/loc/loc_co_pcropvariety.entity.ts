/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
} from 'typeorm';
import { CoPcropvariety } from '../../sc_niva/co_pcropvariety.entity';

@Index('loc_co_pcropvariety_pkey', ['couId', 'crpId', 'crvId'])
@Index('pk_loc_co_pcropvariety', ['crvId'], { unique: true })
@Entity('loc_co_pcropvariety', { schema: 'sc_niva_extra' })
export class LocCoPcropvariety extends BaseEntity {
  @Column('character varying', {
    primary: true,
    name: 'crv_id',
    length: 6,
    comment: 'Internal registry ID in database',
  })
  crvId: string;

  @Column('character varying', {
    name: 'crv_name_et',
    length: 50,
    comment: 'Description in local language (Estonian)',
  })
  crvNameEt: string;

  @Column('character varying', {
    name: 'crv_name_en',
    length: 50,
    comment: 'Description',
  })
  crvNameEn: string;

  @Column('character varying', {
    primary: true,
    name: 'crp_id',
    length: 6,
    comment: 'Registry ID, according to values of LOC_CO_PCROPPRODUCT table',
  })
  crpId: string;

  @Column('character varying', {
    primary: true,
    name: 'cou_id',
    length: 2,
    comment: 'Country ID, according to values of LOC_CO_PCOUNTRY table',
  })
  couId: string;

  @ManyToOne(
    () => CoPcropvariety,
    (coPcropvariety) => coPcropvariety.locCoPcropvariety,
  )
  @JoinColumn([
    { name: 'crp_id', referencedColumnName: 'crpId' },
    { name: 'crv_id', referencedColumnName: 'crvId' },
    { name: 'cou_id', referencedColumnName: 'couId' },
  ])
  coPcropvariety: CoPcropvariety;
}
