/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, OneToOne, JoinColumn } from 'typeorm';
import { CoPcultivationdet } from '../../sc_niva/co_pcultivationdet.entity';

@Index('pk_loc_co_pcultivationdet', ['cudId'], { unique: true })
@Entity('loc_co_pcultivationdet', {
  schema: 'sc_niva_extra',
})
export class LocCoPcultivationdet {
  @Column('smallint', {
    primary: true,
    name: 'cud_id',
    comment: 'Internal registry ID in database',
  })
  cudId: number;

  @OneToOne(
    () => CoPcultivationdet,
    (coPcultivationdet) => coPcultivationdet.cudId,
  )
  @JoinColumn([{ name: 'cud_id', referencedColumnName: 'cudId' }])
  CoPcultivationdet: CoPcultivationdet;

  @Column({
    name: 'cud_name_et',
    length: 50,
    comment: 'Description in local language (Estonian)',
  })
  cudNameEt: string;

  @Column({ name: 'cud_name_en', length: 50, comment: 'Description' })
  cudNameEn: string;
}
