/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, OneToOne, JoinColumn } from 'typeorm';
import { CoPcultivationsys } from '../../sc_niva/co_pcultivationsys.entity';

@Index('pk_loc_co_pcultivationsys', ['cusId'], { unique: true })
@Entity('loc_co_pcultivationsys', {
  schema: 'sc_niva_extra',
})
export class LocCoPcultivationsys {
  @Column('smallint', {
    primary: true,
    name: 'cus_id',
    comment: 'Internal registry ID in database',
  })
  cusId: number;

  @OneToOne(
    () => CoPcultivationsys,
    (coPcultivationsys) => coPcultivationsys.cusId,
  )
  @JoinColumn([{ name: 'cus_id', referencedColumnName: 'cusId' }])
  CoPcultivationsys: CoPcultivationsys;

  @Column({
    name: 'cus_name_et',
    length: 50,
    comment: 'Description in local language (Estonian)',
  })
  cusNameEt: string;

  @Column({ name: 'cus_name_en', length: 50, comment: 'Description' })
  cusNameEn: string;
}
