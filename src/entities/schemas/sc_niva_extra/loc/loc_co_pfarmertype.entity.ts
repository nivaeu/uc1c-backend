/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, OneToOne, JoinColumn } from 'typeorm';
import { CoPfarmertype } from '../../sc_niva/co_pfarmertype.entity';

@Index('pk_loc_co_pfarmertype', ['fatId'], { unique: true })
@Entity('loc_co_pfarmertype', { schema: 'sc_niva_extra' })
export class LocCoPfarmertype {
  @Column('smallint', {
    primary: true,
    name: 'fat_id',
    comment: 'Register ID in database',
  })
  fatId: number;

  @OneToOne(() => CoPfarmertype, (coPfarmertype) => coPfarmertype.fatId)
  @JoinColumn([{ name: 'fat_id', referencedColumnName: 'fatId' }])
  CoPfarmertype: CoPfarmertype;

  @Column({
    name: 'fat_name_et',
    length: 50,
    comment: 'Description in local language (Estonian)',
  })
  fatNameEt: string;

  @Column({ name: 'fat_name_en', length: 50, comment: 'Description' })
  fatNameEn: string;
}
