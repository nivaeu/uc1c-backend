/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, OneToOne, JoinColumn } from 'typeorm';
import { CoPfarmingtype } from '../../sc_niva/co_pfarmingtype.entity';

@Index('pk_loc_co_pfarmingtype', ['frtId'], { unique: true })
@Entity('loc_co_pfarmingtype', { schema: 'sc_niva_extra' })
export class LocCoPfarmingtype {
  @Column('character varying', {
    primary: true,
    name: 'frt_id',
    length: 3,
    comment: 'Internal ID in database',
  })
  frtId: string;

  @OneToOne(() => CoPfarmingtype, (coPfarmingtype) => coPfarmingtype.frtId)
  @JoinColumn([{ name: 'frt_id', referencedColumnName: 'frtId' }])
  CoPfarmingtype: CoPfarmingtype;

  @Column({
    name: 'frt_name_et',
    length: 200,
    comment: 'Description in local language (Estonian)',
  })
  frtNameEt: string;

  @Column({ name: 'frt_name_en', length: 200, comment: 'Description' })
  frtNameEn: string;
}
