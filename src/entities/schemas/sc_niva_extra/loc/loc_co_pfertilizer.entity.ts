/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import {
  Column,
  Entity,
  Index,
  OneToOne,
  JoinColumn,
  BaseEntity,
} from 'typeorm';
import { CoPfertilizer } from '../../sc_niva/co_pfertilizer.entity';

@Index('pk_loc_co_pfertilizer', ['ferId'], { unique: true })
@Entity('loc_co_pfertilizer', { schema: 'sc_niva_extra' })
export class LocCoPfertilizer extends BaseEntity {
  @Column('smallint', {
    primary: true,
    name: 'fer_id',
    comment: 'Internal registry ID in database',
  })
  ferId: number;

  @OneToOne(() => CoPfertilizer, (coPfertilizer) => coPfertilizer.ferId)
  @JoinColumn([{ name: 'fer_id', referencedColumnName: 'ferId' }])
  CoPfertilizer: CoPfertilizer;

  @Column({
    name: 'fer_name_et',
    length: 300,
    comment: 'Description in local language (Estonian)',
  })
  ferNameEt: string;

  @Column({ name: 'fer_name_en', length: 300, comment: 'Description' })
  ferNameEn: string;

  @Column({
    name: 'fer_active_substance_et',
    length: 200,
    nullable: true,
    comment: 'Active substance in local language (Estonian)',
  })
  ferActiveSubstanceEt: string;

  @Column({
    name: 'fer_active_substance_en',
    length: 200,
    nullable: true,
    comment: 'Active substance',
  })
  ferActiveSubstanceEn: string;

  @Column({
    name: 'fer_national_id',
    length: 20,
    nullable: true,
    comment: 'National fertilizer registry ID, if available',
  })
  ferNationalId: string;
}
