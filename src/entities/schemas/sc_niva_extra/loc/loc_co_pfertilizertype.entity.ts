/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, OneToOne, JoinColumn } from 'typeorm';
import { CoPfertilizertype } from '../../sc_niva/co_pfertilizertype.entity';

@Index('pk_loc_co_pfertilizertype', ['fetId'], { unique: true })
@Entity('loc_co_pfertilizertype', {
  schema: 'sc_niva_extra',
})
export class LocCoPfertilizertype {
  @Column('smallint', {
    primary: true,
    name: 'fet_id',
    comment: 'Internal registry ID in database',
  })
  fetId: number;

  @OneToOne(
    () => CoPfertilizertype,
    (coPfertilizertype) => coPfertilizertype.fetId,
  )
  @JoinColumn([{ name: 'fet_id', referencedColumnName: 'fetId' }])
  CoPfertilizertype: CoPfertilizertype;

  @Column({
    name: 'fet_name_et',
    length: 100,
    comment: 'Description in local language (Estonian)',
  })
  fetNameEt: string;

  @Column({ name: 'fet_name_en', length: 100, comment: 'Description' })
  fetNameEn: string;
}
