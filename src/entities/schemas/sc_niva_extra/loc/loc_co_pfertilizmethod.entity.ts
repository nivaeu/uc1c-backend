/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, OneToOne, JoinColumn } from 'typeorm';
import { CoPfertilizmethod } from '../../sc_niva/co_pfertilizmethod.entity';

@Index('pk_loc_co_pfertilizmethod', ['femId'], { unique: true })
@Entity('loc_co_pfertilizmethod', {
  schema: 'sc_niva_extra',
})
export class LocCoPfertilizmethod {
  @Column('smallint', {
    primary: true,
    name: 'fem_id',
    comment: 'Internal registry ID in database',
  })
  femId: number;

  @OneToOne(
    () => CoPfertilizmethod,
    (coPfertilizmethod) => coPfertilizmethod.femId,
  )
  @JoinColumn([{ name: 'fem_id', referencedColumnName: 'femId' }])
  CoPfertilizmethod: CoPfertilizmethod;

  @Column({
    name: 'fem_name_et',
    length: 50,
    comment: 'Description in local language (Estonian)',
  })
  femNameEt: string;

  @Column({ name: 'fem_name_en', length: 50, comment: 'Description' })
  femNameEn: string;
}
