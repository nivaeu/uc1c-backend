/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, OneToOne, JoinColumn } from 'typeorm';
import { CoPfertiliztype } from '../../sc_niva/co_pfertiliztype.entity';

@Index('pk_loc_co_pfertiliztype', ['frtId'], { unique: true })
@Entity('loc_co_pfertiliztype', { schema: 'sc_niva_extra' })
export class LocCoPfertiliztype {
  @Column('smallint', {
    primary: true,
    name: 'frt_id',
    comment: 'Internal registry ID in database',
  })
  frtId: number;

  @OneToOne(() => CoPfertiliztype, (coPfertiliztype) => coPfertiliztype.frtId)
  @JoinColumn([{ name: 'frt_id', referencedColumnName: 'frtId' }])
  CoPfertiliztype: CoPfertiliztype;

  @Column({
    name: 'frt_name_et',
    length: 50,
    comment: 'Description in local language (Estonian)',
  })
  frtNameEt: string;

  @Column({ name: 'frt_name_en', length: 50, comment: 'Description' })
  frtNameEn: string;
}
