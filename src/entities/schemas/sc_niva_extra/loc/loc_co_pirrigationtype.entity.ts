/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, OneToOne, JoinColumn } from 'typeorm';
import { CoPirrigationtype } from '../../sc_niva/co_pirrigationtype.entity';

@Index('pk_loc_co_pirrigationtype', ['irrId'], { unique: true })
@Entity('loc_co_pirrigationtype', {
  schema: 'sc_niva_extra',
})
export class LocCoPirrigationtype {
  @Column('smallint', {
    primary: true,
    name: 'irr_id',
    comment: 'Internal registry ID in database',
  })
  irrId: number;

  @OneToOne(
    () => CoPirrigationtype,
    (coPirrigationtype) => coPirrigationtype.irrId,
  )
  @JoinColumn([{ name: 'irr_id', referencedColumnName: 'irrId' }])
  CoPirrigationtype: CoPirrigationtype;

  @Column({
    name: 'irr_name_et',
    length: 50,
    comment: 'Description in local language (Estonian)',
  })
  irrNameEt: string;

  @Column({ name: 'irr_name_en', length: 50, comment: 'Description' })
  irrNameEn: string;
}
