/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import {
  Column,
  Entity,
  Index,
  OneToOne,
  JoinColumn,
  BaseEntity,
} from 'typeorm';
import { CoPlabortype } from '../../sc_niva/co_plabortype.entity';

@Index('pk_loc_co_plabortype', ['labId'], { unique: true })
@Entity('loc_co_plabortype', { schema: 'sc_niva_extra' })
export class LocCoPlabortype extends BaseEntity {
  @Column('smallint', {
    primary: true,
    name: 'lab_id',
    comment: 'Internal registry ID in database',
  })
  labId: number;

  @OneToOne(() => CoPlabortype, (coPlabortype) => coPlabortype.labId)
  @JoinColumn([{ name: 'lab_id', referencedColumnName: 'labId' }])
  CoPlabortype: CoPlabortype;

  @Column({
    name: 'lab_name_et',
    length: 100,
    comment: 'Description in local language (Estonian)',
  })
  labNameEt: string;

  @Column({ name: 'lab_name_en', length: 100 })
  labNameEn: string;

  @Column({ name: 'lab_type', length: 30 })
  labType: string;
}
