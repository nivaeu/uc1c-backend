/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, OneToOne, JoinColumn } from 'typeorm';
import { CoPlandtenure } from '../../sc_niva/co_plandtenure.entity';

@Index('pk_loc_co_plandtenure', ['lteId'], { unique: true })
@Entity('loc_co_plandtenure', { schema: 'sc_niva_extra' })
export class LocCoPlandtenure {
  @Column('smallint', {
    primary: true,
    name: 'lte_id',
    comment: 'Internal registry ID in database',
  })
  lteId: number;

  @OneToOne(() => CoPlandtenure, (coPlandtenure) => coPlandtenure.lteId)
  @JoinColumn([{ name: 'lte_id', referencedColumnName: 'lteId' }])
  CoPlandtenure: CoPlandtenure;

  @Column({
    name: 'lte_name_et',
    length: 50,
    comment: 'Description in local language (Estonian)',
  })
  lteNameEt: string;

  @Column({ name: 'lte_name_en', length: 50, comment: 'Description' })
  lteNameEn: string;
}
