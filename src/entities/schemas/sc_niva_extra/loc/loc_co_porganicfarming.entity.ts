/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, OneToOne, JoinColumn } from 'typeorm';
import { CoPorganicfarming } from '../../sc_niva/co_porganicfarming.entity';

@Index('pk_loc_co_porganicfarming', ['orfId'], { unique: true })
@Entity('loc_co_porganicfarming', {
  schema: 'sc_niva_extra',
})
export class LocCoPorganicfarming {
  @Column('smallint', {
    primary: true,
    name: 'orf_id',
    comment: 'Internal registry ID in database',
  })
  orfId: number;

  @OneToOne(
    () => CoPorganicfarming,
    (coPorganicfarming) => coPorganicfarming.orfId,
  )
  @JoinColumn([{ name: 'orf_id', referencedColumnName: 'orfId' }])
  CoPorganicfarming: CoPorganicfarming;

  @Column({
    name: 'orf_name_et',
    length: 50,
    comment: 'Description in local language (Estonian)',
  })
  orfNameEt: string;

  @Column({ name: 'orf_name_en', length: 50, comment: 'Description' })
  orfNameEn: string;
}
