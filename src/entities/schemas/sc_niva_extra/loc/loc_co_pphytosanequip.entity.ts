/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, OneToOne, JoinColumn } from 'typeorm';
import { CoPphytosanequip } from '../../sc_niva/co_pphytosanequip.entity';

@Index('pk_loc_co_pphytosanequip', ['pheId'], { unique: true })
@Entity('loc_co_pphytosanequip', {
  schema: 'sc_niva_extra',
})
export class LocCoPphytosanequip {
  @Column('smallint', {
    primary: true,
    name: 'phe_id',
    comment: 'Internal registry ID in database',
  })
  pheId: number;

  @OneToOne(
    () => CoPphytosanequip,
    (coPphytosanequip) => coPphytosanequip.pheId,
  )
  @JoinColumn([{ name: 'phe_id', referencedColumnName: 'pheId' }])
  CoPphytosanequip: CoPphytosanequip;

  @Column({
    name: 'phe_name_et',
    length: 100,
    comment: 'Description in local language (Estonian)',
  })
  pheNameEt: string;

  @Column({ name: 'phe_name_en', length: 100, comment: 'Description' })
  pheNameEn: string;
}
