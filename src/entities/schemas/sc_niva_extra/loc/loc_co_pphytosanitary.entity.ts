/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import {
  Column,
  Entity,
  Index,
  OneToOne,
  JoinColumn,
  BaseEntity,
} from 'typeorm';
import { CoPphytosanitary } from '../../sc_niva/co_pphytosanitary.entity';

@Index('pk_loc_co_pphytosanitary', ['phyId'], { unique: true })
@Entity('loc_co_pphytosanitary', {
  schema: 'sc_niva_extra',
})
export class LocCoPphytosanitary extends BaseEntity {
  @Column('character varying', {
    primary: true,
    name: 'phy_id',
    length: 8,
    comment: 'Internal registry ID in database',
  })
  phyId: string;

  @OneToOne(
    () => CoPphytosanitary,
    (coPphytosanitary) => coPphytosanitary.phyId,
  )
  @JoinColumn([{ name: 'phy_id', referencedColumnName: 'phyId' }])
  CoPphytosanitary: CoPphytosanitary;

  @Column({
    name: 'phy_name_et',
    length: 50,
    comment: 'Description in local language (Estonian)',
  })
  phyNameEt: string;

  @Column({ name: 'phy_name_en', length: 50, comment: 'Description' })
  phyNameEn: string;

  @Column({
    name: 'phy_formulation_et',
    length: 50,
    nullable: true,
    comment: 'Formulation in local language (Estonian)',
  })
  phyFormulationEt: string | null;

  @Column({
    name: 'phy_formulation_en',
    length: 50,
    nullable: true,
    comment: 'Formulation',
  })
  phyFormulationEn: string | null;
}
