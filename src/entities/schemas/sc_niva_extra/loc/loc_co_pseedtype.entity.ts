/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, OneToOne, JoinColumn } from 'typeorm';
import { CoPseedtype } from '../../sc_niva/co_pseedtype.entity';

@Index('pk_loc_co_pseedtype', ['seeId'], { unique: true })
@Entity('loc_co_pseedtype', { schema: 'sc_niva_extra' })
export class LocCoPseedtype {
  @Column('smallint', {
    primary: true,
    name: 'see_id',
    comment: 'Internal registry ID in database',
  })
  seeId: number;

  @OneToOne(() => CoPseedtype, (coPseedtype) => coPseedtype.seeId)
  @JoinColumn([{ name: 'see_id', referencedColumnName: 'seeId' }])
  CoPseedtype: CoPseedtype;

  @Column({
    name: 'see_name_et',
    length: 50,
    comment: 'Description in local language (Estonian)',
  })
  seeNameEt: string;

  @Column({ name: 'see_name_en', length: 50, comment: 'Description' })
  seeNameEn: string;
}
