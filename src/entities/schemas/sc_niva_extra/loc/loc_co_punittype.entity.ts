/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import {
  Column,
  Entity,
  Index,
  OneToOne,
  JoinColumn,
  BaseEntity,
} from 'typeorm';
import { CoPunittype } from '../../sc_niva/co_punittype.entity';

@Index('pk_loc_co_punittype', ['uniId'], { unique: true })
@Entity('loc_co_punittype', { schema: 'sc_niva_extra' })
export class LocCoPunittype extends BaseEntity {
  @Column('smallint', {
    primary: true,
    name: 'uni_id',
    comment: 'Internal registry ID in database',
  })
  uniId: number;

  @OneToOne(() => CoPunittype, (coPunittype) => coPunittype.uniId)
  @JoinColumn([{ name: 'uni_id', referencedColumnName: 'uniId' }])
  CoPunittype: CoPunittype;

  @Column({
    name: 'uni_name_et',
    length: 50,
    comment: 'Unit ratio in local language (Estonian)',
  })
  uniNameEt: string;

  @Column({ name: 'uni_name_en', length: 50, comment: 'Unit ratio' })
  uniNameEn: string;

  @Column({
    name: 'unit_name_et',
    length: 50,
    comment: 'Unit description in local language (Estonian)',
  })
  unitNameEt: string;

  @Column({ name: 'unit_name_en', length: 50, comment: 'Unit description' })
  unitNameEn: string;
}
