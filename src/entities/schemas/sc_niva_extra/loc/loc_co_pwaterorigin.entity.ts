/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, OneToOne, JoinColumn } from 'typeorm';
import { CoPwaterorigin } from '../../sc_niva/co_pwaterorigin.entity';

@Index('pk_loc_co_pwaterorigin', ['waoId'], { unique: true })
@Entity('loc_co_pwaterorigin', { schema: 'sc_niva_extra' })
export class LocCoPwaterorigin {
  @Column('smallint', {
    primary: true,
    name: 'wao_id',
    comment: 'Internal registry ID in database',
  })
  waoId: number;

  @OneToOne(() => CoPwaterorigin, (coPwaterorigin) => coPwaterorigin.waoId)
  @JoinColumn([{ name: 'wao_id', referencedColumnName: 'waoId' }])
  CoPwaterorigin: CoPwaterorigin;

  @Column({
    name: 'wao_name_et',
    length: 50,
    comment: 'Description in local language (Estonian)',
  })
  waoNameEt: string;

  @Column({ name: 'wao_name_en', length: 50, comment: 'Description' })
  waoNameEn: string;
}
