/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, OneToOne, JoinColumn } from 'typeorm';
import { CoPworkunittype } from '../../sc_niva/co_pworkunittype.entity';

@Index('pk_loc_co_pworkunittype', ['wutId'], { unique: true })
@Entity('loc_co_pworkunittype', { schema: 'sc_niva_extra' })
export class LocCoPworkunittype {
  @Column('smallint', {
    primary: true,
    name: 'wut_id',
    comment: 'Internal registry ID in database',
  })
  wutId: number;

  @OneToOne(() => CoPworkunittype, (coPworkunittype) => coPworkunittype.wutId)
  @JoinColumn([{ name: 'wut_id', referencedColumnName: 'wutId' }])
  CoPworkunittype: CoPworkunittype;

  @Column({
    name: 'wut_name_et',
    length: 50,
    comment: 'Description in local language (Estonian)',
  })
  wutNameEt: string;

  @Column({ name: 'wut_name_en', length: 50, comment: 'Description' })
  wutNameEn: string;
}
