/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { Column, Entity, Index, OneToOne, JoinColumn } from 'typeorm';
import { EcrDreportdoc } from '../../sc_niva/ecr_dreportdoc.entity';

@Index('pk_loc_ecr_dreportdoc', ['ecrId'], { unique: true })
@Entity('loc_ecr_dreportdoc', { schema: 'sc_niva_extra' })
export class LocEcrDreportdoc {
  @Column('integer', {
    primary: true,
    name: 'ecr_id',
    comment: 'Identifier of the Report Document',
  })
  ecrId: number;

  @OneToOne(() => EcrDreportdoc, (ecrDreportdoc) => ecrDreportdoc.ecrId)
  @JoinColumn([{ name: 'ecr_id', referencedColumnName: 'ecrId' }])
  EcrDreportdoc: EcrDreportdoc;

  @Column({
    name: 'ecr_description_et',
    length: 5,
    nullable: true,
    comment:
      'The textual description of this crop report document, in local language (Estonian)',
  })
  ecrDescriptionEt: string | null;

  @Column({
    name: 'ecr_description_en',
    length: 5,
    nullable: true,
    comment: 'The textual description of this crop report document',
  })
  ecrDescriptionEn: string | null;

  @Column({
    name: 'ecr_information_et',
    length: 255,
    nullable: true,
    comment:
      'Information, expressed as text, for this crop report document, in local language (Estonian)',
  })
  ecrInformationEt: string | null;

  @Column({
    name: 'ecr_information_en',
    length: 255,
    nullable: true,
    comment: 'Information, expressed as text, for this crop report document',
  })
  ecrInformationEn: string | null;
}
