/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import {
  Column,
  Entity,
  Index,
  OneToOne,
  JoinColumn,
  BaseEntity,
} from 'typeorm';
import { RpDcropparcel } from '../../sc_niva/rp_dcropparcel.entity';

@Index('pk_loc_rp_dcropparcel', ['rcpId'], { unique: true })
@Entity('loc_rp_dcropparcel', { schema: 'sc_niva_extra' })
export class LocRpDcropParcel extends BaseEntity {
  @Column('uuid', {
    primary: true,
    name: 'rcp_id',
    comment: 'Register ID related with RP_DCROPPARCEL table',
  })
  rcpId: string;

  @OneToOne(() => RpDcropparcel, (rpDcropparcel) => rpDcropparcel.rcpId)
  @JoinColumn([{ name: 'rcp_id', referencedColumnName: 'rcpId' }])
  rcp: RpDcropparcel;

  @Column('bigint', {
    name: 'crpreg_pa_id',
    comment:
      'Agricultural parcel ID, generated in Paying Agency internal register (IACS)',
  })
  crpegPaId: string;
}
