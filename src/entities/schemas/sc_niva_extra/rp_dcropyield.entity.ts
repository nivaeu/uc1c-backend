/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import {
  Column,
  Entity,
  Index,
  BaseEntity,
  JoinColumn,
  ManyToOne,
} from 'typeorm';
import { RpDcropparcel } from '../sc_niva/rp_dcropparcel.entity';
import { CoPunittype } from '../sc_niva/co_punittype.entity';

@Index('pk_rp_dcropyield', ['rcpId'], { unique: true })
@Entity('rp_dcropyield', { schema: 'sc_niva_extra' })
export class RpDcropYield extends BaseEntity {
  @Column('uuid', {
    primary: true,
    name: 'rcp_id',
    comment: 'Register ID related to RP_DCROPPARCEL table',
  })
  rcpId: string;

  @ManyToOne(() => RpDcropparcel, (rpDcropparcel) => rpDcropparcel.rcpId)
  @JoinColumn([{ name: 'rcp_id', referencedColumnName: 'rcpId' }])
  rpDcropparcel: RpDcropparcel;

  @Column({
    name: 'cry_planned_amount',
    type: 'decimal',
    nullable: true,
    comment: 'Amount of yield planned',
  })
  cryPlannedAmount: number;

  @Column({
    name: 'cry_actual_amount',
    type: 'decimal',
    nullable: true,
    comment: 'Amount of yield actually harvested',
  })
  cryActualAmount: number;

  @ManyToOne(() => CoPunittype, (coPunittype) => coPunittype.uniId)
  @JoinColumn([{ name: 'uni_id', referencedColumnName: 'uniId' }])
  uniId: CoPunittype;
}
