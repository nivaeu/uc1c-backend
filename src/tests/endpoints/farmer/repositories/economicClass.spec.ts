/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { getPossibleSocCodes } from '../../../../endpoints/farmer/repositories/utils/economicClass';

it('Should get all possible soc codes used in production type calculation', () => {
  const possibleSocCodes = getPossibleSocCodes();

  expect(possibleSocCodes).toEqual([
    'SOC_CLVS015',
    'SOC_CLVS008',
    'SOC_CLVS012',
    'SOC_CLND004',
    'SOC_CLND005',
    'SOC_CLND006',
    'SOC_CLND007',
    'SOC_CLND008',
    'SOC_CLND009',
    'SOC_CLND010_011_012',
    'SOC_CLND013',
    'SOC_CLND014',
    'SOC_CLND015',
    'SOC_CLND017',
    'SOC_CLND018',
    'SOC_CLND032',
    'SOC_CLND033',
    'SOC_CLND030',
    'SOC_CLND022',
    'SOC_CLND023',
    'SOC_CLND024',
    'SOC_CLND025',
    'SOC_CLND026',
    'SOC_CLND028',
    'SOC_CLND029',
    'SOC_CLND031',
    'SOC_CLND034',
    'SOC_CLND035_036',
    'SOC_CLND045',
    'SOC_CLND047',
    'SOC_CLND048_083',
    'SOC_CLND049',
    'SOC_CLND019',
    'SOC_CLND038',
    'SOC_CLND039',
    'SOC_CLND040',
    'SOC_CLND041_042',
    'SOC_CLND051',
    'SOC_CLND052',
    'SOC_CLND044',
    'SOC_CLND081',
    'SOC_CLND046',
    'SOC_CLND082',
    'SOC_CLND079',
    'SOC_CLND070',
    'SOC_CLND056',
    'SOC_CLND057',
    'SOC_CLND059',
    'SOC_CLND060',
    'SOC_CLND061',
    'SOC_CLND067',
    'SOC_CLND069',
    'SOC_CLND062',
    'SOC_CLND071',
    'SOC_CLVS001',
    'SOC_CLVS004',
    'SOC_CLVS007',
    'SOC_CLVS009',
    'SOC_CLVS011',
    'SOC_CLVS003',
    'SOC_CLVS005',
    'SOC_CLVS010',
    'SOC_CLVS013',
    'SOC_CLVS014',
    'SOC_CLVS016',
    'SOC_CLVS017',
    'SOC_CLVS018',
    'SOC_CLVS019',
    'SOC_CLVS020',
    'SOC_CLVS021',
    'SOC_CLVS022',
    'SOC_CLVS023',
    'SOC_CLVS029',
    'SOC_CLND055',
    'SOC_CLND061',
    'SOC_CLND084',
    'SOC_CLND037',
    'SOC_CLVS030',
  ]);
});
